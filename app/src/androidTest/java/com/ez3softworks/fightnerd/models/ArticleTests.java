package com.ez3softworks.fightnerd.models;

import android.app.Activity;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static org.junit.Assert.*;

import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.AndroidJUnitRunner;
import android.test.ActivityInstrumentationTestCase2;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(AndroidJUnit4.class)
public class ArticleTests {
    public ArticleTests() {
    }

    Context context;
    Realm realm;

    @Before
    public void setUp() {
        context = new RenamingDelegatingContext(InstrumentationRegistry.getInstrumentation().getTargetContext(), "test_");

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).name("test.realm").inMemory().build();
        realm = Realm.getInstance(realmConfiguration);
    }

    @Test
    public void createArticle() throws Exception {
        realm.beginTransaction();
        Article article = realm.createOrUpdateObjectFromJson(Article.class, "{\"publication\":\"MMA Junkie\",\"author\":\"Mike Bohn\",\"title\":\"UFC Fight Night 82 post-fight facts: 'Wonderboy' winning at welterweight\",\"sourceUrl\":\"http://mmajunkie.com/2016/02/ufc-fight-night-82-post-fight-facts-wonderboy-winning-at-welterweight\",\"imageUrl\":\"http://usatmmajunkie.files.wordpress.com/2016/02/stephen-thompson-ufc-fight-night-82-2.jpg\",\"date\":\"2016-02-07T19:15:20Z\",\"guid\":\"0fe2ca8bb51ec70e104508693747c979\",\"url\":\"https://d2omniay5i8qm4.cloudfront.net/api/publications/article/0fe2ca8bb51ec70e104508693747c979/\",\"contentUrl\":\"https://d2omniay5i8qm4.cloudfront.net/publications/article/0fe2ca8bb51ec70e104508693747c979/\"},{\"publication\":\"MMA Mania\",\"author\":\"Alex Schlinsky\",\"title\":\"UFC Fight Night 82 results recap: What's next fight for Stephen Thompson?\",\"sourceUrl\":\"http://www.mmamania.com/2016/2/7/10930530/ufc-fight-night-82-results-recap-whats-next-fight-for-stephen-thompson-mma\",\"imageUrl\":\"http://cdn3.vox-cdn.com/thumbor/NH-kyGDa5Ry0_hryQIou4q7flBI=/0x239:2996x1924/1600x900/cdn0.vox-cdn.com/uploads/chorus_image/image/48749511/usa-today-9100809.0.jpg\",\"date\":\"2016-02-07T19:00:02Z\",\"guid\":\"f57d195d57964e03e015e4e39057801a\",\"url\":\"https://d2omniay5i8qm4.cloudfront.net/api/publications/article/f57d195d57964e03e015e4e39057801a/\",\"contentUrl\":\"https://d2omniay5i8qm4.cloudfront.net/publications/article/f57d195d57964e03e015e4e39057801a/\"},{\"publication\":\"MMA Junkie\",\"author\":\"Dann Stupp\",\"title\":\"Joseph Benavidez on fellow contender Henry Cejudo's title shot: 'He didn't earn it'\",\"sourceUrl\":\"http://mmajunkie.com/2016/02/joseph-benavidez-on-fellow-contender-henry-cejudos-title-shot-he-didnt-earn-it\",\"imageUrl\":\"http://usatmmajunkie.files.wordpress.com/2016/02/joseph-benavidez-post-ufc-fight-night-82.jpg\",\"date\":\"2016-02-07T18:30:15Z\",\"guid\":\"e889fd4cf67a117b19bb9971cb9b7a36\",\"url\":\"https://d2omniay5i8qm4.cloudfront.net/api/publications/article/e889fd4cf67a117b19bb9971cb9b7a36/\",\"contentUrl\":\"https://d2omniay5i8qm4.cloudfront.net/publications/article/e889fd4cf67a117b19bb9971cb9b7a36/\"}");
        realm.commitTransaction();
        assertNotNull(article.getGuid());
        assertEquals(article.getGuid(), "0fe2ca8bb51ec70e104508693747c979");
        assertNotNull(article.getAuthor());
        assertNotNull(article.getContentUrl());
        assertNotNull(article.getDate());
        assertNotNull(article.getImageUrl());
        assertNotNull(article.getPublication());
        assertNotNull(article.getSourceUrl());
        assertNotNull(article.getTitle());
        assertNotNull(article.getUrl());
    }
}