package com.ez3softworks.fightnerd.stores;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.RenamingDelegatingContext;

import com.ez3softworks.fightnerd.models.Article;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(AndroidJUnit4.class)
public class ApiManagerTests {
    Context context;

    @Before
    public void setUp() {
        context = new RenamingDelegatingContext(InstrumentationRegistry.getInstrumentation().getTargetContext(), "test_");
    }

    @Test
    public void listArticles() throws Exception {
        ApiManager apiManager = ApiManager.getInstance();
        Call<ApiManager.ListResponse<Article>> call = apiManager.fightNerdService.listArticles(null, null);
        ApiManager.ListResponse<Article> articleListResponse = call.execute().body();
        assertNotNull(articleListResponse);
        assertNotNull(articleListResponse.getResults());
        assertTrue(articleListResponse.getResults().size() > 0);
    }

    @Test
    public void filterArticlesByDate() throws Exception {
        ApiManager apiManager = ApiManager.getInstance();
        DateTime dateTime = new DateTime();
        dateTime = dateTime.minus(3 * 60 * 60 * 1000);
        ApiManager.QueryDate queryDate = new ApiManager.QueryDate(dateTime.toDate());
        Call<ApiManager.ListResponse<Article>> call = apiManager.fightNerdService.listArticles(queryDate, null);
        ApiManager.ListResponse<Article> articleListResponse = call.execute().body();
        assertNotNull(articleListResponse);
        assertNotNull(articleListResponse.getResults());
        assertTrue(articleListResponse.getResults().size() > 0);
        for (Article article : articleListResponse.getResults()) {
            assertTrue(article.getDate().getTime() <= dateTime.toDate().getTime());
        }
    }

    @Test
    public void filterArticlesByPublication() throws Exception {
        String publication = "Sherdog";
        ApiManager apiManager = ApiManager.getInstance();
        Call<ApiManager.ListResponse<Article>> call = apiManager.fightNerdService.listArticles(null, publication);
        ApiManager.ListResponse<Article> articleListResponse = call.execute().body();
        assertNotNull(articleListResponse);
        assertNotNull(articleListResponse.getResults());
        assertTrue(articleListResponse.getResults().size() > 0);
        for (Article article : articleListResponse.getResults()) {
            assertEquals(article.getPublication(), publication);
        }
    }

}