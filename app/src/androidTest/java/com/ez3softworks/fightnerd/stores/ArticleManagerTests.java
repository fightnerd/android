package com.ez3softworks.fightnerd.stores;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.RenamingDelegatingContext;

import com.ez3softworks.fightnerd.models.Article;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

import io.realm.Realm;
import io.realm.RealmResults;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class ArticleManagerTests {
    Context context;
    ArticleManager articleManager;

    @Before
    public void setUp() {
        context = new RenamingDelegatingContext(InstrumentationRegistry.getInstrumentation().getTargetContext(), "test_");
        articleManager = ArticleManager.getInstance(context);
    }

    @Test
    public void fetchArticles() throws Exception {
        Realm realm = RealmManager.getInstance(context).getRealm();
        RealmResults<Article> realmResults = realm.where(Article.class).findAll();
        assertEquals(realmResults.size(), 0);

        final CountDownLatch countDownLatch = new CountDownLatch(1);
        articleManager.fetchArticles(null, null, new Callback() {
            @Override
            public void onSuccess() {
                Realm realm = RealmManager.getInstance(context).getRealm();
                RealmResults<Article> realmResults = realm.where(Article.class).findAll();
                assertTrue(realmResults.size() > 0);
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(Throwable t) {
                throw new AssertionError(t);
            }
        });
        countDownLatch.await();

    }

}