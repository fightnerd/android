package com.ez3softworks.fightnerd.controllers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.utilities.LocaleUtilities;
import com.ez3softworks.fightnerd.utilities.SpannableStringBuilder;
import com.ez3softworks.fightnerd.utilities.StringUtilities;
import com.ez3softworks.fightnerd.utilities.TypefaceUtilities;

public abstract class SpinnerAdapter extends ArrayAdapter<CharSequence> {
    private final int resource;
	public SpinnerAdapter(Context context, int resource, CharSequence[] objects) {
        super(context, resource, objects);
        this.resource = resource;
    }

    @Override
    public View getView(final int position, final View convertView,
                        final ViewGroup viewGroup) {
        int selectedItemPosition = position;
        if (viewGroup instanceof AdapterView) {
            selectedItemPosition = ((AdapterView) viewGroup).getSelectedItemPosition();
        }
        return makeLayout(selectedItemPosition, convertView, viewGroup, resource);
    }

    private View makeLayout(final int position, final View convertView,
                            final ViewGroup parent, final int layout) {
        TextView textView;
        if (convertView != null) {
            textView = (TextView) convertView;
        } else {
            textView = (TextView) LayoutInflater.from(getContext()).inflate(layout, parent, false);
        }

        textView.setText(getItem(position));

        return textView;
    }
}