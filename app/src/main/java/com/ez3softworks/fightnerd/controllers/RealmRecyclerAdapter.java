package com.ez3softworks.fightnerd.controllers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import io.realm.RealmResults;

public abstract class RealmRecyclerAdapter<T extends io.realm.RealmObject> extends RecyclerView.Adapter<RealmRecyclerAdapter.ViewHolder> {
    private final RealmResults<T> realmResults;
    private final Context context;

    public RealmRecyclerAdapter(Context context, RealmResults<T> realmResults) {
        this.realmResults = realmResults;
        this.context = context;
    }

    public abstract T getItem(int position);

    public RealmResults<T> getRealmResults() {
        return realmResults;
    }

    public Context getContext() {
        return context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }


}
