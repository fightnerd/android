package com.ez3softworks.fightnerd.controllers;

import java.lang.reflect.AccessibleObject;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;

import android.support.v7.app.ActionBar;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseListFragment extends ListFragment {
	
	@Override
	public void onStart() {
		super.onStart();
	}
	
	@Override
	public void onStop() {
		super.onStop();
	}
	
	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
//		getActivity().getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	}

	public AppCompatActivity getSupportActivity() {
		Activity activity = getActivity();
		if (activity != null && activity instanceof AppCompatActivity) {
			return (AppCompatActivity)activity;
		}
		return null;
	}

	public ActionBar getSupportActionBar() {
		AppCompatActivity supportActivity = getSupportActivity();
		return supportActivity.getSupportActionBar();
	}
}
