package com.ez3softworks.fightnerd.controllers;

import android.content.Intent;
import android.os.Bundle;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Article;
import com.ez3softworks.fightnerd.stores.RealmManager;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

public class ArticleDetailFragment extends RealmWebFragment<Article> {
	public static final String TAG = "ArticleDetailFragment";

    public static ArticleDetailFragment newInstance(String guid) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_GUID, guid);

        ArticleDetailFragment fragment = new ArticleDetailFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected Class<Article> getRealmObjectClass() {
        return Article.class;
    }

    @Override
    protected String getContentUrl() {
        Article article = getRealmObject();
        return article.getContentUrl();
    }

    @Override
    protected String getActionBarTitle() {
        Article article = getRealmObject();
        return article.getPublication();
    }

    @Override
    protected Intent getShareIntent() {
        Article article = getRealmObject();
        if (article == null) {
            return null;
        }
        Intent shareIntent = new Intent();
        String text = String.format("%s", article.getSourceUrl());
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, article.getTitle());
        shareIntent.setType("text/plain");
        return shareIntent;
    }

    @Override
    protected boolean isFavorite() {
        Article article = getRealmObject();
        return article.isFavorite();
    }

    @Override
    protected void onPressFavoriteMenuItem() {
        Realm realm = RealmManager.getInstance(getContext().getApplicationContext()).getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Article article = getRealmObject();
                article.setIsFavorite(!article.isFavorite());
            }
        });
    }

    @Override
    protected int getOptionsMenuResourceId() {
        return R.menu.article_detail_options;
    }

    @Override
    public String getEventName() {
        return "Article Detail";
    }

    @Override
    public Map<String, Object> getEventAttributes() {
        Article article = getRealmObject();
        Map<String, Object> eventAttributes = new HashMap<>();
        eventAttributes.put("sourceUrl", article.getSourceUrl());
        eventAttributes.put("author", article.getAuthor());
        eventAttributes.put("publication", article.getPublication());
        eventAttributes.put("guid", article.getGuid());
        eventAttributes.put("title", article.getTitle());
        eventAttributes.put("contentUrl", article.getContentUrl());
        return eventAttributes;
    }

    @Override
    protected String getSourceUrl() {
        Article article = getRealmObject();
        return article.getSourceUrl();
    }
}
