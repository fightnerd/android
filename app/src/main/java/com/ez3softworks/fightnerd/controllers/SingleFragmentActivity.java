package com.ez3softworks.fightnerd.controllers;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.animation.AccelerateInterpolator;
import android.webkit.WebSettings;
import android.widget.ListView;

import com.ez3softworks.fightnerd.R;

public abstract class SingleFragmentActivity extends BaseFragmentActivity {

	private Toolbar toolbar;

	protected abstract Fragment createFragment();

	protected int getLayoutResId() {
		return R.layout.activity_single_fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutResId());
		FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment fragment = fragmentManager.findFragmentById(R.id.frame_layout);
		if (fragment == null) {
			fragment = createFragment();
			fragmentManager.beginTransaction().add(R.id.frame_layout, fragment).commit();
		}

		// Nav bar
		toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeButtonEnabled(true);
		}

	}

//	public void hideToolbar() {
//		if (toolbar == null) {
//			return;
//		}
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1 ) {
//			toolbar.animate().translationY(-toolbar.getBottom()).setInterpolator(new AccelerateInterpolator()).start();
//		} else {
//			ActionBar actionBar = getSupportActionBar();
//			if (actionBar != null) {
//				actionBar.hide();
//			}
//		}
//
//	}
//
//	public void showToolbar() {
//		ActionBar actionBar = getSupportActionBar();
//		if (actionBar == null) {
//			return;
//		}
//
//		if (toolbar == null) {
//			return;
//		}
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1 ) {
//			actionBar.animate().translationY(-toolbar.getBottom()).setInterpolator(new AccelerateInterpolator()).start();
//		} else {
//			ActionBar actionBar = getSupportActionBar();
//			if (actionBar != null) {
//				actionBar.hide();
//			}
//		}
//
//		toolbar.animate().translationY(-toolbar.getBottom()).setInterpolator(new AccelerateInterpolator()).start();
//	}

}
