package com.ez3softworks.fightnerd.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Article;
import com.ez3softworks.fightnerd.models.Publication;
import com.ez3softworks.fightnerd.stores.ApiManager;
import com.ez3softworks.fightnerd.stores.ArticleManager;
import com.ez3softworks.fightnerd.stores.RealmManager;
import com.ez3softworks.fightnerd.views.ArticleListItemLayout;
import com.ez3softworks.fightnerd.views.ImagelessArticleListItemLayout;

import java.util.Date;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Response;


public class ArticleListFragment extends ListFragment {
	private static final String TAG = "ArticleListFragment";

    private Publication currentPublication = Publication.TopNews;
    private final LoaderManager.LoaderCallbacks<Response<ApiManager.ListResponse<Article>>> articleListLoaderCallbacks = new ArticleListLoaderCallbacks();

    @Override
    protected boolean onSpinnerItemSelected(String item) {
        Publication publication = Publication.getEnum(item);
        if (currentPublication == publication) {
            return false;
        }
        currentPublication = publication;
        return true;
    }

    @Override
    protected int getOptionsMenuResourceId() {
        return R.menu.article_list_options;
    }

    @Override
    protected CharSequence[] getSpinnerTextArray() {
        return getResources().getTextArray(R.array.action_publication_array);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_list;
    }

    @Override
    protected int getCurrentSpinnerSelectionIndex() {
        return currentPublication.ordinal();
    }

    @Override
    protected Bundle onRestartLoader(Bundle bundle) {
        Date beforeDate = new Date();
        boolean doResetAdapter = true;
        Publication publication = currentPublication;
        String query = null;

        RestartLoaderAction restartAction = RestartLoaderAction.valueOf(bundle.getString(RESTART_LOADER_ACTION));
        switch(restartAction) {
            case SearchQuery:
                query = bundle.getString(SEARCH_QUERY);
                Answers.getInstance().logCustom(new CustomEvent("Article Search").putCustomAttribute("query", query));
                break;
            case SearchMenuItemActionCollapse:
                break;
            case SwipeToLastItem:
                RealmRecyclerAdapter realmRecyclerAdapter = (RealmRecyclerAdapter) getRecyclerView().getAdapter();
                RealmResults<?> realmResults = realmRecyclerAdapter.getRealmResults();
                if (!realmResults.isEmpty()) {
                    RealmObject realmObject = realmResults.get(realmResults.size() - 1);
                    if (realmObject instanceof Article) {
                        beforeDate = ((Article)realmObject).getDate();
                    }
                }
                doResetAdapter = false;
                break;
            case SwipeToRefresh:
                break;
            case SpinnerItemSelected:
                String spinnerItem = bundle.getString(SPINNER_ITEM);
                publication = Publication.getEnum(spinnerItem);
                break;
            case FragmentResume:
                break;
        }

        Bundle loaderBundle = new Bundle();
        loaderBundle.putBoolean(ArticleListLoaderCallbacks.DO_RESET_ADAPTER, doResetAdapter);
        loaderBundle.putLong(ArticleListLoaderCallbacks.BEFORE_DATE, beforeDate.getTime());
        if (publication != null) {
            loaderBundle.putInt(ArticleListLoaderCallbacks.PUBLICATION, publication.ordinal());
        }
        if (query != null) {
            loaderBundle.putString(ArticleListLoaderCallbacks.QUERY, query);
        }

        return loaderBundle;
    }

    @Override
    protected LoaderManager.LoaderCallbacks<?> getLoaderCallbacks() {
        return articleListLoaderCallbacks;
    }

    @Override
    public String getEventName() {
        return "Article List";
    }

    @Override
    public Map<String, Object> getEventAttributes() {
        return null;
    }

    private class ArticleListLoaderCallbacks implements LoaderManager.LoaderCallbacks<Response<ApiManager.ListResponse<Article>>> {
        private static final String TAG = "ArticleListLoaderCallbacks";
        private static final String PUBLICATION = "PUBLICATION";
        private static final String BEFORE_DATE = "BEFORE_DATE";
        private static final String QUERY = "QUERY";
        private static final String DO_RESET_ADAPTER = "DO_RESET_ADAPTER";

        private RealmResults<Article> realmResults;
        private boolean doResetAdapter = false;
        private String query = null;

        public ArticleListLoaderCallbacks() {

        }

        @Override
        public Loader<Response<ApiManager.ListResponse<Article>>> onCreateLoader(int id, Bundle bundle) {
            Date beforeDate = null;
            Publication publication = null;
            query = null;
            if (bundle.containsKey(BEFORE_DATE)) {
                beforeDate = new Date(bundle.getLong(BEFORE_DATE));
            }
            if (bundle.containsKey(PUBLICATION)) {
                publication = Publication.values()[bundle.getInt(PUBLICATION)];
            }
            if (bundle.containsKey(QUERY)) {
                query = bundle.getString(QUERY);
            }
            this.doResetAdapter = bundle.getBoolean(DO_RESET_ADAPTER);
            return new ArticleListLoader(getApplicationContext(), beforeDate, publication, query);
        }

        @Override
        public void onLoadFinished(Loader<Response<ApiManager.ListResponse<Article>>> loader, Response<ApiManager.ListResponse<Article>> response) {
            // Query Realm (cannot do this on non main thread =D)
            if (realmResults == null || doResetAdapter) {
                Realm realm = RealmManager.getInstance(getApplicationContext()).getRealm();
                RealmQuery<Article> realmQuery = realm.where(Article.class);
                if (currentPublication != null && currentPublication != Publication.TopNews) {
                    realmQuery = realmQuery.equalTo("publication", currentPublication.getDisplayName());
                }

                boolean isResponseValid = response != null && response.body() != null && response.body().getResults() != null;
                if (query != null && isResponseValid && response.body().getResults().size() > 0) {
                    realmQuery = realmQuery.beginGroup();
                    for (int i = 0; i < response.body().getResults().size(); i++) {
                        Article article = response.body().getResults().get(i);
                        realmQuery = realmQuery.equalTo("guid", article.getGuid());
                        if (i < (response.body().getResults().size() - 1)) {
                            realmQuery = realmQuery.or();
                        }
                    }
                    realmQuery = realmQuery.endGroup();

                }

                if (isResponseValid && response.body().getResults().isEmpty() && query != null) {
                    realmResults = realmQuery.equalTo("guid", "9000").findAll();
                } else if (query != null) {
                    realmResults = realmQuery.findAllSorted("rank", Sort.ASCENDING);
                } else {
                    realmResults = realmQuery.findAllSorted("date", Sort.DESCENDING);
                }
            }

            if (doResetAdapter || getRecyclerView().getAdapter() == null) {
                getRecyclerView().setAdapter(new ArticleRecyclerAdapter(getActivity(), realmResults));
            }
            getRecyclerView().getAdapter().notifyDataSetChanged();

            // Stop refresher
            setRefreshing(false);

            // Reset loader
            getLoaderManager().destroyLoader(LOADER_ID);
        }

        @Override
        public void onLoaderReset(Loader<Response<ApiManager.ListResponse<Article>>> loader) {

        }
    }

    private static class ArticleListLoader extends AsyncTaskLoader<Response<ApiManager.ListResponse<Article>>> {
        private final Date beforeDate;
        private final Publication publication;
        private final String query;
        public ArticleListLoader(Context context, Date beforeDate, Publication publication, String query) {
            super(context);
            this.beforeDate = beforeDate;
            this.publication = publication;
            this.query = query;
        }

        @Override
        public Response<ApiManager.ListResponse<Article>> loadInBackground() {
            return ArticleManager.getInstance(getContext()).fetchArticles(beforeDate, publication, query);
        }
    }

    private static class ArticleRecyclerAdapter extends RealmRecyclerAdapter<Article> {
        private static final int VIEW_TYPE_ARTICLE = 0;
        private static final int VIEW_TYPE_PROGRESS_BAR = 1;
        private static final int VIEW_TYPE_NO_OBJECTS = 2;

        public ArticleRecyclerAdapter(Context context, RealmResults<Article> realmResults) {
            super(context, realmResults);
        }

        @Override
        public int getItemViewType(int position) {
            Article article = getItem(position);
            if (getRealmResults().isEmpty()) {
                return VIEW_TYPE_NO_OBJECTS;
            } else {
                if (article != null) {
                    return VIEW_TYPE_ARTICLE;
                } else {
                    return VIEW_TYPE_PROGRESS_BAR;
                }
            }
        }

        public Article getItem(int position) {
            if (position >= getRealmResults().size()) {
                return null;
            }
            return getRealmResults().get(position);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            ViewHolder viewHolder = null;
            View view = null;
            LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            switch (viewType) {
                case VIEW_TYPE_ARTICLE:
                    view = layoutInflater.inflate(R.layout.list_item_article, viewGroup, false);
                    break;
                case VIEW_TYPE_PROGRESS_BAR:
                    view = layoutInflater.inflate(R.layout.list_item_progress_bar, viewGroup, false);
                    break;
                case VIEW_TYPE_NO_OBJECTS:
                    view = layoutInflater.inflate(R.layout.list_item_no_objects, viewGroup, false);
                    break;
            }
            if (view != null) {
                viewHolder = new ViewHolder(view);
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, final int position) {
            if (getRealmResults().isEmpty()) {
                View view = viewHolder.itemView;
                if (view instanceof TextView) {
                    ((TextView)view).setText(R.string.no_articles);
                }
            } else {
                final Article article = getItem(position);
                if (article == null) {
                    return;
                }
                View view = viewHolder.itemView;
                if (view instanceof ArticleListItemLayout) {
                    ((ArticleListItemLayout)view).bindArticle(getContext(), article, position, true);
                }
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext().getApplicationContext(), ArticleDetailActivity.class);
                        intent.putExtra(RealmWebFragment.EXTRA_GUID, article.getGuid());
                        getContext().startActivity(intent);

                        // Save
                        Realm realm = RealmManager.getInstance(getContext().getApplicationContext()).getRealm();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                article.setIsRead(true);
                            }
                        });

                        notifyItemChanged(position);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return getRealmResults().size() + 1;
        }
    }
}
