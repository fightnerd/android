package com.ez3softworks.fightnerd.controllers;

import android.content.Intent;
import android.os.Bundle;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Video;
import com.ez3softworks.fightnerd.stores.RealmManager;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

public class VideoDetailFragment extends RealmWebFragment<Video> {
	public static final String TAG = "VideoDetailFragment";

    public static VideoDetailFragment newInstance(String guid) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_GUID, guid);

        VideoDetailFragment fragment = new VideoDetailFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected Class<Video> getRealmObjectClass() {
        return Video.class;
    }

    @Override
    protected String getContentUrl() {
        Video video = getRealmObject();
        return video.getContentUrl();
    }

    @Override
    protected String getActionBarTitle() {
        Video video = getRealmObject();
        return video.getChannel();
    }

    @Override
    protected Intent getShareIntent() {
        Video video = getRealmObject();
        if (video == null) {
            return null;
        }
        Intent shareIntent = new Intent();
        String text = String.format("%s", video.getSourceUrl());
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, video.getTitle());
        shareIntent.setType("text/plain");
        return shareIntent;
    }

    @Override
    protected boolean isFavorite() {
        Video video = getRealmObject();
        return video.isFavorite();
    }

    @Override
    protected void onPressFavoriteMenuItem() {
        Realm realm = RealmManager.getInstance(getContext().getApplicationContext()).getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Video video = getRealmObject();
                video.setIsFavorite(!video.isFavorite());
            }
        });
    }

    @Override
    protected int getOptionsMenuResourceId() {
        return R.menu.video_detail_options;
    }

    @Override
    public String getEventName() {
        return "Video Detail";
    }

    @Override
    public Map<String, Object> getEventAttributes() {
        Video video = getRealmObject();
        Map<String, Object> eventAttributes = new HashMap<>();
        eventAttributes.put("sourceUrl", video.getSourceUrl());
        eventAttributes.put("channel", video.getChannel());
        eventAttributes.put("guid", video.getGuid());
        eventAttributes.put("title", video.getTitle());
        eventAttributes.put("contentUrl", video.getContentUrl());
        return eventAttributes;
    }

    @Override
    protected String getSourceUrl() {
        Video video = getRealmObject();
        return video.getSourceUrl();
    }
}
