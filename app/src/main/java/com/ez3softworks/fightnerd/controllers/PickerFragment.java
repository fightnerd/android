package com.ez3softworks.fightnerd.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.ez3softworks.fightnerd.R;

public class PickerFragment extends DialogFragment {
	public static final String EXTRA_SELECTED_INDEX = "com.ez3softworks.fightnerd.selectedindex";
	public static final String EXTRA_TEXT_ARRAY_RESOURCE_ID = "com.ez3softworks.fightnerd.textarrayresourceid";
	
	private Spinner mSpinner;
	
	public static PickerFragment newInstance(int selectedIndex, int textArrayResourceId) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_SELECTED_INDEX, selectedIndex);
		bundle.putSerializable(EXTRA_TEXT_ARRAY_RESOURCE_ID, textArrayResourceId);
		
		PickerFragment pickerFragment = new PickerFragment();
		pickerFragment.setArguments(bundle);
		return pickerFragment;
	}
	
	@Override
    public Dialog onCreateDialog(Bundle bundle) {
		int selectedIndex = getArguments().getInt(EXTRA_SELECTED_INDEX);
		int textArrayResourceId = getArguments().getInt(EXTRA_TEXT_ARRAY_RESOURCE_ID);
		
		View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_picker, null);
		
		mSpinner = (Spinner)view.findViewById(R.id.dialog_picker_spinner);
		ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(getActivity(), textArrayResourceId, android.R.layout.simple_spinner_item);
		arrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
		mSpinner.setAdapter(arrayAdapter);
		mSpinner.setSelection(selectedIndex);
		mSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view,
					int position, long id) {
				getArguments().putSerializable(EXTRA_SELECTED_INDEX, position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {				
			}
		});
		
		DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				sendResult(Activity.RESULT_OK);
			}
		};
		AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
									.setView(view)
									.setTitle(R.string.choose_publication)
									.setPositiveButton(android.R.string.ok, onClickListener)
									.create();
        return alertDialog;
    }
	
	private void sendResult(int resultCode) {
		if (getTargetFragment() == null) {
			return;
		}
		
		Intent intent = new Intent();
		intent.putExtra(EXTRA_SELECTED_INDEX, mSpinner.getSelectedItemPosition());
		getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
	}
}
