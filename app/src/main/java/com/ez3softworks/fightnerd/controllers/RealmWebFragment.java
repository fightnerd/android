package com.ez3softworks.fightnerd.controllers;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.stores.AdManager;
import com.ez3softworks.fightnerd.stores.RealmManager;

import io.realm.Realm;
import io.realm.RealmObject;

public abstract class RealmWebFragment<T extends RealmObject> extends BaseFragment {
	public static final String TAG = "RealmWebFragment";
	public static final String EXTRA_GUID = "EXTRA_GUID";

	private MenuItem favoriteMenuItem;
	private WebView webView;
	private ShareActionProvider shareActionProvider;
	private T realmObject = null;
    private boolean didShowInterstitial = false;


    protected final T getRealmObject() {
        Bundle bundle = getArguments();
        String guid = bundle.getString(EXTRA_GUID);
		if (realmObject == null) {
            Realm realm = RealmManager.getInstance(getContext().getApplicationContext()).getRealm();
            realmObject = realm.where(getRealmObjectClass()).equalTo("guid", guid).findFirst();
		}
		return realmObject;
	}

	@Override
	public void onResume() {
		super.onResume();
        if (!didShowInterstitial) {
            didShowInterstitial = AdManager.getInstance(getApplicationContext()).showInterstitial();
        }
		onStartEvent();
		webView.loadUrl(getContentUrl());
	}

	@Override
	public void onPause() {
		super.onPause();
		webView.onPause();
		webView.loadUrl("");
		onStopEvent();
	}

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setRetainInstance(true);
		
		// Add options menu, so user can share article
		setHasOptionsMenu(true);
		
		// Set action bar title to publication
		((BaseFragmentActivity)getActivity()).setTitle(getActionBarTitle());
//		getSherlockActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
		View view = layoutInflater.inflate(R.layout.fragment_web_detail,  viewGroup, false);
        view.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.white));

		final ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.progress_bar);
//		progressBar.getProgressDrawable().setColorFilter(getResources().getColor(R.color.cagejunkie_red), Mode.SRC_ATOP);
		progressBar.setMax(100);
		
		webView = (WebView)view.findViewById(R.id.web_view);
		webView.setAlpha(0f);
        webView.setVisibility(View.VISIBLE);
        webView.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.white));
		webView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
		}
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				int animationDuration = 500;
				if (isAdded()) {
					animationDuration = getResources().getInteger(android.R.integer.config_longAnimTime);
				}
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
					webView.animate()
							.alpha(1f)
							.setDuration(animationDuration)
							.setListener(null);
				} else {
					webView.setAlpha(1f);
				}
			}
		});
		webView.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged(android.webkit.WebView webView, int progress) {
				if (progress == 100) {
					progressBar.setVisibility(View.INVISIBLE);
				} else {
					progressBar.setVisibility(View.VISIBLE);
					progressBar.setProgress(progress);
				}
			}

			@Override
			public void onReceivedTitle(android.webkit.WebView webView, String title) {
            }
        });
		return view;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(getOptionsMenuResourceId(), menu);

        // Set up ShareActionProvider
        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
		shareActionProvider = (ShareActionProvider)MenuItemCompat.getActionProvider(shareItem);
        shareActionProvider.setShareIntent(getShareIntent());

        // Favorites
		favoriteMenuItem = menu.findItem(R.id.menu_item_favorite);
		favoriteMenuItem.setIcon(isFavorite() ? ContextCompat.getDrawable(getContext(), R.drawable.ic_action_heart) : ContextCompat.getDrawable(getContext(), R.drawable.ic_action_heart_o));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
			case android.R.id.home:
				getActivity().onBackPressed();
				break;
			case R.id.menu_item_favorite:
				onPressFavoriteMenuItem();
                favoriteMenuItem.setIcon(isFavorite() ? ContextCompat.getDrawable(getContext(), R.drawable.ic_action_heart) : ContextCompat.getDrawable(getContext(), R.drawable.ic_action_heart_o));
				break;
			case R.id.menu_item_web:
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getSourceUrl()));
				getActivity().startActivity(intent);
                break;
		}
		return super.onOptionsItemSelected(menuItem);
	}
	
	protected abstract String getActionBarTitle();

	protected abstract String getContentUrl();

	protected abstract String getSourceUrl();

    protected abstract Class<T> getRealmObjectClass();

    protected abstract Intent getShareIntent();

    protected abstract boolean isFavorite();

    protected abstract void onPressFavoriteMenuItem();

    protected abstract int getOptionsMenuResourceId();

}
