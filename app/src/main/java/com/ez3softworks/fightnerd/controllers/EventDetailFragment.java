package com.ez3softworks.fightnerd.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Event;
import com.ez3softworks.fightnerd.models.Fight;
import com.ez3softworks.fightnerd.stores.AdManager;
import com.ez3softworks.fightnerd.stores.RealmManager;
import com.ez3softworks.fightnerd.utilities.LocaleUtilities;
import com.ez3softworks.fightnerd.views.EventListItemLayout;
import com.ez3softworks.fightnerd.views.FightListItemLayout;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;


public class EventDetailFragment extends BaseFragment {
	private static final String TAG = "EventDetailFragment";
    public static final String EXTRA_GUID = "EXTRA_GUID";

    private ShareActionProvider shareActionProvider;
    private Event event = null;
    private boolean didShowInterstitial = false;

    public static EventDetailFragment newInstance(String guid) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_GUID, guid);

        EventDetailFragment fragment = new EventDetailFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    protected final Event getEvent() {
        Bundle bundle = getArguments();
        String guid = bundle.getString(EXTRA_GUID);
        if (event == null) {
            Realm realm = RealmManager.getInstance(getContext().getApplicationContext()).getRealm();
            event = realm.where(Event.class).equalTo("guid", guid).findFirst();
        }
        return event;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!didShowInterstitial) {
            didShowInterstitial = AdManager.getInstance(getApplicationContext()).showInterstitial();
        }
        onStartEvent();
    }

    @Override
    public void onPause() {
        super.onPause();
        onStopEvent();
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);

        // Add options menu, so user can share article
        setHasOptionsMenu(true);

        // Set action bar title to publication
        ((BaseFragmentActivity)getActivity()).setTitle(getEvent().getTitle());
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = layoutInflater.inflate(R.layout.fragment_list,  viewGroup, false);

        // Recycler view
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new FightRecyclerAdapter(getContext(), getEvent()));

        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setEnabled(false);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.event_detail_options, menu);

        // Set up ShareActionProvider
        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        shareActionProvider.setShareIntent(getShareIntent());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    protected Intent getShareIntent() {
        Event event = getEvent();
        if (event == null) {
            return null;
        }
        Intent shareIntent = new Intent();
        String text;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM d, yyyy", LocaleUtilities.DEFAULT_LOCALE);
        if (event.isTimeValid()) {
            text = String.format("%s - %s on %s at %s",
                    event.getTitle(),
                    event.getSubtitle(),
                    dateFormat.format(event.getDate()),
                    new SimpleDateFormat("hh:mm a", LocaleUtilities.DEFAULT_LOCALE).format(event.getDate()));
        } else {
            text = String.format("%s - %s is on %s", event.getTitle(), event.getSubtitle(), dateFormat.format(event.getDate()));
        }
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, event.getTitle() + " - " + event.getSubtitle());
        shareIntent.setType("text/plain");
        return shareIntent;
    }

    @Override
    public String getEventName() {
        return "Event Detail";
    }

    @Override
    public Map<String, Object> getEventAttributes() {
        Event event = getEvent();
        Map<String, Object> eventAttributes = new HashMap<>();
        eventAttributes.put("subtitle", event.getSubtitle());
        eventAttributes.put("guid", event.getGuid());
        eventAttributes.put("title", event.getTitle());
        return eventAttributes;
    }

    private static class FightRecyclerAdapter extends RecyclerView.Adapter<FightRecyclerAdapter.ViewHolder> {
        private static final int VIEW_TYPE_EVENT = 0;
        private static final int VIEW_TYPE_EVENT_ODDS = 1;
        private static final int VIEW_TYPE_FIGHT = 2;
        private static final int VIEW_TYPE_FIGHT_ODDS = 3;
        private static final int VIEW_TYPE_NO_OBJECTS = 4;

        private final Event event;
        private final Context context;

        public FightRecyclerAdapter(Context context, Event event) {
            this.context = context;
            this.event = event;
        }

        @Override
        public int getItemViewType(int position) {
            Fight fight = getItem(position);
            if (getFights().isEmpty()) {
                return VIEW_TYPE_NO_OBJECTS;
            } else if (position == 0) {
                return Fight.areOddsValid(fight) ? VIEW_TYPE_EVENT_ODDS : VIEW_TYPE_EVENT;
            } else {
                return Fight.areOddsValid(fight) ? VIEW_TYPE_FIGHT_ODDS : VIEW_TYPE_FIGHT;
            }
        }

        public Fight getItem(int position) {
            if (position >= getFights().size()) {
                return null;
            }
            return getFights().get(position);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            ViewHolder viewHolder = null;
            View view = null;
            LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            switch (viewType) {
                case VIEW_TYPE_EVENT:
                    view = layoutInflater.inflate(R.layout.list_item_event, viewGroup, false);
                    break;
                case VIEW_TYPE_EVENT_ODDS:
                    view = layoutInflater.inflate(R.layout.list_item_event_odds, viewGroup, false);
                    break;
                case VIEW_TYPE_FIGHT:
                    view = layoutInflater.inflate(R.layout.list_item_fight, viewGroup, false);
                    break;
                case VIEW_TYPE_FIGHT_ODDS:
                    view = layoutInflater.inflate(R.layout.list_item_fight_odds, viewGroup, false);
                    break;
                case VIEW_TYPE_NO_OBJECTS:
                    view = layoutInflater.inflate(R.layout.list_item_no_objects, viewGroup, false);
                    break;
            }
            if (view != null) {
                viewHolder = new ViewHolder(view);
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int position) {
            if (getFights().isEmpty()) {
                View view = viewHolder.itemView;
                if (view instanceof TextView) {
                    ((TextView)view).setText(R.string.no_fights);
                }
            } else {
                final Fight fight = getItem(position);
                if (fight == null) {
                    return;
                }
                View view = viewHolder.itemView;
                if (view instanceof EventListItemLayout) {
                    ((EventListItemLayout)view).bind(getContext(), event);
                } else if (view instanceof FightListItemLayout) {
                    ((FightListItemLayout)view).bind(getContext(), fight);
                }
            }
        }

        protected RealmList<Fight> getFights() {
            return getEvent().getFights();
        }

        @Override
        public int getItemCount() {
            return getFights().isEmpty() ? 1 : getFights().size();
        }

        public Context getContext() {
            return context;
        }

        public Event getEvent() {
            return event;
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(View itemView) {
                super(itemView);
            }
        }

    }
}
