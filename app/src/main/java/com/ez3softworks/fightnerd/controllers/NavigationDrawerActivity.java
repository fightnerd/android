package com.ez3softworks.fightnerd.controllers;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.views.NavigationTitleListItemLayout;
import com.google.android.gms.ads.MobileAds;
import java.util.Arrays;
import java.util.List;

public class NavigationDrawerActivity extends BaseFragmentActivity {
    private static final String SAVE_FRAGMENT_TYPE = "SAVE_FRAGMENT_TYPE";
    private static final String EXTRA_FRAGMENT_TYPE = "EXTRA_FRAGMENT_TYPE";
    private static final String ACTION_LAUNCH_FRAGMENT = "ACTION_LAUNCH_FRAGMENT";

    private DrawerLayout drawerLayout;
    private Spinner spinner;
	private ActionBarDrawerToggle actionBarDrawerToggle;
	private ListView drawerListView;
	private static FragmentType currentFragmentType = FragmentType.Invalid;

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getAction().equals(Intent.ACTION_SEARCH)) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragment = fragmentManager.findFragmentByTag(currentFragmentType.name());
            if (fragment instanceof ListFragment) {
                ((ListFragment)fragment).handleSearchQuery(intent.getStringExtra(SearchManager.QUERY));
            }

        }
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_navigation_drawer);

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerListView = (ListView) findViewById(R.id.drawer_list_view);

        // Nav bar
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        actionBarDrawerToggle = new ActionBarDrawerToggle(
				this,
				drawerLayout,
				R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
			}
		};
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

		// Set the adapter for the list view
		final NavigationTitleAdapter navigationTitleAdapter = new NavigationTitleAdapter(this, Arrays.asList(getResources().getStringArray(R.array.navigation_title_array)));
		drawerListView.setAdapter(navigationTitleAdapter);

		// Set up a click listener
		drawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                switch (position) {
                    case 0:
                        loadFragment(FragmentType.ArticleList, true);
                        break;
                    case 1:
                        loadFragment(FragmentType.EventList, true);
                        break;
                    case 2:
                        loadFragment(FragmentType.FighterList, true);
                        break;
                    case 3:
                        loadFragment(FragmentType.RankingList, true);
                        break;
                    case 4:
                        loadFragment(FragmentType.Store, true);
                        break;
                    default:
                        break;
                }
                navigationTitleAdapter.notifyDataSetChanged();
                drawerLayout.closeDrawer(drawerListView);
            }
        });

        // Load initial fragment
        Intent intent = getIntent();
        if (currentFragmentType.equals(FragmentType.Invalid)) {
            currentFragmentType = FragmentType.ArticleList;
        }
        if ((intent != null) && (intent.getAction() != null) && intent.getAction().equals(ACTION_LAUNCH_FRAGMENT)) {
            currentFragmentType = FragmentType.values()[intent.getIntExtra(
                    EXTRA_FRAGMENT_TYPE,
                    FragmentType.ArticleList.ordinal()
            )];
        }
        loadFragment(currentFragmentType, false, true);

		// Ads...?
        MobileAds.initialize(getApplicationContext(), getString(R.string.admob_banner_api_key));
        final AdView adView = (AdView) findViewById(R.id.ad_view);
		final AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                adView.loadAd(adRequest);
                LogUtilities.d("GOOGLE ERROR", errorCode + "");
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                LogUtilities.d("GOOGLE MOBILE AD", "AD JUST LOADED!!!");
            }
        });
	}

    @Override
    protected void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        actionBarDrawerToggle.onConfigurationChanged(configuration);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return actionBarDrawerToggle.onOptionsItemSelected(menuItem) || super.onOptionsItemSelected(menuItem);
    }

    private void loadFragment(FragmentType fragmentType, boolean doReplace) {
		loadFragment(fragmentType, doReplace, false);
	}

	private void loadFragment(FragmentType fragmentType, boolean doReplace, boolean doForce) {
		if (!doForce && currentFragmentType.equals(fragmentType)) {
			return;
		}
		FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment fragment = fragmentManager.findFragmentByTag(fragmentType.name());
		if (fragment == null) {
			if (fragmentType.equals(FragmentType.ArticleList)) {
				fragment = new ArticleListFragment();
			}
//            else if (fragmentType.equals(FragmentType.FighterList)) {
//				fragment = new FighterListFragment();
//			} else if (fragmentType.equals(FragmentType.RankingList)) {
//				fragment = new RankingListFragment();
//			} else if (fragmentType.equals(FragmentType.EventList)) {
//				fragment = new UpcomingEventListFragment();
//			} else if (fragmentType.equals(FragmentType.Store)) {
//				fragment = new StoreFragment();
//			} else {
//
//			}
		}
		if (fragment != null) {
			if (doReplace || fragmentManager.findFragmentByTag(fragmentType.name())!=null) {
                ActionBar actionBar = getSupportActionBar();
                if (actionBar != null) {
                    actionBar.setTitle("");
                }
				fragmentManager.beginTransaction().replace(R.id.frame_layout, fragment, fragmentType.name()).commit();
			} else {
				fragmentManager.beginTransaction().add(R.id.frame_layout, fragment, fragmentType.name()).commit();
			}
			currentFragmentType = fragmentType;
		}
	}


	private class NavigationTitleAdapter extends BaseAdapter {
		private static final int VIEW_TYPE_ARTICLE_LIST = 0;
		private static final int VIEW_TYPE_EVENT_LIST = 1;
		private static final int VIEW_TYPE_FIGHTER_LIST = 2;
		private static final int VIEW_TYPE_RANKING_LIST = 3;
		private static final int VIEW_TYPE_STORE = 4;

		private final Context context;
		private final List<String> navigationTitles;

		public NavigationTitleAdapter(Context context, List<String> navigationTitles) {
			this.context = context;
			this.navigationTitles = navigationTitles;
		}

		@Override
		public int getCount() {
			return navigationTitles.size();
		}

		@Override
		public Object getItem(int position) {
			return navigationTitles.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup viewGroup) {
			View view = convertView;
			if (view == null) {
				view = newView(position, viewGroup);
			}
			bindView(position, view);
			return view;
		}

		@Override
		public int getViewTypeCount() {
			return 6;
		}

		@Override
		public int getItemViewType(int position) {
			return position;
		}

		public View newView(int position, ViewGroup viewGroup) {
			View newView = null;
			LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			newView = layoutInflater.inflate(R.layout.list_item_navigation_title, viewGroup, false);
			return newView;
		}

		public void bindView(int position, View view) {
			int itemViewType = getItemViewType(position);
			String title = navigationTitles.get(position);
			Drawable drawable;
			switch(itemViewType) {
				case VIEW_TYPE_ARTICLE_LIST:
					drawable = ContextCompat.getDrawable(context, R.drawable.ic_newspaper);
					break;
				case VIEW_TYPE_FIGHTER_LIST:
					drawable = ContextCompat.getDrawable(context, R.drawable.ic_fighters);
					break;
				case VIEW_TYPE_RANKING_LIST:
					drawable = ContextCompat.getDrawable(context, R.drawable.ic_rankings);
					break;
				case VIEW_TYPE_EVENT_LIST:
					drawable = ContextCompat.getDrawable(context, R.drawable.ic_calendar);
					break;
				default:
					drawable = null;
					break;
			}
			((NavigationTitleListItemLayout)view).loadNavigationTitle(context, title, drawable);
			setViewBackgroundColor(view, itemViewType);
		}

		private void setViewBackgroundColor(View view, int itemViewType) {
			view.setBackgroundColor(ContextCompat.getColor(context, R.color.cagejunkie_clear));
			switch(itemViewType) {
				case VIEW_TYPE_ARTICLE_LIST:
					if (currentFragmentType.equals(FragmentType.ArticleList)) {
						view.setBackgroundColor(ContextCompat.getColor(context, R.color.cagejunkie_red));
					}
					break;
				case VIEW_TYPE_FIGHTER_LIST:
					if (currentFragmentType.equals(FragmentType.FighterList)) {
						view.setBackgroundColor(ContextCompat.getColor(context, R.color.cagejunkie_red));
					}
					break;
				case VIEW_TYPE_RANKING_LIST:
					if (currentFragmentType.equals(FragmentType.RankingList)) {
						view.setBackgroundColor(ContextCompat.getColor(context, R.color.cagejunkie_red));
					}
					break;
				case VIEW_TYPE_EVENT_LIST:
					if (currentFragmentType.equals(FragmentType.EventList)) {
						view.setBackgroundColor(ContextCompat.getColor(context, R.color.cagejunkie_red));
					}
					break;
				case VIEW_TYPE_STORE:
					if (currentFragmentType.equals(FragmentType.Store)) {
						view.setBackgroundColor(ContextCompat.getColor(context, R.color.cagejunkie_red));
					}
					break;
				default:
					break;
			}
		}
	}

	public enum FragmentType {
		ArticleList,
		FighterList,
		RankingList,
		EventList,
		Store,
		Invalid,
	}
}
