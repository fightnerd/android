package com.ez3softworks.fightnerd.controllers;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.stores.AdManager;
import com.ez3softworks.fightnerd.stores.GoogleCloudMessagingManager;
import com.ez3softworks.fightnerd.utilities.StringUtilities;
import com.ez3softworks.fightnerd.utilities.TypefaceUtilities;
import com.ez3softworks.fightnerd.views.CustomTypefaceSpan;
import com.google.android.gms.ads.AdView;

public abstract class BaseFragmentActivity extends AppCompatActivity {
	private static final String TAG = "BaseFragmentActivity";
	private boolean didShowInterstitial = false;
	private GoogleCloudMessagingManager googleCloudMessagingManager;

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		googleCloudMessagingManager = new GoogleCloudMessagingManager(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		googleCloudMessagingManager.connect();
//		Log.i(TAG, this+":  "+"doShowInterstitial(): "+doShowInterstitial()+"  didShowInterstitial(): "+didShowInterstitial);
//		if (AdManager.getInstance(this).areAdsEnabled() && !didShowInterstitial && doShowInterstitial()) {
//			mRevMob.showFullscreen(this);
//			didShowInterstitial = true;
//		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	protected void setTitle(String title) {
		CustomTypefaceSpan typefaceSpan = new CustomTypefaceSpan(TypefaceUtilities.getTypeface(this, TypefaceUtilities.ROBOTO_LIGHT));
		SpannableString spannableString = new SpannableString(title);
		int styleResourceId = R.style.TextAppearance_ActionBarTitle_CageJunkie;
		spannableString = StringUtilities.setSpan(this, spannableString, title, title, styleResourceId);
		spannableString = StringUtilities.setSpan(this, spannableString, title, title, typefaceSpan);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(spannableString);
        }
	}
}
