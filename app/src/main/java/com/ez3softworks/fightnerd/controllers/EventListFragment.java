package com.ez3softworks.fightnerd.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Article;
import com.ez3softworks.fightnerd.models.Event;
import com.ez3softworks.fightnerd.stores.ApiManager;
import com.ez3softworks.fightnerd.stores.FighterManager;
import com.ez3softworks.fightnerd.stores.RealmManager;
import com.ez3softworks.fightnerd.views.ArticleListItemLayout;
import com.ez3softworks.fightnerd.views.EventListItemLayout;
import com.ez3softworks.fightnerd.views.EventOddsListItemLayout;
import com.ez3softworks.fightnerd.views.ImagelessArticleListItemLayout;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Response;


public class EventListFragment extends ListFragment {
	private static final String TAG = "EventListFragment";

    private final LoaderManager.LoaderCallbacks<Response<ApiManager.ListResponse<Event>>> eventListLoaderCallbacks = new EventListLoaderCallbacks();
    private final DateTime afterDate = (new DateTime()).minusDays(2);

    @Override
    protected boolean onSpinnerItemSelected(String item) {
        return false;
    }

    @Override
    protected int getOptionsMenuResourceId() {
        return -1;
    }

    @Override
    protected CharSequence[] getSpinnerTextArray() {
        return null;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_list;
    }

    @Override
    protected int getCurrentSpinnerSelectionIndex() {
        return -1;
    }

    @Override
    protected Bundle onRestartLoader(Bundle bundle) {
        RestartLoaderAction restartAction = RestartLoaderAction.valueOf(bundle.getString(RESTART_LOADER_ACTION));
        switch(restartAction) {
            case SwipeToLastItem:
                return null;
        }

        Bundle loaderBundle = new Bundle();
        loaderBundle.putBoolean(EventListLoaderCallbacks.DO_RESET_ADAPTER, true);
        loaderBundle.putLong(EventListLoaderCallbacks.AFTER_DATE, afterDate.toDate().getTime());
        return loaderBundle;
    }

    @Override
    protected LoaderManager.LoaderCallbacks<?> getLoaderCallbacks() {
        return eventListLoaderCallbacks;
    }

    @Override
    public String getEventName() {
        return "Event List";
    }

    @Override
    public Map<String, Object> getEventAttributes() {
        return null;
    }

    private class EventListLoaderCallbacks implements LoaderManager.LoaderCallbacks<Response<ApiManager.ListResponse<Event>>> {
        private static final String TAG = "EventListLoaderCallbacks";
        private static final String AFTER_DATE = "AFTER_DATE";
        private static final String DO_RESET_ADAPTER = "DO_RESET_ADAPTER";

        private RealmResults<Event> realmResults;
        private boolean doResetAdapter = false;
        private String query = null;
        private Date afterDate = new Date();

        public EventListLoaderCallbacks() {

        }

        @Override
        public Loader<Response<ApiManager.ListResponse<Event>>> onCreateLoader(int id, Bundle bundle) {
            afterDate = null;
            if (bundle.containsKey(AFTER_DATE)) {
                afterDate = new Date(bundle.getLong(AFTER_DATE));
            }
            this.doResetAdapter = bundle.getBoolean(DO_RESET_ADAPTER);
            getRecyclerView().setAdapter(new RecyclerView.Adapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                    return null;
                }

                @Override
                public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

                }

                @Override
                public int getItemCount() {
                    return 0;
                }
            });
            return new EventListLoader(getApplicationContext(), afterDate);
        }

        @Override
        public void onLoadFinished(Loader<Response<ApiManager.ListResponse<Event>>> loader, Response<ApiManager.ListResponse<Event>> response) {
            // Query Realm (cannot do this on non main thread =D)
            if (realmResults == null || doResetAdapter) {
                Realm realm = RealmManager.getInstance(getApplicationContext()).getRealm();
                RealmQuery<Event> realmQuery = realm.where(Event.class).greaterThan("date", afterDate);
                realmResults = realmQuery.findAllSorted("date", Sort.ASCENDING);
            }

            if (doResetAdapter || getRecyclerView().getAdapter() == null) {
                getRecyclerView().setAdapter(new EventRecyclerAdapter(getActivity(), realmResults));
            }
            getRecyclerView().getAdapter().notifyDataSetChanged();

            // Reset loader
            getLoaderManager().destroyLoader(LOADER_ID);

            // Stop refresher
            setRefreshing(false);

        }

        @Override
        public void onLoaderReset(Loader<Response<ApiManager.ListResponse<Event>>> loader) {

        }
    }

    private static class EventListLoader extends AsyncTaskLoader<Response<ApiManager.ListResponse<Event>>> {
        private final Date afterDate;
        public EventListLoader(Context context, Date afterDate) {
            super(context);
            this.afterDate = afterDate;
        }

        @Override
        public Response<ApiManager.ListResponse<Event>> loadInBackground() {
            return FighterManager.getInstance(getContext()).fetchEvents(afterDate);
        }
    }

    private static class EventRecyclerAdapter extends RealmRecyclerAdapter<Event> {
        private static final int VIEW_TYPE_EVENT = 0;
        private static final int VIEW_TYPE_EVENT_ODDS = 1;
        private static final int VIEW_TYPE_NO_OBJECTS = 2;

        public EventRecyclerAdapter(Context context, RealmResults<Event> realmResults) {
            super(context, realmResults);
        }

        @Override
        public int getItemViewType(int position) {
            Event event = getItem(position);
            RealmResults<Event> realmResults = getRealmResults();
            if (realmResults == null || realmResults.isEmpty()) {
                return VIEW_TYPE_NO_OBJECTS;
            } else {
                return Event.areOddsValid(event) ? VIEW_TYPE_EVENT_ODDS : VIEW_TYPE_EVENT;
            }
        }

        public Event getItem(int position) {
            RealmResults<Event> realmResults = getRealmResults();
            if (realmResults == null || position >= realmResults.size()) {
                return null;
            }
            return realmResults.get(position);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            ViewHolder viewHolder = null;
            View view = null;
            LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            switch (viewType) {
                case VIEW_TYPE_EVENT:
                    view = layoutInflater.inflate(R.layout.list_item_event, viewGroup, false);
                    break;
                case VIEW_TYPE_EVENT_ODDS:
                    view = layoutInflater.inflate(R.layout.list_item_event_odds, viewGroup, false);
                    break;
                case VIEW_TYPE_NO_OBJECTS:
                    view = layoutInflater.inflate(R.layout.list_item_no_objects, viewGroup, false);
                    break;
            }
            if (view != null) {
                viewHolder = new ViewHolder(view);
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int position) {
            RealmResults<Event> realmResults = getRealmResults();
            if (realmResults == null || realmResults.isEmpty()) {
                View view = viewHolder.itemView;
                if (view instanceof TextView) {
                    ((TextView)view).setText(R.string.no_events);
                }
            } else {
                final Event event = getItem(position);
                if (event == null) {
                    return;
                }
                View view = viewHolder.itemView;
                if (view instanceof EventOddsListItemLayout) {
                    ((EventOddsListItemLayout)view).bind(getContext(), event);
                } else if (view instanceof EventListItemLayout) {
                    ((EventListItemLayout)view).bind(getContext(), event);
                }
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext().getApplicationContext(), EventDetailActivity.class);
                        intent.putExtra(RealmWebFragment.EXTRA_GUID, event.getGuid());
                        getContext().startActivity(intent);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            RealmResults<Event> realmResults = getRealmResults();
            if (realmResults == null) {
                return 0;
            }
            return realmResults.isEmpty() ? 1 : realmResults.size();
        }
    }
}
