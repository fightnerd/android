package com.ez3softworks.fightnerd.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.SectionIndexer;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Fighter;
import com.ez3softworks.fightnerd.stores.ApiManager;
import com.ez3softworks.fightnerd.stores.FighterManager;
import com.ez3softworks.fightnerd.stores.RealmManager;
import com.ez3softworks.fightnerd.views.FighterListItemLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Response;


public class FighterListFragment extends ListFragment {
	private static final String TAG = "FighterListFragment";

    private SortOrder currentSortOrder = SortOrder.Name;
    private final LoaderManager.LoaderCallbacks<Response<ApiManager.ListResponse<Fighter>>> fighterListLoaderCallbacks = new FighterListLoaderCallbacks();

    @Override
    protected boolean onSpinnerItemSelected(String item) {
        SortOrder sortOrder = SortOrder.getEnum(item);
        if (currentSortOrder == sortOrder) {
            return false;
        }
        currentSortOrder = sortOrder;
        return true;
    }

    @Override
    protected int getOptionsMenuResourceId() {
        return R.menu.fighter_list_options;
    }

    @Override
    protected CharSequence[] getSpinnerTextArray() {
        return getResources().getTextArray(R.array.action_fighter_sort_array);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_fighter_list;
    }

    @Override
    protected int getCurrentSpinnerSelectionIndex() {
        return currentSortOrder.ordinal();
    }

    @Override
    protected Bundle onRestartLoader(Bundle bundle) {
        boolean doResetAdapter = true;
        SortOrder sortOrder = currentSortOrder;
        String query = null;

        RestartLoaderAction restartAction = RestartLoaderAction.valueOf(bundle.getString(RESTART_LOADER_ACTION));
        switch(restartAction) {
            case SearchQuery:
                query = bundle.getString(SEARCH_QUERY);
                Answers.getInstance().logCustom(new CustomEvent("Fighter Search").putCustomAttribute("query", query));
                break;
            case SearchMenuItemActionCollapse:
                break;
            case SwipeToLastItem:
                return null;
            case SwipeToRefresh:
                break;
            case SpinnerItemSelected:
                String spinnerItem = bundle.getString(SPINNER_ITEM);
                sortOrder = SortOrder.getEnum(spinnerItem);
                break;
            case FragmentResume:
                break;
        }

        Bundle loaderBundle = new Bundle();
        loaderBundle.putBoolean(FighterListLoaderCallbacks.DO_RESET_ADAPTER, doResetAdapter);
        if (query != null) {
            loaderBundle.putString(FighterListLoaderCallbacks.QUERY, query);
        }

        return loaderBundle;
    }

    @Override
    protected LoaderManager.LoaderCallbacks<?> getLoaderCallbacks() {
        return fighterListLoaderCallbacks;
    }

    @Override
    public String getEventName() {
        return "Fighter List";
    }

    @Override
    public Map<String, Object> getEventAttributes() {
        return null;
    }

    private class FighterListLoaderCallbacks implements LoaderManager.LoaderCallbacks<Response<ApiManager.ListResponse<Fighter>>> {
        private static final String TAG = "FighterListLoaderCallbacks";
        private static final String QUERY = "QUERY";
        private static final String DO_RESET_ADAPTER = "DO_RESET_ADAPTER";

        private RealmResults<Fighter> realmResults;
        private boolean doResetAdapter = false;
        private String query = null;

        public FighterListLoaderCallbacks() {

        }

        @Override
        public Loader<Response<ApiManager.ListResponse<Fighter>>> onCreateLoader(int id, Bundle bundle) {
            query = null;
            if (bundle.containsKey(QUERY)) {
                query = bundle.getString(QUERY);
            }
            this.doResetAdapter = bundle.getBoolean(DO_RESET_ADAPTER);
            return new FighterListLoader(getApplicationContext(), query);
        }

        @Override
        public void onLoadFinished(Loader<Response<ApiManager.ListResponse<Fighter>>> loader, Response<ApiManager.ListResponse<Fighter>> response) {
            // Query Realm (cannot do this on non main thread =D)
            if (realmResults == null || doResetAdapter) {
                Realm realm = RealmManager.getInstance(getApplicationContext()).getRealm();
                RealmQuery<Fighter> realmQuery = realm.where(Fighter.class);

                boolean isResponseValid = response != null && response.body() != null && response.body().getResults() != null;
                if (query != null && isResponseValid && response.body().getResults().size() > 0) {
                    realmQuery = realmQuery.beginGroup();
                    for (int i = 0; i < response.body().getResults().size(); i++) {
                        Fighter fighter = response.body().getResults().get(i);
                        realmQuery = realmQuery.equalTo("guid", fighter.getGuid());
                        if (i < (response.body().getResults().size() - 1)) {
                            realmQuery = realmQuery.or();
                        }
                    }
                    realmQuery = realmQuery.endGroup();

                } else {
                    realmQuery = realmQuery.equalTo("isPrime", true);
                }

                if (isResponseValid && response.body().getResults().isEmpty() && query != null) {
                    realmResults = realmQuery.equalTo("guid", "9000").findAll();
                } else {
                    if (query != null) {
                        realmResults = realmQuery.findAllSorted("rank", Sort.ASCENDING);
                    } else if (currentSortOrder.equals(SortOrder.Name)) {
                        realmResults = realmQuery.findAllSorted("fullName", Sort.ASCENDING);
                    } else {
                        realmResults = realmQuery.findAllSorted(
                                new String[]{"weightClassIndex", "fullName"},
                                new Sort[]{Sort.ASCENDING, Sort.ASCENDING}
                        );
                    }
                }
            }

            if (doResetAdapter || getListView().getAdapter() == null) {
                getListView().setFastScrollEnabled(true);
                getListView().setAdapter(new FighterListAdapter(getContext(), realmResults, false, currentSortOrder));
            }
            ((FighterListAdapter)getListView().getAdapter()).notifyDataSetChanged();

            // Reset loader
            getLoaderManager().destroyLoader(LOADER_ID);

            // Stop refresher
            setRefreshing(false);
        }

        @Override
        public void onLoaderReset(Loader<Response<ApiManager.ListResponse<Fighter>>> loader) {

        }
    }

    private static class FighterListLoader extends AsyncTaskLoader<Response<ApiManager.ListResponse<Fighter>>> {
        private final String query;
        public FighterListLoader(Context context, String query) {
            super(context);
            this.query = query;
        }

        @Override
        public Response<ApiManager.ListResponse<Fighter>> loadInBackground() {
            return FighterManager.getInstance(getContext()).fetchFighters(query);
        }
    }

    private static class FighterListAdapter extends RealmListAdapter<Fighter> implements SectionIndexer {
        private static final int VIEW_TYPE_FIGHTER = 0;
        private List<SectionInfo> sectionInfos;
        private final SortOrder sortOrder;

        public FighterListAdapter(Context context, RealmResults<Fighter> realmResults, boolean doAutomaticUpdate, SortOrder sortOrder) {
            super(context, realmResults, doAutomaticUpdate);
            this.sortOrder = sortOrder;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public int getItemViewType(int position) {
            return VIEW_TYPE_FIGHTER;
        }

        @Override
        public int getViewResourceId(int itemViewType) {
            return R.layout.list_item_fighter;
        }

        @Override
        public void bindView(int position, View view) {
            final Fighter fighter = getItem(position);
            if (view instanceof FighterListItemLayout) {
                ((FighterListItemLayout) view).bind(getContext(), fighter, position, false, "");
            }
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext().getApplicationContext(), FighterDetailActivity.class);
                    intent.putExtra(RealmWebFragment.EXTRA_GUID, fighter.getGuid());
                    getContext().startActivity(intent);
                }
            });
        }

        @Override
        public Object[] getSections() {
            sectionInfos = new ArrayList<>();
            int startIndex = 0;
            String sectionName = null;
            for (int i = 0; i < realmResults.size(); i++) {
                Fighter fighter = realmResults.get(i);
                String sectionNameForObject = getSectionNameForObject(fighter);
                if (!sectionNameForObject.equalsIgnoreCase(sectionName)) {
                    if (sectionName != null) {
                        sectionInfos.add(new SectionInfo(sectionName, startIndex, i - 1));
                    }
                    sectionName = sectionNameForObject;
                    startIndex = i;
                }
            }
            return sectionInfos.toArray();
        }

        @Override
        public int getPositionForSection(int sectionIndex) {
            return (sectionInfos.isEmpty()) ? 0 : sectionInfos.get(sectionIndex).startIndex;
        }

        @Override
        public int getSectionForPosition(int position) {
            for (int i = 0; i < sectionInfos.size(); i++) {
                SectionInfo sectionInfo = sectionInfos.get(i);
                if (position >= sectionInfo.startIndex && position < sectionInfo.endIndex) {
                    return i;
                }
            }
            return 0;
        }

        public String getSectionNameForObject(Fighter fighter) {
            String sectionName = "";
            switch (sortOrder) {
                case Name:
                    sectionName = fighter.getFullName().substring(0, 1);
                    break;
                case WeightClass:
                    sectionName = Fighter.WeightClass.getEnum(fighter.getWeightClass()).getSectionName();
                    break;
            }
            return sectionName;
        }

        private static class SectionInfo {
            private final int startIndex;
            private final int endIndex;
            private final String name;

            public SectionInfo(String name, int startIndex, int endIndex) {
                this.startIndex = startIndex;
                this.endIndex = endIndex;
                this.name = name;
            }

            @Override
            public String toString() {
                return name;
            }
        }
    }

    public enum SortOrder {
        Name("Sort by Name"),
        WeightClass("Sort by Weight Class");

        private final String displayName;

        SortOrder(String displayName) {
            this.displayName = displayName;
        }

        public static SortOrder getEnum(String name) {
            for (SortOrder sortOrder : SortOrder.values()) {
                if (sortOrder.getDisplayName().equalsIgnoreCase(name)) {
                    return sortOrder;
                }
            }
            return Name;
        }

        public String getDisplayName() {
            return displayName;
        }


    }
}
