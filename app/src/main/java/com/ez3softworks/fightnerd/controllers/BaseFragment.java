package com.ez3softworks.fightnerd.controllers;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.Map;

public abstract class BaseFragment extends Fragment {
    private static final String TAG = "BaseFragment";
    private static final String EXTRA_START_TIME = "EXTRA_START_TIME";
	private DateTime startTime;

//	@Override
//	public void onSaveInstanceState(Bundle outState) {
//		super.onSaveInstanceState(outState);
//		if (startTime != null) {
//			outState.putLong(EXTRA_START_TIME, startTime.getMillis());
//		}
//	}

	@Override
	public void setUserVisibleHint(boolean visible) {
		super.setUserVisibleHint(visible);
		if (visible) {
			onVisible();
		} else {
			onInvisible();
		}
	}

	protected void onVisible() {
    }

	protected void onInvisible() {
    }

    public void onStartEvent() {
        startTime = new DateTime();
        Log.i(TAG, "Start time is " + startTime);
    }

    public void onStopEvent() {
        if (startTime == null) {
            return;
        }
        Log.i(TAG, "Start time is " + startTime);
        Duration duration = new Duration(startTime, new DateTime());
        CustomEvent customEvent = new CustomEvent(getEventName())
                .putCustomAttribute("duration", ((double)duration.getMillis()) / 1000.0d);
        Map<String, Object> eventAttributes = getEventAttributes();
        if (eventAttributes != null) {
            for (Map.Entry<String, Object> entry : eventAttributes.entrySet()) {
                if (entry.getValue() instanceof Number) {
                    customEvent.putCustomAttribute(entry.getKey(), (Number)entry.getValue());
                } else if (entry.getValue() instanceof String) {
                    String value = (String) entry.getValue();
                    if (value.length() > 100) {
                        value = value.substring(0, 100);
                    }
                    customEvent.putCustomAttribute(entry.getKey(), value);
                }
            }
        }
        Answers.getInstance().logCustom(customEvent);
    }

//    @Override
//	public void onCreate(Bundle bundle) {
//		super.onCreate(bundle);
////        setRetainInstance(true);
////		if (bundle != null && bundle.containsKey(EXTRA_START_TIME)) {
////			startTime = new DateTime(bundle.getLong(EXTRA_START_TIME));
////		}
//	}

	public AppCompatActivity getSupportActivity() {
		Activity activity = getActivity();
		if (activity != null && activity instanceof AppCompatActivity) {
			return (AppCompatActivity)activity;
		}
		return null;
	}

	public ActionBar getSupportActionBar() {
		AppCompatActivity supportActivity = getSupportActivity();
		return supportActivity.getSupportActionBar();
	}

	protected AppBarLayout getAppBarLayout() {
		Activity activity = getActivity();
		if (activity instanceof FragmentPagerActivity) {
			return ((FragmentPagerActivity)activity).getAppBarLayout();
		}
		return null;
	}

	protected Context getApplicationContext() {
		return getContext().getApplicationContext();
	}


	public abstract String getEventName();
    public abstract Map<String, Object> getEventAttributes();
}