package com.ez3softworks.fightnerd.controllers;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Spinner;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.ez3softworks.fightnerd.views.ViewPager;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.codechimp.apprater.AppRater;

import java.util.HashMap;
import java.util.Map;

public class FragmentPagerActivity extends BaseFragmentActivity {
    private static final String TAG = "FragmentPagerActivity";

    private AppBarLayout appBarLayout;
    private Spinner spinner;
    private ViewPager viewPager;

    @Override
    public void onResume() {
        super.onResume();

        if (viewPager == null) {
            return;
        }

        Fragment fragment = ((FragmentPagerAdapter)viewPager.getAdapter()).getFragments().get(viewPager.getCurrentItem());
        if (fragment != null && fragment instanceof ListFragment) {
            ((ListFragment) fragment).onStartEvent();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (viewPager == null) {
            return;
        }

        Fragment fragment = ((FragmentPagerAdapter)viewPager.getAdapter()).getFragments().get(viewPager.getCurrentItem());
        if (fragment != null && fragment instanceof ListFragment) {
            ((ListFragment) fragment).onStopEvent();
        }
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_fragment_pager);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(fragmentPagerAdapter);
        viewPager.clearOnPageChangeListeners();

        // Nav bar
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("");
        }

        // App bar layout
        appBarLayout = (AppBarLayout)findViewById(R.id.app_bar_layout);

        // Spinner
        spinner = (Spinner)findViewById(R.id.spinner);

        // Tab layout
        TabLayout tabLayout = (TabLayout)findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        TabLayout.ViewPagerOnTabSelectedListener viewPagerOnTabSelectedListener = new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                int position = tab.getPosition();

                // Title bar
                ActionBar actionBar = getSupportActionBar();
                if (actionBar != null) {
                    if (position == 2) {
                        actionBar.setTitle("Events");
                    } else {
                        actionBar.setTitle("");
                    }
                }

                // Answers
                Fragment fragment = ((FragmentPagerAdapter)viewPager.getAdapter()).getFragments().get(position);
                if (fragment != null && fragment instanceof ListFragment) {
                    ((ListFragment) fragment).onStartEvent();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                int position = tab.getPosition();
                Fragment fragment = ((FragmentPagerAdapter)viewPager.getAdapter()).getFragments().get(position);
                if (fragment != null && fragment instanceof ListFragment) {
                    ((ListFragment) fragment).onStopEvent();
                }
            }
        };
        tabLayout.setOnTabSelectedListener(viewPagerOnTabSelectedListener);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab == null) {
                continue;
            }
            tab.setIcon(fragmentPagerAdapter.getPageIcon(i));
        }

        // For first fragment...
        Fragment fragment = ((FragmentPagerAdapter)viewPager.getAdapter()).getFragments().get(0);
        if (fragment != null && fragment instanceof ListFragment) {
            ((ListFragment) fragment).onStartEvent();
        }
        viewPager.setCurrentItem(0);

        // Beg for ratings
        AppRater.setVersionNameCheckEnabled(true);
        AppRater.app_launched(this);

        // Ads...?
        MobileAds.initialize(getApplicationContext(), getString(R.string.admob_banner_api_key));
        AdView adView = (AdView) findViewById(R.id.ad_view);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }
        });
    }

    public AppBarLayout getAppBarLayout() {
        return appBarLayout;
    }

    private class FragmentPagerAdapter extends FragmentStatePagerAdapter {
        private Map<Integer, Fragment> fragments;
        private boolean didStartFirstEvent = false;

        public FragmentPagerAdapter(FragmentManager fm) {
            super(fm);
            fragments = new HashMap<>();
        }

        @Override
        public Fragment getItem(int position) {
            ListFragment fragment = null;
            ListFragment.Delegate delegate = new ListFragment.Delegate() {
                @Override
                public Spinner getSpinner() {
                    return spinner;
                }
            };
            switch(position) {
                case 0:
                    fragment = new ArticleListFragment();
                    fragment.setDelegate(delegate);
                    if (!didStartFirstEvent) {
                        fragment.onStartEvent();
                        didStartFirstEvent = true;
                    }
                    fragments.put(0, fragment);
                    break;
                case 1:
                    fragment = new VideoListFragment();
                    fragment.setDelegate(delegate);
                    fragments.put(1, fragment);
                    break;
                case 2:
                    fragment = new EventListFragment();
                    fragment.setDelegate(delegate);
                    fragments.put(2, fragment);
                    break;
                case 3:
                    fragment = new RankingListFragment();
                    fragment.setDelegate(delegate);
                    fragments.put(3, fragment);
                    break;
                case 4:
                    fragment = new FighterListFragment();
                    fragment.setDelegate(delegate);
                    fragments.put(4, fragment);
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 5;
        }

        public int getPageIcon(int position) {
            int icon = -1;
            switch (position) {
                case 0:
                    icon = R.drawable.ic_action_newspaper;
                    break;
                case 1:
                    icon = R.drawable.ic_action_film;
                    break;
                case 2:
                    icon = R.drawable.ic_action_calendar;
                    break;
                case 3:
                    icon = R.drawable.ic_action_rankings;
                    break;
                case 4:
                    icon = R.drawable.ic_action_fighters;
                    break;
            }
            return icon;
        }

        public Map<Integer, Fragment> getFragments() {
            return fragments;
        }
    }

	public enum FragmentType {
		ArticleList,
        VideoList,
		FighterList,
		RankingList,
		EventList,
		Store,
		Invalid,
	}
}
