package com.ez3softworks.fightnerd.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Fighter;
import com.ez3softworks.fightnerd.models.Fighter;
import com.ez3softworks.fightnerd.stores.RealmManager;
import com.ez3softworks.fightnerd.utilities.LogUtilities;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

public class FighterDetailFragment extends RealmWebFragment<Fighter> {
	public static final String TAG = "FighterDetailFragment";

    public static FighterDetailFragment newInstance(String guid) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_GUID, guid);

        FighterDetailFragment fragment = new FighterDetailFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = super.onCreateView(layoutInflater, viewGroup, bundle);
        if (view != null) {
            view.setBackgroundColor(0x191919);
            WebView webView = (WebView) view.findViewById(R.id.web_view);
            webView.setBackgroundColor(0x191919);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected Class<Fighter> getRealmObjectClass() {
        return Fighter.class;
    }

    @Override
    protected String getContentUrl() {
        Fighter fighter = getRealmObject();
        return fighter.getContentUrl() + "?android=true";
    }

    @Override
    protected String getActionBarTitle() {
        Fighter fighter = getRealmObject();
        return fighter.getFullName();
    }

    @Override
    protected Intent getShareIntent() {
        Fighter fighter = getRealmObject();
        if (fighter == null) {
            return null;
        }
        Intent shareIntent = new Intent();
        String text = String.format("%s", fighter.getContentUrl());
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, fighter.getImageUrl());
        shareIntent.setType("text/plain");
        return shareIntent;
    }

    @Override
    protected boolean isFavorite() {
        Fighter fighter = getRealmObject();
        return fighter.isFavorite();
    }

    @Override
    protected void onPressFavoriteMenuItem() {
        Realm realm = RealmManager.getInstance(getContext().getApplicationContext()).getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Fighter fighter = getRealmObject();
                fighter.setIsFavorite(!fighter.isFavorite());
            }
        });
    }

    @Override
    protected int getOptionsMenuResourceId() {
        return R.menu.fighter_detail_options;
    }

    @Override
    public String getEventName() {
        return "Fighter Detail";
    }

    @Override
    public Map<String, Object> getEventAttributes() {
        Fighter fighter = getRealmObject();
        Map<String, Object> eventAttributes = new HashMap<>();
        eventAttributes.put("fullName", fighter.getFullName());
        eventAttributes.put("guid", fighter.getGuid());
        return eventAttributes;
    }

    @Override
    protected String getSourceUrl() {
        return null;
    }
}
