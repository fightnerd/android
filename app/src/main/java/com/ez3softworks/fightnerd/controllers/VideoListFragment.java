package com.ez3softworks.fightnerd.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Video;
import com.ez3softworks.fightnerd.models.Channel;
import com.ez3softworks.fightnerd.stores.ApiManager;
import com.ez3softworks.fightnerd.stores.VideoManager;
import com.ez3softworks.fightnerd.stores.RealmManager;
import com.ez3softworks.fightnerd.views.VideoListItemLayout;

import java.util.Date;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Response;


public class VideoListFragment extends ListFragment {
	private static final String TAG = "VideoListFragment";

    private Channel currentChannel = Channel.TopVideos;
    private final LoaderManager.LoaderCallbacks<Response<ApiManager.ListResponse<Video>>> videoListLoaderCallbacks = new VideoListLoaderCallbacks();

    @Override
    protected boolean onSpinnerItemSelected(String item) {
        Channel channel = Channel.getEnum(item);
        if (currentChannel == channel) {
            return false;
        }
        currentChannel = channel;
        return true;
    }

    @Override
    protected int getOptionsMenuResourceId() {
        return R.menu.video_list_options;
    }

    @Override
    protected CharSequence[] getSpinnerTextArray() {
        return getResources().getTextArray(R.array.action_channel_array);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_list;
    }

    @Override
    protected Bundle onRestartLoader(Bundle bundle) {
        Date beforeDate = new Date();
        boolean doResetAdapter = true;
        Channel channel = currentChannel;
        String query = null;

        RestartLoaderAction restartAction = RestartLoaderAction.valueOf(bundle.getString(RESTART_LOADER_ACTION));
        switch(restartAction) {
            case SearchQuery:
                query = bundle.getString(SEARCH_QUERY);
                Answers.getInstance().logCustom(new CustomEvent("Video Search").putCustomAttribute("query", query));
                break;
            case SearchMenuItemActionCollapse:
                break;
            case SwipeToLastItem:
                RealmRecyclerAdapter realmRecyclerAdapter = (RealmRecyclerAdapter) getRecyclerView().getAdapter();
                RealmResults<?> realmResults = realmRecyclerAdapter.getRealmResults();
                if (!realmResults.isEmpty()) {
                    RealmObject realmObject = realmResults.get(realmResults.size() - 1);
                    if (realmObject instanceof Video) {
                        beforeDate = ((Video)realmObject).getDate();
                    }
                }
                doResetAdapter = false;
                break;
            case SwipeToRefresh:
                break;
            case SpinnerItemSelected:
                String spinnerItem = bundle.getString(SPINNER_ITEM);
                channel = Channel.getEnum(spinnerItem);
                break;
            case FragmentResume:
                break;
        }

        Bundle loaderBundle = new Bundle();
        loaderBundle.putBoolean(VideoListLoaderCallbacks.DO_RESET_ADAPTER, doResetAdapter);
        loaderBundle.putLong(VideoListLoaderCallbacks.BEFORE_DATE, beforeDate.getTime());
        if (channel != null) {
            loaderBundle.putInt(VideoListLoaderCallbacks.CHANNEL, channel.ordinal());
        }
        if (query != null) {
            loaderBundle.putString(VideoListLoaderCallbacks.QUERY, query);
        }

        return loaderBundle;
    }

    @Override
    protected LoaderManager.LoaderCallbacks<?> getLoaderCallbacks() {
        return videoListLoaderCallbacks;
    }

    @Override
    public String getEventName() {
        return "Video List";
    }

    @Override
    public Map<String, Object> getEventAttributes() {
        return null;
    }

    @Override
    protected int getCurrentSpinnerSelectionIndex() {
        return currentChannel.ordinal();
    }

    private class VideoListLoaderCallbacks implements LoaderManager.LoaderCallbacks<Response<ApiManager.ListResponse<Video>>> {
        private static final String TAG = "VideoListLoaderCallbacks";
        private static final String CHANNEL = "CHANNEL";
        private static final String BEFORE_DATE = "BEFORE_DATE";
        private static final String QUERY = "QUERY";
        private static final String DO_RESET_ADAPTER = "DO_RESET_ADAPTER";

        private RealmResults<Video> realmResults;
        private boolean doResetAdapter = false;
        private String query = null;

        public VideoListLoaderCallbacks() {

        }

        @Override
        public Loader<Response<ApiManager.ListResponse<Video>>> onCreateLoader(int id, Bundle bundle) {
            Date beforeDate = null;
            Channel channel = null;
            query = null;
            if (bundle.containsKey(BEFORE_DATE)) {
                beforeDate = new Date(bundle.getLong(BEFORE_DATE));
            }
            if (bundle.containsKey(CHANNEL)) {
                channel = Channel.values()[bundle.getInt(CHANNEL)];
            }
            if (bundle.containsKey(QUERY)) {
                query = bundle.getString(QUERY);
            }
            this.doResetAdapter = bundle.getBoolean(DO_RESET_ADAPTER);
            return new VideoListLoader(getApplicationContext(), beforeDate, channel, query);
        }

        @Override
        public void onLoadFinished(Loader<Response<ApiManager.ListResponse<Video>>> loader, Response<ApiManager.ListResponse<Video>> response) {
            // Query Realm (cannot do this on non main thread =D)
            if (realmResults == null || doResetAdapter) {
                Realm realm = RealmManager.getInstance(getApplicationContext()).getRealm();
                RealmQuery<Video> realmQuery = realm.where(Video.class);
                if (currentChannel != null && currentChannel != Channel.TopVideos) {
                    realmQuery = realmQuery.equalTo("channel", currentChannel.getDisplayName());
                }

                boolean isResponseValid = response != null && response.body() != null && response.body().getResults() != null;
                if (query != null && isResponseValid && response.body().getResults().size() > 0) {
                    realmQuery = realmQuery.beginGroup();
                    for (int i = 0; i < response.body().getResults().size(); i++) {
                        Video video = response.body().getResults().get(i);
                        realmQuery = realmQuery.equalTo("guid", video.getGuid());
                        if (i < (response.body().getResults().size() - 1)) {
                            realmQuery = realmQuery.or();
                        }
                    }
                    realmQuery = realmQuery.endGroup();

                }

                if (isResponseValid && response.body().getResults().isEmpty() && query != null) {
                    realmResults = realmQuery.equalTo("guid", "9000").findAll();
                } else if (query != null) {
                    realmResults = realmQuery.findAllSorted("rank", Sort.ASCENDING);
                } else {
                    realmResults = realmQuery.findAllSorted("date", Sort.DESCENDING);
                }
            }

            if (doResetAdapter || getRecyclerView().getAdapter() == null) {
                getRecyclerView().setAdapter(new VideoRecyclerAdapter(getActivity(), realmResults));
            }
            getRecyclerView().getAdapter().notifyDataSetChanged();

            // Reset loader
            getLoaderManager().destroyLoader(LOADER_ID);

            // Stop refresher
            setRefreshing(false);
        }

        @Override
        public void onLoaderReset(Loader<Response<ApiManager.ListResponse<Video>>> loader) {

        }
    }

    private static class VideoListLoader extends AsyncTaskLoader<Response<ApiManager.ListResponse<Video>>> {
        private final Date beforeDate;
        private final Channel channel;
        private final String query;
        public VideoListLoader(Context context, Date beforeDate, Channel channel, String query) {
            super(context);
            this.beforeDate = beforeDate;
            this.channel = channel;
            this.query = query;
        }

        @Override
        public Response<ApiManager.ListResponse<Video>> loadInBackground() {
            return VideoManager.getInstance(getContext()).fetchVideos(beforeDate, channel, query);
        }
    }

    private static class VideoRecyclerAdapter extends RealmRecyclerAdapter<Video> {
        private static final int VIEW_TYPE_VIDEO = 0;
        private static final int VIEW_TYPE_PROGRESS_BAR = 1;
        private static final int VIEW_TYPE_NO_OBJECTS = 2;

        public VideoRecyclerAdapter(Context context, RealmResults<Video> realmResults) {
            super(context, realmResults);
        }

        @Override
        public int getItemViewType(int position) {
            Video video = getItem(position);
            if (getRealmResults().isEmpty()) {
                return VIEW_TYPE_NO_OBJECTS;
            } else {
                if (video != null) {
                    return VIEW_TYPE_VIDEO;
                } else {
                    return VIEW_TYPE_PROGRESS_BAR;
                }
            }
        }

        public Video getItem(int position) {
            if (position >= getRealmResults().size()) {
                return null;
            }
            return getRealmResults().get(position);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            ViewHolder viewHolder = null;
            View view = null;
            LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            switch (viewType) {
                case VIEW_TYPE_VIDEO:
                    view = layoutInflater.inflate(R.layout.list_item_video, viewGroup, false);
                    break;
                case VIEW_TYPE_PROGRESS_BAR:
                    view = layoutInflater.inflate(R.layout.list_item_progress_bar, viewGroup, false);
                    break;
                case VIEW_TYPE_NO_OBJECTS:
                    view = layoutInflater.inflate(R.layout.list_item_no_objects, viewGroup, false);
                    break;
            }
            if (view != null) {
                viewHolder = new ViewHolder(view);
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, final int position) {
            if (getRealmResults().isEmpty()) {
                View view = viewHolder.itemView;
                if (view instanceof TextView) {
                    ((TextView)view).setText(R.string.no_videos);
                }
            } else {
                final Video video = getItem(position);
                if (video == null) {
                    return;
                }
                View view = viewHolder.itemView;
                if (view instanceof VideoListItemLayout) {
                    ((VideoListItemLayout)view).bind(getContext(), video, position, true);
                }
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext().getApplicationContext(), VideoDetailActivity.class);
                        intent.putExtra(RealmWebFragment.EXTRA_GUID, video.getGuid());
                        getContext().startActivity(intent);

                        // Save
                        Realm realm = RealmManager.getInstance(getContext().getApplicationContext()).getRealm();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                video.setIsWatched(true);
                            }
                        });

                        notifyItemChanged(position);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return getRealmResults().size() + 1;
        }
    }
}
