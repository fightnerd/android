package com.ez3softworks.fightnerd.controllers;

import android.widget.SectionIndexer;

public interface TitledSectionIndexer extends SectionIndexer {
	public String[] getSectionTitles();
}
