package com.ez3softworks.fightnerd.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.SectionIndexer;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Ranking;
import com.ez3softworks.fightnerd.stores.ApiManager;
import com.ez3softworks.fightnerd.stores.FighterManager;
import com.ez3softworks.fightnerd.stores.RealmManager;
import com.ez3softworks.fightnerd.views.RankingListItemLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Response;


public class RankingListFragment extends ListFragment {
	private static final String TAG = "RankingListFragment";

    private Ranking.WeightClass currentWeightClass = Ranking.WeightClass.PoundForPound;
    private final CharSequence[] spinnerTextArray = new CharSequence[]{
            Ranking.WeightClass.PoundForPound.getDisplayName(),
            Ranking.WeightClass.Heavyweight.getDisplayName(),
            Ranking.WeightClass.LightHeavyweight.getDisplayName(),
            Ranking.WeightClass.Middleweight.getDisplayName(),
            Ranking.WeightClass.Welterweight.getDisplayName(),
            Ranking.WeightClass.Lightweight.getDisplayName(),
            Ranking.WeightClass.Featherweight.getDisplayName(),
            Ranking.WeightClass.Bantamweight.getDisplayName(),
            Ranking.WeightClass.WomensBantamweight.getDisplayName(),
            Ranking.WeightClass.Flyweight.getDisplayName(),
            Ranking.WeightClass.WomensStrawweight.getDisplayName(),

    };
    private final LoaderManager.LoaderCallbacks<Response<ApiManager.ListResponse<Ranking>>> rankingListLoaderCallbacks = new RankingListLoaderCallbacks();

    @Override
    protected boolean onSpinnerItemSelected(String item) {
        Ranking.WeightClass weightClass = Ranking.WeightClass.getEnum(item);
        if (currentWeightClass == weightClass) {
            return false;
        }
        currentWeightClass = weightClass;
        return true;
    }

    @Override
    protected int getOptionsMenuResourceId() {
        return -1;
    }

    @Override
    protected CharSequence[] getSpinnerTextArray() {
        return spinnerTextArray;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_ranking_list;
    }

    @Override
    protected int getCurrentSpinnerSelectionIndex() {
        for (int i = 0; i < spinnerTextArray.length; i++) {
            if (spinnerTextArray[i].equals(currentWeightClass.getDisplayName())) {
                return i;
            }
        }
        return 0;
    }

    @Override
    protected Bundle onRestartLoader(Bundle bundle) {
        boolean doResetAdapter = true;
        Ranking.WeightClass weightClass = currentWeightClass;

        RestartLoaderAction restartAction = RestartLoaderAction.valueOf(bundle.getString(RESTART_LOADER_ACTION));
        switch(restartAction) {
            case SearchMenuItemActionCollapse:
                break;
            case SwipeToLastItem:
                return null;
            case SwipeToRefresh:
                break;
            case SpinnerItemSelected:
                String spinnerItem = bundle.getString(SPINNER_ITEM);
                weightClass = Ranking.WeightClass.getEnum(spinnerItem);
                break;
            case FragmentResume:
                break;
        }

        Bundle loaderBundle = new Bundle();
        loaderBundle.putBoolean(RankingListLoaderCallbacks.DO_RESET_ADAPTER, doResetAdapter);
        loaderBundle.putInt(RankingListLoaderCallbacks.WEIGHT_CLASS, weightClass.ordinal());
        return loaderBundle;
    }

    @Override
    protected LoaderManager.LoaderCallbacks<?> getLoaderCallbacks() {
        return rankingListLoaderCallbacks;
    }

    @Override
    public String getEventName() {
        return "Ranking List";
    }

    @Override
    public Map<String, Object> getEventAttributes() {
        return null;
    }

    private class RankingListLoaderCallbacks implements LoaderManager.LoaderCallbacks<Response<ApiManager.ListResponse<Ranking>>> {
        private static final String TAG = "RankingListLoaderCallbacks";
        private static final String WEIGHT_CLASS = "WEIGHT_CLASS";
        private static final String DO_RESET_ADAPTER = "DO_RESET_ADAPTER";

        private RealmResults<Ranking> realmResults;
        private boolean doResetAdapter = false;
        private Ranking.WeightClass weightClass = null;

        public RankingListLoaderCallbacks() {

        }

        @Override
        public Loader<Response<ApiManager.ListResponse<Ranking>>> onCreateLoader(int id, Bundle bundle) {
            weightClass = null;
            if (bundle.containsKey(WEIGHT_CLASS)) {
                weightClass = Ranking.WeightClass.values()[bundle.getInt(WEIGHT_CLASS)];
            }
            this.doResetAdapter = bundle.getBoolean(DO_RESET_ADAPTER);
            return new RankingListLoader(getApplicationContext(), weightClass);
        }

        @Override
        public void onLoadFinished(Loader<Response<ApiManager.ListResponse<Ranking>>> loader, Response<ApiManager.ListResponse<Ranking>> response) {
            // Query Realm (cannot do this on non main thread =D)
            if (realmResults == null || doResetAdapter) {
                Realm realm = RealmManager.getInstance(getContext().getApplicationContext()).getRealm();
                RealmQuery<Ranking> realmQuery = realm.where(Ranking.class).equalTo("weightClass", weightClass.getDisplayName());
                realmResults = realmQuery.findAllSorted("rank", Sort.ASCENDING);
            }

            if (doResetAdapter || getListView().getAdapter() == null) {
                getListView().setAdapter(new RankingListAdapter(getContext(), realmResults, false));
            }
            ((RankingListAdapter)getListView().getAdapter()).notifyDataSetChanged();

            // Reset loader
            getLoaderManager().destroyLoader(LOADER_ID);

            // Stop refresher
            setRefreshing(false);
        }

        @Override
        public void onLoaderReset(Loader<Response<ApiManager.ListResponse<Ranking>>> loader) {

        }
    }

    private static class RankingListLoader extends AsyncTaskLoader<Response<ApiManager.ListResponse<Ranking>>> {
        private final Ranking.WeightClass weightClass;
        public RankingListLoader(Context context, Ranking.WeightClass weightClass) {
            super(context);
            this.weightClass = weightClass;
        }

        @Override
        public Response<ApiManager.ListResponse<Ranking>> loadInBackground() {
            return FighterManager.getInstance(getContext()).fetchRankings(weightClass);
        }
    }

    private static class RankingListAdapter extends RealmListAdapter<Ranking> {
        private static final int VIEW_TYPE_RANKING = 0;

        public RankingListAdapter(Context context, RealmResults<Ranking> realmResults, boolean doAutomaticUpdate) {
            super(context, realmResults, doAutomaticUpdate);
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public int getItemViewType(int position) {
            return VIEW_TYPE_RANKING;
        }

        @Override
        public int getViewResourceId(int itemViewType) {
            return R.layout.list_item_ranking;
        }

        @Override
        public void bindView(int position, View view) {
            final Ranking ranking = getItem(position);
            if (view instanceof RankingListItemLayout) {
                ((RankingListItemLayout) view).bind(getContext(), ranking, position);
            }
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext().getApplicationContext(), FighterDetailActivity.class);
                    intent.putExtra(RealmWebFragment.EXTRA_GUID, ranking.getFighter().getGuid());
                    getContext().startActivity(intent);
                }
            });
        }
    }
}
