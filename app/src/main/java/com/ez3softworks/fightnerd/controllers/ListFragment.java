package com.ez3softworks.fightnerd.controllers;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import com.ez3softworks.fightnerd.R;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import io.realm.RealmObject;


public abstract class ListFragment extends BaseFragment {
	private static final String TAG = "ListFragment";
	protected static final int LOADER_ID = 0;

    protected static final String RESTART_LOADER_ACTION = "RESTART_LOADER_ACTION";
    protected static final String SPINNER_ITEM = "SPINNER_ITEM";
    protected static final String SEARCH_QUERY = "SEARCH_QUERY";

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private ListView listView;
    private LinearLayoutManager layoutManager;
    private String currentQuery;
    private DateTime lastResumeDate = null;
    private Delegate delegate;

    @Override
    protected void onVisible() {
        super.onVisible();
        loadSpinner();

        RecyclerView recyclerView = getRecyclerView();
        if (recyclerView != null) {
            recyclerView.invalidate();
        }
    }

    @Override
	public void onResume() {
		super.onResume();

        if (lastResumeDate != null) {
            Duration duration = new Duration(new DateTime(), lastResumeDate);
            if (duration.getStandardMinutes() < 60) {
                return;
            }
        }
        setRefreshing(true);
        lastResumeDate = new DateTime();
        Bundle bundle = new Bundle();
        bundle.putString(RESTART_LOADER_ACTION, RestartLoaderAction.FragmentResume.toString());
        restartLoader(onRestartLoader(bundle));
	}

    @Override
    public void onPause() {
        super.onPause();
        setRefreshing(false);
        getLoaderManager().destroyLoader(LOADER_ID);
    }

    protected void setRefreshing(final boolean isRefreshing) {
        if (swipeRefreshLayout == null) {
            return;
        }
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(isRefreshing);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        lastResumeDate = null;
    }

	@Override
	public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        int optionsMenuResourceId = getOptionsMenuResourceId();
        if (optionsMenuResourceId < 0) {
            return;
        }
        menuInflater.inflate(optionsMenuResourceId, menu);

        // Search
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.menu_item_search);
        if (searchMenuItem != null) {
            MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    currentQuery = null;
                    Bundle bundle = new Bundle();
                    bundle.putString(RESTART_LOADER_ACTION, RestartLoaderAction.SearchMenuItemActionCollapse.toString());
                    setRefreshing(true);
                    restartLoader(onRestartLoader(bundle));
                    return true;
                }
            });
            SearchView searchView = (SearchView)MenuItemCompat.getActionView(searchMenuItem);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    handleSearchQuery(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
            searchView.setIconifiedByDefault(false);
            searchView.setSubmitButtonEnabled(true);
        }
    }

    @Override
	public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
		View view = layoutInflater.inflate(getLayoutResourceId(), viewGroup, false);

        // Recycler view
        View collectionView = view.findViewById(R.id.recycler_view);
        if (collectionView != null && collectionView instanceof RecyclerView) {
            recyclerView = (RecyclerView)collectionView;
            recyclerView.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(new RecyclerView.Adapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                    return null;
                }

                @Override
                public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

                }

                @Override
                public int getItemCount() {
                    return 0;
                }
            });
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    RealmRecyclerAdapter adapter = (RealmRecyclerAdapter) recyclerView.getAdapter();
                    if (adapter == null) {
                        return;
                    }
                    if (adapter.getItemCount() <= 1) {
                        return;
                    }
                    if (currentQuery != null) {
                        return;
                    }
                    if (layoutManager.findLastVisibleItemPosition() >= adapter.getItemCount() - 1) {
                        RealmObject realmObject = adapter.getItem(adapter.getItemCount() - 2);
                        if (realmObject == null) {
                            return;
                        }
                        Bundle bundle = new Bundle();
                        bundle.putString(RESTART_LOADER_ACTION, RestartLoaderAction.SwipeToLastItem.toString());
                        bundle = onRestartLoader(bundle);
                        if (bundle != null) {
                            restartLoader(bundle);
                        }
                    }
                }
            });
        }

        // List view
        collectionView = view.findViewById(R.id.list_view);
        if (collectionView != null && collectionView instanceof ListView) {
            listView = (ListView)collectionView;
            ViewCompat.setNestedScrollingEnabled(listView, true);
        }

	    // Now find the PullToRefreshLayout to setup
		swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                Bundle bundle = new Bundle();
                bundle.putString(RESTART_LOADER_ACTION, RestartLoaderAction.SwipeToRefresh.toString());
                restartLoader(onRestartLoader(bundle));
            }
        });
        swipeRefreshLayout.setColorSchemeResources(
                R.color.holo_primary_one,
                R.color.holo_primary_three,
                R.color.holo_primary_five
        );

		return view;
	}

    protected void loadSpinner() {
        if (delegate == null) {
            return;
        }
        final Spinner spinner = delegate.getSpinner();
        if (spinner == null) {
            return;
        }
        final CharSequence[] spinnerTextArray = getSpinnerTextArray();
        if (spinnerTextArray != null) {
            spinner.setVisibility(View.VISIBLE);
            SpinnerAdapter spinnerAdapter = new SpinnerAdapter(
                    getContext(),
                    R.layout.spinner_row,
                    spinnerTextArray) {
            };
            spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spinner.setAdapter(spinnerAdapter);
            int currentSpinnerSelectionIndex = getCurrentSpinnerSelectionIndex();
            spinner.setSelection(currentSpinnerSelectionIndex, true);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    String item = spinnerTextArray[position].toString();
                    if (!onSpinnerItemSelected(item)) {
                        return;
                    }
                    setRefreshing(true);
                    Bundle bundle = new Bundle();
                    bundle.putString(RESTART_LOADER_ACTION, RestartLoaderAction.SpinnerItemSelected.toString());
                    bundle.putString(SPINNER_ITEM, item);
                    restartLoader(onRestartLoader(bundle));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            spinnerAdapter.notifyDataSetChanged();
        } else {
            spinner.setVisibility(View.GONE);
        }
    }

    protected Context getApplicationContext() {
        return getContext().getApplicationContext();
    }

    protected abstract int getLayoutResourceId();
    protected abstract int getCurrentSpinnerSelectionIndex();

    public void handleSearchQuery(String query) {
        currentQuery = query;
        setRefreshing(true);
        Bundle bundle = new Bundle();
        bundle.putString(RESTART_LOADER_ACTION, RestartLoaderAction.SearchQuery.toString());
        bundle.putString(SEARCH_QUERY, query);
        restartLoader(onRestartLoader(bundle));
    }

    private void restartLoader(Bundle bundle) {
        Loader loader = getLoaderManager().getLoader(LOADER_ID);
        if (loader == null || !loader.isStarted()) {
            getLoaderManager().restartLoader(LOADER_ID, bundle, getLoaderCallbacks()).forceLoad();
        }
    }

    public String getCurrentQuery() {
        return currentQuery;
    }

    public LinearLayoutManager getLayoutManager() {
        return layoutManager;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public ListView getListView() {
        return listView;
    }

    public Delegate getDelegate() {
        return delegate;
    }

    public void setDelegate(Delegate delegate) {
        this.delegate = delegate;
    }

    protected abstract boolean onSpinnerItemSelected(String item);
    protected abstract int getOptionsMenuResourceId();
    protected abstract CharSequence[] getSpinnerTextArray();
    protected abstract Bundle onRestartLoader(Bundle bundle);
    protected abstract LoaderManager.LoaderCallbacks<?> getLoaderCallbacks();

    public interface Delegate {
        Spinner getSpinner();
    }

    public enum RestartLoaderAction {
        SearchQuery,
        SearchMenuItemActionCollapse,
        SwipeToLastItem,
        SwipeToRefresh,
        SpinnerItemSelected,
        FragmentResume
    }

    public interface Listener {
    }
}
