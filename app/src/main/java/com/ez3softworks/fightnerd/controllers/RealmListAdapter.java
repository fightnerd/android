package com.ez3softworks.fightnerd.controllers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Fighter;

import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;


public abstract class RealmListAdapter<T extends io.realm.RealmObject> extends RealmBaseAdapter<T> implements ListAdapter {
    private final Context context;

    public RealmListAdapter(Context context, RealmResults<T> realmResults, boolean doAutomaticUpdate) {
        super(context, realmResults, doAutomaticUpdate);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            view = getNewView(position, viewGroup);
        }
        bindView(position, view);
        return view;
    }

    public View getNewView(int position, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int itemViewType = getItemViewType(position);
        return layoutInflater.inflate(getViewResourceId(itemViewType), viewGroup, false);
    }

    protected Context getContext() {
        return context;
    }

    public abstract int getViewTypeCount();
    public abstract int getItemViewType(int position);
    public abstract int getViewResourceId(int itemViewType);
    public abstract void bindView(int position, View view);

}