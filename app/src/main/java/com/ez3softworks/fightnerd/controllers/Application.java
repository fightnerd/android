package com.ez3softworks.fightnerd.controllers;


import android.content.Context;
import android.content.SharedPreferences;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.ez3softworks.fightnerd.models.Fighter;
import com.ez3softworks.fightnerd.stores.FighterManager;
import com.ez3softworks.fightnerd.stores.RealmManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;

public class Application extends android.app.Application {
    private static final String PREFERENCES_DID_LOAD_DATABASE = "com.ez3softworks.fightnerd.controllers.Application.didLoadDatabase";

    public Application() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics(), new Answers());
//        FighterManager.getInstance(getApplicationContext()).loadFighters();
        loadDatabase();
    }

    private void loadDatabase() {
        if (didLoadDatabase()) {
            return;
        }
        try {
            final InputStream inputStream = this.getAssets().open("realm/FightNerd.realm");
            copyBundledRealmFile(inputStream, "FightNerd.realm");
            setDidLoadDatabase(true);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private String copyBundledRealmFile(InputStream inputStream, String outFileName) {
        try {
            File file = new File(this.getFilesDir(), outFileName);
            FileOutputStream outputStream = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, bytesRead);
            }
            outputStream.close();
            return file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean didLoadDatabase() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(PREFERENCES_DID_LOAD_DATABASE, Context.MODE_PRIVATE);;
        return sharedPreferences.getBoolean(PREFERENCES_DID_LOAD_DATABASE, false);
    }

    private void setDidLoadDatabase(boolean didLoadDatabase) {
        SharedPreferences sharedPreferences = this.getSharedPreferences(PREFERENCES_DID_LOAD_DATABASE, Context.MODE_PRIVATE);;
        sharedPreferences.edit().putBoolean(PREFERENCES_DID_LOAD_DATABASE, didLoadDatabase).apply();
    }

}
