package com.ez3softworks.fightnerd.controllers;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import android.database.Cursor;
import android.database.DataSetObserver;
import android.widget.SectionIndexer;

public class IntegerIndexer extends DataSetObserver implements TitledSectionIndexer {
	private Cursor mCursor;
	private int mColumnIndex;
	private Map<Integer, Integer> mSectionToPosition;
	private Map<Integer, Integer> mSectionToOffset;
	private Map<Integer, String> mSectionToFastScrollTitle;
	private Map<Integer, String> mSectionToTitle;
	private Object[] mSections;
	private Object[] mSectionFastScrollTitles;
	private String[] mSectionTitles;
	
	public IntegerIndexer(Cursor cursor, int columnIndex, Map<Integer, String> sectionToFastScrollTitle, Map<Integer, String> sectionToTitle) {
		super();
		
		mCursor = cursor;
		mColumnIndex = columnIndex;
		mSectionToFastScrollTitle = sectionToFastScrollTitle;
		mSectionToTitle = sectionToTitle;
		mSectionToPosition = new TreeMap<Integer, Integer>();
		mSectionToOffset = new TreeMap<Integer, Integer>();
		
        if (mCursor != null) {
        	mCursor.registerDataSetObserver(this);
        }
        
        buildIndex();
	}

	private void buildIndex() {
		mSectionToPosition.clear();
		mSectionToOffset.clear();
		int position = mCursor.getPosition();
		
		// Go through the selected column and take note of the different sections
		mCursor.moveToLast();
		int i = mCursor.getPosition();
		while (!mCursor.isAfterLast() && !mCursor.isBeforeFirst()) {
			int columnValue = mCursor.getInt(mColumnIndex);
			mSectionToPosition.put(columnValue, i);
			i--;
			mCursor.moveToPrevious();
		}
		
		// Load the section names into mSections
		mSections = new Object[mSectionToPosition.keySet().size()];
		mSectionFastScrollTitles = new Object[mSectionToPosition.keySet().size()];
		mSectionTitles = new String[mSectionToPosition.keySet().size()];
		i = 0;
		for (Integer section:mSectionToPosition.keySet()) {
			mSectionToOffset.put(section, i);
			mSections[i] = section;
			mSectionFastScrollTitles[i] = mSectionToFastScrollTitle.get(section);
			mSectionTitles[i] = mSectionToTitle.get(section);
			i++;
		}
		
		// Reset cursor
		mCursor.moveToPosition(position);
	}

	@Override
	public int getPositionForSection(int section) {
		return mSectionToPosition.get(mSections[section]);
	}

	@Override
	public int getSectionForPosition(int position) {
		Entry<Integer, Integer> previousEntry = null;
		int section = -1;
		for (Map.Entry<Integer, Integer> entry : mSectionToPosition.entrySet()) {
			if (previousEntry != null) {
				if (position<entry.getValue()) {
					section = mSectionToOffset.get(previousEntry.getKey());
					break;
				}
			}
			previousEntry = entry;
		}
		if (section == -1) {
			section = mSectionToOffset.get(previousEntry.getKey());
		}
		return section;
	}

	@Override
	public Object[] getSections() {
		return mSectionFastScrollTitles;
	}
	
    @Override
    public void onChanged() {
        super.onChanged();
        buildIndex();
    }

    @Override
    public void onInvalidated() {
        super.onInvalidated();
        buildIndex();
    }

	public Cursor getCursor() {
		return mCursor;
	}

	public void setCursor(Cursor cursor) {
        if (mCursor != null) {
        	mCursor.unregisterDataSetObserver(this);
        }
        mCursor = cursor;
        if (mCursor != null) {
            mCursor.registerDataSetObserver(this);
        }
	}

	@Override
	public String[] getSectionTitles() {
		return mSectionTitles;
	}
}