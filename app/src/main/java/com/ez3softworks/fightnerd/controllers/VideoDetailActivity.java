package com.ez3softworks.fightnerd.controllers;

import android.support.v4.app.Fragment;

public class VideoDetailActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		String guid = getIntent().getStringExtra(RealmWebFragment.EXTRA_GUID);
		return VideoDetailFragment.newInstance(guid);
	}
}
