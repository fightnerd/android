package com.ez3softworks.fightnerd.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Fighter extends RealmObject {
    @PrimaryKey
    private String guid;
    private String contentUrl;
    private int draws;
    private String fullName;
    private String imageUrl;
    private boolean isFavorite;
    private boolean isPrime;
    private int lossesByDecision;
    private int lossesByKnockout;
    private int lossesByOther;
    private int lossesBySubmission;
    private String nickname;
    private int noContests;
    private int rank;
    private String url;
    private String weightClass;
    private int weightClassIndex;
    private int winsByDecision;
    private int winsByKnockout;
    private int winsByOther;
    private int winsBySubmission;
    private boolean isSavedForLater;

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getWeightClassIndex() {
        return weightClassIndex;
    }

    public void setWeightClassIndex(int weightClassIndex) {
        this.weightClassIndex = weightClassIndex;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public boolean isPrime() {
        return isPrime;
    }

    public void setIsPrime(boolean isPrime) {
        this.isPrime = isPrime;
    }

    public int getLossesByDecision() {
        return lossesByDecision;
    }

    public void setLossesByDecision(int lossesByDecision) {
        this.lossesByDecision = lossesByDecision;
    }

    public int getLossesByKnockout() {
        return lossesByKnockout;
    }

    public void setLossesByKnockout(int lossesByKnockout) {
        this.lossesByKnockout = lossesByKnockout;
    }

    public int getLossesByOther() {
        return lossesByOther;
    }

    public void setLossesByOther(int lossesByOther) {
        this.lossesByOther = lossesByOther;
    }

    public int getLossesBySubmission() {
        return lossesBySubmission;
    }

    public void setLossesBySubmission(int lossesBySubmission) {
        this.lossesBySubmission = lossesBySubmission;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getNoContests() {
        return noContests;
    }

    public void setNoContests(int noContests) {
        this.noContests = noContests;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWeightClass() {
        return weightClass;
    }

    public void setWeightClass(String weightClass) {
        this.weightClass = weightClass;
    }

    public int getWinsByDecision() {
        return winsByDecision;
    }

    public void setWinsByDecision(int winsByDecision) {
        this.winsByDecision = winsByDecision;
    }

    public int getWinsByKnockout() {
        return winsByKnockout;
    }

    public void setWinsByKnockout(int winsByKnockout) {
        this.winsByKnockout = winsByKnockout;
    }

    public int getWinsByOther() {
        return winsByOther;
    }

    public void setWinsByOther(int winsByOther) {
        this.winsByOther = winsByOther;
    }

    public int getWinsBySubmission() {
        return winsBySubmission;
    }

    public void setWinsBySubmission(int winsBySubmission) {
        this.winsBySubmission = winsBySubmission;
    }

    public boolean isSavedForLater() {
        return isSavedForLater;
    }

    public void setIsSavedForLater(boolean isSavedForLater) {
        this.isSavedForLater = isSavedForLater;
    }

    public static boolean hasImage(Fighter fighter) {
        return fighter.getImageUrl() != null && !fighter.getImageUrl().isEmpty();
    }

    public static int getTotalWins(Fighter fighter) {
        return fighter.getWinsByDecision() + fighter.getWinsByKnockout() + fighter.getWinsByOther() + fighter.getWinsBySubmission();
    }

    public static int getTotalLosses(Fighter fighter) {
        return fighter.getLossesByDecision() + fighter.getLossesByKnockout() + fighter.getLossesByOther() + fighter.getLossesBySubmission();
    }

    public enum WeightClass {
        Atomweight("Atomweight", "105"),
        Strawweight("Strawweight", "115"),
        Flyweight("Flyweight", "125"),
        Bantamweight("Bantamweight", "135"),
        Featherweight("Featherweight", "145"),
        Lightweight("Lightweight", "155"),
        Welterweight("Welterweight", "170"),
        Middleweight("Middleweight", "185"),
        LightHeavyweight("Light Heavyweight", "205"),
        Heavyweight("Heavyweight", "265"),
        SuperHeavyweight("Super Heavyweight", "265+");

        private final String displayName;
        private final String sectionName;

        WeightClass(String displayName, String sectionName) {
            this.displayName = displayName;
            this.sectionName = sectionName;
        }

        public String getDisplayName() {
            return displayName;
        }

        public String getSectionName() {
            return sectionName;
        }

        public static WeightClass getEnum(String name) {
            for (WeightClass weightClass : WeightClass.values()) {
                if (weightClass.getDisplayName().equalsIgnoreCase(name)) {
                    return weightClass;
                }
            }
            return Atomweight;
        }
    }
}
