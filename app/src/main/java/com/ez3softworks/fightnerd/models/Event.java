package com.ez3softworks.fightnerd.models;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Event extends RealmObject {
    @PrimaryKey
    private String guid;
    private Date date;
    private boolean isTimeValid;
    private String subtitle;
    private String title;
    private String url;
    private boolean isUpcoming;
    private RealmList<Fight> fights;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isTimeValid() {
        return isTimeValid;
    }

    public void setIsTimeValid(boolean isTimeValid) {
        this.isTimeValid = isTimeValid;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isUpcoming() {
        return isUpcoming;
    }

    public void setIsUpcoming(boolean isUpcoming) {
        this.isUpcoming = isUpcoming;
    }

    public RealmList<Fight> getFights() {
        return fights;
    }

    public void setFights(RealmList<Fight> fights) {
        this.fights = fights;
    }

    public static Fight getMainEvent(Event event) {
        for (Fight fight : event.getFights()) {
            if (fight.getOrder() == 0) {
                return fight;
            }
        }
        return null;
    }

    public static boolean areOddsValid(Event event) {
        Fight fight = Event.getMainEvent(event);
        return (fight != null) && Fight.areOddsValid(fight);
    }
}
