package com.ez3softworks.fightnerd.models;

import io.realm.RealmObject;

public class Corner extends RealmObject {
    private int odds;
    private boolean isWinner;
    private int order;
    private Fighter fighter;

    public int getOdds() {
        return odds;
    }

    public void setOdds(int odds) {
        this.odds = odds;
    }

    public boolean isWinner() {
        return isWinner;
    }

    public void setIsWinner(boolean isWinner) {
        this.isWinner = isWinner;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Fighter getFighter() {
        return fighter;
    }

    public void setFighter(Fighter fighter) {
        this.fighter = fighter;
    }

    public static boolean areOddsValid(Corner corner) {
        return Math.abs(corner.getOdds()) > 0;
    }
}
