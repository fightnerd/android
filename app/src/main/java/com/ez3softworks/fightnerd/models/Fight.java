package com.ez3softworks.fightnerd.models;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Fight extends RealmObject {
    private int order;
    private RealmList<Corner> corners;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public RealmList<Corner> getCorners() {
        return corners;
    }

    public void setCorners(RealmList<Corner> corners) {
        this.corners = corners;
    }

    public static boolean areOddsValid(Fight fight) {
        for (Corner corner : fight.getCorners()) {
            if (!Corner.areOddsValid(corner)) {
                return false;
            }
        }
        return true;
    }

    public static Corner getCorner(Fight fight, int order) {
        for (Corner corner : fight.getCorners()) {
            if (corner.getOrder() == order) {
                return corner;
            }
        }
        return null;
    }
}
