package com.ez3softworks.fightnerd.models;

public enum Channel {
    TopVideos("Top Videos"),
    BellatorMMA("Bellator MMA"),
    FreeFights("Free Fights"),
    MMAFighting("MMA Fighting"),
    UFC("UFC"),
    Invalid("Invalid");

    private final String displayName;

    Channel(String displayName) {
        this.displayName = displayName;
    }

    public static Channel getEnum(String name) {
        for (Channel publication : Channel.values()) {
            if (publication.getDisplayName().equalsIgnoreCase(name)) {
                return publication;
            }
        }
        return Invalid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getQuery() {
        return displayName;
    }
}
