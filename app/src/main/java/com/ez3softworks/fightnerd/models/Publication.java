package com.ez3softworks.fightnerd.models;

public enum Publication {
    TopNews("Top News"),
    BleacherReport("Bleacher Report"),
    BloodyElbow("Bloody Elbow"),
    CagePotato("Cage Potato"),
    FiveOuncesOfPain("Five Ounces of Pain"),
    MMAFighting("MMA Fighting"),
    MMAJunkie("MMA Junkie"),
    MMAMania("MMA Mania"),
    Sherdog("Sherdog"),
    Invalid("Invalid");

    private final String displayName;

    Publication(String displayName) {
        this.displayName = displayName;
    }

    public static Publication getEnum(String name) {
        for (Publication publication : Publication.values()) {
            if (publication.getDisplayName().equalsIgnoreCase(name)) {
                return publication;
            }
        }
        return Invalid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getQuery() {
        return displayName;
    }
}
