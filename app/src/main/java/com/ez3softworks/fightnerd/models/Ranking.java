package com.ez3softworks.fightnerd.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Ranking extends RealmObject {
    @PrimaryKey
    private String guid;
    private boolean isChampion;
    private boolean isInterimChampion;
    private boolean notPreviouslyRanked;
    private int rank;
    private int rankDelta;
    private String weightClass;
    private Fighter fighter;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public boolean isChampion() {
        return isChampion;
    }

    public void setIsChampion(boolean isChampion) {
        this.isChampion = isChampion;
    }

    public boolean isInterimChampion() {
        return isInterimChampion;
    }

    public void setIsInterimChampion(boolean isInterimChampion) {
        this.isInterimChampion = isInterimChampion;
    }

    public boolean isNotPreviouslyRanked() {
        return notPreviouslyRanked;
    }

    public void setNotPreviouslyRanked(boolean notPreviouslyRanked) {
        this.notPreviouslyRanked = notPreviouslyRanked;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getRankDelta() {
        return rankDelta;
    }

    public void setRankDelta(int rankDelta) {
        this.rankDelta = rankDelta;
    }

    public String getWeightClass() {
        return weightClass;
    }

    public void setWeightClass(String weightClass) {
        this.weightClass = weightClass;
    }

    public Fighter getFighter() {
        return fighter;
    }

    public void setFighter(Fighter fighter) {
        this.fighter = fighter;
    }

    public enum WeightClass {
        WomensBantamweight("Women's Bantamweight"),
        WomensStrawweight("Women's Strawweight"),
        Flyweight("Flyweight"),
        Bantamweight("Bantamweight"),
        Featherweight("Featherweight"),
        Lightweight("Lightweight"),
        Welterweight("Welterweight"),
        Middleweight("Middleweight"),
        LightHeavyweight("Light Heavyweight"),
        Heavyweight("Heavyweight"),
        PoundForPound("Pound-For-Pound");

        private final String displayName;

        WeightClass(String displayName) {
            this.displayName = displayName;
        }

        public String getDisplayName() {
            return displayName;
        }

        public static WeightClass getEnum(String name) {
            for (WeightClass weightClass : WeightClass.values()) {
                if (weightClass.getDisplayName().equalsIgnoreCase(name)) {
                    return weightClass;
                }
            }
            return PoundForPound;
        }
    }
}
