package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Corner;
import com.ez3softworks.fightnerd.models.Fight;
import com.ez3softworks.fightnerd.models.Fighter;
import com.ez3softworks.fightnerd.stores.PicassoManager;
import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.ez3softworks.fightnerd.utilities.SpannableStringBuilder;
import com.ez3softworks.fightnerd.utilities.TypefaceUtilities;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

import java.util.Locale;

public class FightListItemLayout extends RelativeLayout {

	protected static final String TAG = "FightListItemLayout";

	public FightListItemLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public FightListItemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FightListItemLayout(Context context) {
		super(context);
	}

	protected void loadImageView(final Context context, final Fighter fighter, int imageViewId) {
		final PicassoImageView imageView = (PicassoImageView)this.findViewById(imageViewId);

		// Fuck you Google...
		imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.fighter_unknown_image));

//		if (Fighter.hasImage(fighter)) {
//			int width = (int) context.getResources().getDimension(R.dimen.list_item_fighter_overview_image_width);
//			int height = (int) context.getResources().getDimension(R.dimen.list_item_fighter_overview_image_height);
//			imageView.setDelegate(new PicassoImageView.FadeInDelegate(){
//				@Override
//				public void onBitmapLoaded(Bitmap bitmap, LoadedFrom loadedFrom) {
//					super.onBitmapLoaded(bitmap, loadedFrom);
//					LogUtilities.d(TAG, "Bitmap loaded: %s (%s)", loadedFrom, fighter.getFullName());
//				}
//
//				@Override
//				public ImageView getImageView() {
//					return imageView;
//				}
//
//				@Override
//				public Context getContext() {
//					return context;
//				}
//			});
//			PicassoManager.getPicasso(context).load(fighter.getImageUrl())
//					.resize(width, height)
//					.placeholder(R.drawable.placeholder_shape_image)
//					.error(R.drawable.fighter_unknown_image)
//					.noFade()
//					.into((Target) imageView);
//		} else {
//			// Add unknown image...
//			imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.fighter_unknown_image));
//		}
	}
	
	protected void loadName(final Context context, final Fighter fighter, int textViewId) {
		TextView textView = (TextView)this.findViewById(textViewId);
		String text = fighter.getFullName();
		SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text)
															.setSpan(context, text, R.style.TextAppearance_UpcomingFightListItemName_CageJunkie)
															.setSpan(context, text, TypefaceUtilities.ROBOTO_REGULAR);
		textView.setText(spannableStringBuilder.fit(context, textView).build());
	}

	protected void loadRecord(final Context context, final Fighter fighter, int textViewId) {
		TextView textView = (TextView)this.findViewById(textViewId);

		// Create win/loss record text
		String winLossRecordText;
		if (fighter.getDraws() > 0) {
			winLossRecordText = String.format(Locale.US, "%d-%d-%d", Fighter.getTotalWins(fighter), Fighter.getTotalLosses(fighter), fighter.getDraws());
		} else {
			winLossRecordText = String.format(Locale.US,"%d-%d", Fighter.getTotalWins(fighter), Fighter.getTotalLosses(fighter));
		}
		
		// Create noContests text
		String noContestsText = "";
		boolean hasNoContests = fighter.getNoContests() > 0;
		if (hasNoContests) {
			noContestsText = String.format(Locale.US, "/ %d nc", fighter.getNoContests());
		}
		
		String text = String.format(Locale.US, "%s %s", winLossRecordText, noContestsText);
		SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text)
													      .setSpan(context, winLossRecordText, R.style.TextAppearance_UpcomingFightListItemRecord_CageJunkie)
													      .setSpan(context, winLossRecordText, TypefaceUtilities.ROBOTO_LIGHT);
		if (hasNoContests) {
			spannableStringBuilder.setSpan(context, noContestsText, R.style.TextAppearance_UpcomingFightListItemNoContest_CageJunkie)
								  .setSpan(context, noContestsText, TypefaceUtilities.ROBOTO_LIGHT);
		}
		
		textView.setText(spannableStringBuilder.fit(context, textView).build());
	}
	
	protected void bindOdds(Context context, Fight fight) {
		TextView firstFighterOddsTextView = (TextView)this.findViewById(R.id.first_fighter_odds_text_view);
		TextView secondFighterOddsTextView = (TextView)this.findViewById(R.id.second_fighter_odds_text_view);

		Corner firstCorner = Fight.getCorner(fight, 0);
		Corner secondCorner = Fight.getCorner(fight, 1);
		if ((firstCorner == null) || (secondCorner == null)) {
			return;
		}

		if (Fight.areOddsValid(fight)) {
			String firstFighterOddsText = String.format(firstCorner.getOdds() >= 0 ? "+%d":"%d", firstCorner.getOdds());
			String secondFighterOddsText = String.format(secondCorner.getOdds() >= 0 ? "+%d":"%d", secondCorner.getOdds());
			SpannableStringBuilder firstSpannableStringBuilder = new SpannableStringBuilder(firstFighterOddsText)
															      .setSpan(context, firstFighterOddsText, R.style.TextAppearance_UpcomingEventListItemOdds_CageJunkie)
															      .setSpan(context, firstFighterOddsText, TypefaceUtilities.ROBOTO_REGULAR);
			SpannableStringBuilder secondSpannableStringBuilder = new SpannableStringBuilder(secondFighterOddsText)
															      .setSpan(context, secondFighterOddsText, R.style.TextAppearance_UpcomingEventListItemOdds_CageJunkie)
															      .setSpan(context, secondFighterOddsText, TypefaceUtilities.ROBOTO_REGULAR);
			firstFighterOddsTextView.setText(firstSpannableStringBuilder.build());
			secondFighterOddsTextView.setText(secondSpannableStringBuilder.build());
			
			if (firstCorner.getOdds() > secondCorner.getOdds()) {
				firstFighterOddsTextView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_red));
				secondFighterOddsTextView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_green));
			} else if (firstCorner.getOdds() < secondCorner.getOdds()) {
				firstFighterOddsTextView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_green));
				secondFighterOddsTextView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_red));
			} else {
				firstFighterOddsTextView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_black));
				secondFighterOddsTextView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_black));
			}
		} else {
			String text = "";
			SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text)
														      .setSpan(context, text, R.style.TextAppearance_UpcomingEventListItemNoOdds_CageJunkie)
														      .setSpan(context, text, TypefaceUtilities.ROBOTO_REGULAR);
			firstFighterOddsTextView.setText(spannableStringBuilder.build());
			secondFighterOddsTextView.setText(spannableStringBuilder.build());
			firstFighterOddsTextView.setTextSize(0);
			secondFighterOddsTextView.setTextSize(0);
		}
		
	}
	
	public void bind(final Context context, Fight fight) {
        Corner firstCorner = Fight.getCorner(fight, 0);
        Corner secondCorner = Fight.getCorner(fight, 1);
        if ((firstCorner == null) || (secondCorner == null)) {
            return;
        }

        loadImageView(context, firstCorner.getFighter(), R.id.first_fighter_image_view);
		loadImageView(context, secondCorner.getFighter(), R.id.second_fighter_image_view);
		loadName(context, firstCorner.getFighter(), R.id.first_fighter_name_text_view);
		loadName(context, secondCorner.getFighter(), R.id.second_fighter_name_text_view);
		loadRecord(context, firstCorner.getFighter(), R.id.first_fighter_record_text_view);
		loadRecord(context, secondCorner.getFighter(), R.id.second_fighter_record_text_view);
	}
	
}
