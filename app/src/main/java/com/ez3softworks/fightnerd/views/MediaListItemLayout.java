package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Article;
import com.ez3softworks.fightnerd.stores.PicassoManager;
import com.ez3softworks.fightnerd.utilities.DateUtilities;
import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.ez3softworks.fightnerd.utilities.StringUtilities;
import com.ez3softworks.fightnerd.utilities.TypefaceUtilities;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Date;

public abstract class MediaListItemLayout<T> extends RelativeLayout {

	protected static final String TAG = "MediaListItemLayout";
	protected static int ITEM_COUNT = 0;
	protected static final int[] BACKGROUND_COLORS = {0xFF323232,
											  		0xFF4B4B4B,
											  		0xFF646464,
											  		0xFF7D7D7D};
	public MediaListItemLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public MediaListItemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MediaListItemLayout(Context context) {
		super(context);
	}
	
	protected void loadTitle(final Context context, final T media, int position) {
		String title = getTitle(media);
		String publicationName = "\n"+getSource(media);
		String verboseTimeDifference = "     "+DateUtilities.getVerboseTimeDifferenceSinceNow(getDate(media));
		String text = title+publicationName+verboseTimeDifference;
//		title = article.getTitle()+" "+text.length();
		text = title+publicationName+verboseTimeDifference;
		
		// Set text appearance
		SpannableString spannableString = new SpannableString(text);
//		if (text.length() > 125) {
//			spannableString = StringUtilities.setSpan(context, spannableString, text, title, R.style.TextAppearance_SmallArticleListItemTitle_CageJunkie);
//			spannableString = StringUtilities.setSpan(context, spannableString, text, publicationName, R.style.TextAppearance_SmallArticleListItemPublicationName_CageJunkie);
//			spannableString = StringUtilities.setSpan(context, spannableString, text, verboseTimeDifference, R.style.TextAppearance_SmallArticleListItemDate_CageJunkie);
//		} else {
			spannableString = StringUtilities.setSpan(context, spannableString, text, title, R.style.TextAppearance_ArticleListItemTitle_CageJunkie);
			spannableString = StringUtilities.setSpan(context, spannableString, text, publicationName, R.style.TextAppearance_ArticleListItemPublicationName_CageJunkie);
			spannableString = StringUtilities.setSpan(context, spannableString, text, verboseTimeDifference, R.style.TextAppearance_ArticleListItemDate_CageJunkie);
//		}
		
		// Set typeface
		spannableString = StringUtilities.setSpan(context, spannableString, text, title, new CustomTypefaceSpan(TypefaceUtilities.getTypeface(context, TypefaceUtilities.ROBOTO_MEDIUM)));
		spannableString = StringUtilities.setSpan(context, spannableString, text, publicationName, new CustomTypefaceSpan(TypefaceUtilities.getTypeface(context, TypefaceUtilities.ROBOTO_REGULAR)));
		spannableString = StringUtilities.setSpan(context, spannableString, text, verboseTimeDifference, new CustomTypefaceSpan(TypefaceUtilities.getTypeface(context, TypefaceUtilities.ROBOTO_LIGHT)));
		spannableString = StringUtilities.setSpan(context, spannableString, text, text, new RelativeSizeSpan(getRelativeSize(text.length())));
		
		final TextView textView = (TextView)this.findViewById(R.id.name_text_view);
		textView.setText(spannableString);
		
		// Set background color
//		if (!(this instanceof ArticleListItemLayout)) {
//			int color = BACKGROUND_COLORS[position%BACKGROUND_COLORS.length];
//			textView.setBackgroundColor(color);
//		} else {
//		}
		textView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_clear));

	}
	
	private float getRelativeSize(int length) {
		float relativeSize = 1.27551f-0.00306122f*length;
		return Math.max(0.5f, Math.min(1.0f, relativeSize));
	}

	protected void loadDimView(Context context, T media, boolean doEnableDimView) {
		final View dimView = (View)this.findViewById(R.id.dim_view);
		if (!doEnableDimView) {
			dimView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_clear));
		} else {
			if (doShowDimView(media)) {
				dimView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_list_item_dim));
			} else {
				dimView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_clear));
			}
		}
	}

	protected void loadImageView(final Context context, final T media) {
		final PicassoImageView imageView = (PicassoImageView)this.findViewById(R.id.image_view);
//		final View textViewLayout = this.findViewById(R.id.text_view_layout);
		final View fadeView = (View)this.findViewById(R.id.fade_view);
		imageView.setDelegate(new PicassoImageView.FadeInDelegate() {

			@Override
			public void onPrepareLoad(Drawable drawable) {
				super.onPrepareLoad(drawable);
			}

			@Override
			protected ImageView getImageView() {
				return imageView;
			}

			@Override
			protected Context getContext() {
				return context;
			}

			@Override
			public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
				super.onBitmapLoaded(bitmap, loadedFrom);
				LogUtilities.d(TAG, "Bitmap loaded: %s (%s)", loadedFrom, getTitle(media));
			}

			@Override
			public void onAnimationStart(Animation animation) {
				super.onAnimationStart(animation);
			}

			@Override
			public void onBitmapFailed(Drawable drawable) {
				super.onBitmapFailed(drawable);
				LogUtilities.e(TAG, "Bitmap failed: %s", getImageUrl(media));
			}
		});

		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		int width = displayMetrics.widthPixels;
		int height = context.getResources().getDimensionPixelSize(R.dimen.list_item_article_height);
		PicassoManager.getPicasso(context)
				.load(getImageUrl(media))
				.resize(width, height)
				.centerCrop()
				.placeholder(R.drawable.placeholder_shape_image)
				.error(R.drawable.placeholder_shape_image)
				.noFade()
				.into((Target) imageView);

	}

	protected int getWidth(Context context) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		int width = displayMetrics.widthPixels;
		return width;
	}

	public void bind(final Context context, T media, int position, boolean doShowDimView) {
		loadTitle(context, media, position);
		loadDimView(context, media, doShowDimView);
		loadImageView(context, media);
	}

	protected abstract String getTitle(T media);
	protected abstract String getImageUrl(T media);
	protected abstract String getSource(T media);
	protected abstract Date getDate(T media);
	protected abstract boolean doShowDimView(T media);

}
