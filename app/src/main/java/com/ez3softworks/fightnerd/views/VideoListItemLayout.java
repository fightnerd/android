package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Article;
import com.ez3softworks.fightnerd.models.Video;
import com.ez3softworks.fightnerd.stores.PicassoManager;
import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

import java.util.Date;

public class VideoListItemLayout extends MediaListItemLayout<Video> {

	protected static final String TAG = "VideoListItemLayout";

    public VideoListItemLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public VideoListItemLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoListItemLayout(Context context) {
        super(context);
    }

    @Override
	protected String getTitle(Video video) {
		return video.getTitle();
	}

	@Override
	protected String getImageUrl(Video video) {
		return video.getImageUrl();
	}

	@Override
	protected String getSource(Video video) {
		return video.getChannel();
	}

	@Override
	protected Date getDate(Video video) {
		return video.getDate();
	}

	@Override
	protected boolean doShowDimView(Video video) {
		return video.isWatched();
	}
}
