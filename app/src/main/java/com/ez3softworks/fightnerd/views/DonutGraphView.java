package com.ez3softworks.fightnerd.views;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.SpannableString;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.ez3softworks.fightnerd.utilities.TypefaceUtilities;

public class DonutGraphView extends View {
	public static final String TAG = "DonutGraphView";
	
	private List<Data> mData;
	private RectF mCanvasRect;
	private Paint mBackgroundPaint;
	private Paint mZeroPaint;
	private TextPaint mTextPaint;
	private float mDonutHoleScale;
	private String mTitle = "99";
	private String mSubtitle = "LOSSES";
	private Rect mTextRect;
	
	public DonutGraphView(Context context) {
		super(context);
	}

	public DonutGraphView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		mData = new ArrayList<Data>();
		mCanvasRect = new RectF();
		mBackgroundPaint = new Paint();
		mBackgroundPaint.setColor(0x00000000);
		mBackgroundPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		
		mZeroPaint = new Paint();
		mZeroPaint.setColor(context.getResources().getColor(R.color.cagejunkie_black));
		mZeroPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		mDonutHoleScale = 0.775f;
		
		mTextPaint = new TextPaint();
		mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		
		mTextRect = new Rect();
	}

	public DonutGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
	    super.onDraw(canvas);
	    canvas.save();

		// Set canvas size
		int width = 336;
		int height = 336;
		mCanvasRect.set(0, 0, width, height);
		
		float scaleX;
		float scaleY;
		float layoutWidth = this.getLayoutParams().width;
		float layoutHeight = this.getLayoutParams().height;
		scaleX = (float)layoutWidth/(float)width;
		scaleY = (float)layoutHeight/(float)height;
		LogUtilities.d(TAG, "scaleX: %f\tscaleY: %f", scaleX, scaleY);
		LogUtilities.d(TAG, "width: %f\theight: %f", layoutWidth, layoutHeight);
		canvas.scale(scaleX, scaleY);

		// Add arcs
		float sum = getSum();
		float offset = 45;
		if (sum > 0) {
			for (Data data:mData) {
				float angle = 360*(data.getValue()/sum);
				float startAngle = offset;
				float sweepAngle = angle;
				canvas.drawArc(mCanvasRect, startAngle, sweepAngle, true, data.getPaint());
				offset += angle;
			}
		} else {
			canvas.drawArc(mCanvasRect, 0, 360, true, mZeroPaint);
		}
		
		// Add hole
		canvas.drawCircle(width/2, height/2, width*mDonutHoleScale/2, mBackgroundPaint);
		
		// Draw text
		mTextPaint.setTextSize(130f);
		mTextPaint.setColor(getContext().getResources().getColor(R.color.cagejunkie_white));
		mTextPaint.setTypeface(TypefaceUtilities.getTypeface(getContext(), TypefaceUtilities.ROBOTO_LIGHT));
		mTextPaint.setTextAlign(Paint.Align.CENTER);
		mTextPaint.getTextBounds(mTitle, 0, mTitle.length(), mTextRect);
		LogUtilities.d(TAG, "titleRect: %s", mTextRect.toString());
		canvas.drawText(mTitle, width/2, height/2, mTextPaint);
		
		mTextPaint.setTextSize(50f);
		mTextPaint.setColor(getContext().getResources().getColor(R.color.cagejunkie_gray));
		mTextPaint.getTextBounds(mTitle, 0, mTitle.length(), mTextRect);
		LogUtilities.d(TAG, "subtitleRect: %s", mTextRect.toString());
		LogUtilities.d(TAG, "%d x %d", width, height);
		canvas.drawText(mSubtitle, width/2, height/2+75, mTextPaint);		
		
		canvas.restore();
	}
	
	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String title) {
		mTitle = title;
	}

	public String getSubtitle() {
		return mSubtitle;
	}

	public void setSubtitle(String subtitle) {
		mSubtitle = subtitle;
	}

	private float getSum() {
		float sum = 0;
		for (Data data:mData) {
			sum += data.getValue();
		}
		return sum;
	}
	
	public void setDonutHoleScale(float donutHoleScale) {
		mDonutHoleScale = donutHoleScale;
	}

	public void setBackgroundColor(int backgroundColor) {
		mBackgroundPaint.setColor(backgroundColor);
	}

	public List<Data> getData() {
		return mData;
	}

	public void setData(List<Data> data) {
		mData = data;
	}

	public static class Data {
		private final Paint mPaint;
		private final float mValue;
		
		public Data(float value, int color) {
			super();
			mPaint = new Paint();
			mPaint.setColor(color);
			mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
			mValue = value;
		}

		public Paint getPaint() {
			return mPaint;
		}

		public float getValue() {
			return mValue;
		}
	}
}
