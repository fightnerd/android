package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.os.Parcel;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Article;
import com.ez3softworks.fightnerd.utilities.DateUtilities;
import com.ez3softworks.fightnerd.utilities.StringUtilities;
import com.ez3softworks.fightnerd.utilities.TypefaceUtilities;

public class ImagelessArticleListItemLayout extends RelativeLayout {

	protected static final String TAG = "ImagelessArticleListItemLayout";
	protected static int ITEM_COUNT = 0;
	protected static final int[] BACKGROUND_COLORS = {0xFF323232,
											  		0xFF4B4B4B,
											  		0xFF646464,
											  		0xFF7D7D7D};
	public ImagelessArticleListItemLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ImagelessArticleListItemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ImagelessArticleListItemLayout(Context context) {
		super(context);
	}
	
	protected void loadTitle(final Context context, final Article article, int position) {
		String title = article.getTitle();
		String publicationName = "\n"+article.getPublication();
		String verboseTimeDifference = "     "+DateUtilities.getVerboseTimeDifferenceSinceNow(article.getDate());
		String text = title+publicationName+verboseTimeDifference;
//		title = article.getTitle()+" "+text.length();
		text = title+publicationName+verboseTimeDifference;
		
		// Set text appearance
		SpannableString spannableString = new SpannableString(text);
//		if (text.length() > 125) {
//			spannableString = StringUtilities.setSpan(context, spannableString, text, title, R.style.TextAppearance_SmallArticleListItemTitle_CageJunkie);
//			spannableString = StringUtilities.setSpan(context, spannableString, text, publicationName, R.style.TextAppearance_SmallArticleListItemPublicationName_CageJunkie);
//			spannableString = StringUtilities.setSpan(context, spannableString, text, verboseTimeDifference, R.style.TextAppearance_SmallArticleListItemDate_CageJunkie);
//		} else {
			spannableString = StringUtilities.setSpan(context, spannableString, text, title, R.style.TextAppearance_ArticleListItemTitle_CageJunkie);
			spannableString = StringUtilities.setSpan(context, spannableString, text, publicationName, R.style.TextAppearance_ArticleListItemPublicationName_CageJunkie);
			spannableString = StringUtilities.setSpan(context, spannableString, text, verboseTimeDifference, R.style.TextAppearance_ArticleListItemDate_CageJunkie);
//		}
		
		// Set typeface
		spannableString = StringUtilities.setSpan(context, spannableString, text, title, new CustomTypefaceSpan(TypefaceUtilities.getTypeface(context, TypefaceUtilities.ROBOTO_MEDIUM)));
		spannableString = StringUtilities.setSpan(context, spannableString, text, publicationName, new CustomTypefaceSpan(TypefaceUtilities.getTypeface(context, TypefaceUtilities.ROBOTO_REGULAR)));
		spannableString = StringUtilities.setSpan(context, spannableString, text, verboseTimeDifference, new CustomTypefaceSpan(TypefaceUtilities.getTypeface(context, TypefaceUtilities.ROBOTO_LIGHT)));
		spannableString = StringUtilities.setSpan(context, spannableString, text, text, new RelativeSizeSpan(getRelativeSize(text.length())));
		
		final TextView textView = (TextView)this.findViewById(R.id.name_text_view);
		textView.setText(spannableString);
		
		// Set background color
		if (!(this instanceof ArticleListItemLayout)) {
			int color = BACKGROUND_COLORS[position%BACKGROUND_COLORS.length];
			textView.setBackgroundColor(color);
		} else {
			textView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_clear));
		}
	}
	
	private float getRelativeSize(int length) {
		float relativeSize = 1.27551f-0.00306122f*length;
		return Math.max(0.5f, Math.min(1.0f, relativeSize));
	}

	protected void loadDimView(Context context, Article article, boolean doShowDimView) {
		final View dimView = (View)this.findViewById(R.id.dim_view);
		if (!doShowDimView) {
			dimView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_clear));
		} else {
			if (article.isRead()) {
				dimView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_list_item_dim));
			} else {
				dimView.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_clear));
			}
		}
	}

	public void bindArticle(final Context context, Article article, int position, boolean doShowDimView) {
		loadTitle(context, article, position);
		loadDimView(context, article, doShowDimView);
	}
	
}
