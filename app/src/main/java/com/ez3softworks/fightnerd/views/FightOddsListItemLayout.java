package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.util.AttributeSet;

import com.ez3softworks.fightnerd.models.Fight;

public class FightOddsListItemLayout extends FightListItemLayout {
	
	protected static final String TAG = "FightOddsListItemLayout";

	public FightOddsListItemLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public FightOddsListItemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FightOddsListItemLayout(Context context) {
		super(context);
	}
		
	public void bind(final Context context, Fight fight) {
		super.bind(context, fight);
		bindOdds(context, fight);
	}

}
