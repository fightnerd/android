package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.text.SpannableString;
import android.util.AttributeSet;
import android.widget.TextView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Ranking;
import com.ez3softworks.fightnerd.utilities.LocaleUtilities;
import com.ez3softworks.fightnerd.utilities.SpannableStringBuilder;
import com.ez3softworks.fightnerd.utilities.TypefaceUtilities;

public class RankingListItemLayout extends FighterListItemLayout {

	protected static final String TAG = "FighterListItemRankingLayout";

	public RankingListItemLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public RankingListItemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public RankingListItemLayout(Context context) {
		super(context);
	}
	
	private void bindRank(Context context, Ranking ranking) {
		TextView textView = (TextView)this.findViewById(R.id.rank_text_view);
		int rankOffset = (ranking.getWeightClass().equals(Ranking.WeightClass.PoundForPound.getDisplayName())) ? 1:0;
		String text = (ranking.isChampion()) ? "\u25C8":ranking.getRank()+rankOffset+"";
		SpannableString spannableString = new SpannableStringBuilder(text)
			.setSpan(context, text, (ranking.isChampion()) ? R.style.TextAppearance_FighterListItemRankChampion_CageJunkie:R.style.TextAppearance_FighterListItemRank_CageJunkie)
			.setSpan(context, text, TypefaceUtilities.ROBOTO_MEDIUM)
			.build();
		textView.setText(spannableString);
	}

	private void bindRankDelta(Context context, Ranking ranking) {
		TextView textView = (TextView)this.findViewById(R.id.rank_delta_text_view);

		if (ranking.getRankDelta() == 0) {
			textView.setText("");
			return;
		}
		
		String text = String.format(LocaleUtilities.DEFAULT_LOCALE, "%s%d", (ranking.getRankDelta() > 0) ? "\u25B2":"\u25BC", Math.abs(ranking.getRankDelta()));
		SpannableString spannableString = new SpannableStringBuilder(text)
			.setSpan(context, text, (ranking.getRankDelta() > 0) ? R.style.TextAppearance_FighterListItemRankDeltaPositive_CageJunkie:R.style.TextAppearance_FighterListItemRankDeltaNegative_CageJunkie)
			.setSpan(context, text, TypefaceUtilities.ROBOTO_MEDIUM)
			.build();
		textView.setText(spannableString);
	}

	public void bind(final Context context, Ranking ranking, int position) {
		super.bind(context, ranking.getFighter(), position, false, "");
        bindRank(context, ranking);
		bindRankDelta(context, ranking);
	}
	
}
