package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Event;
import com.ez3softworks.fightnerd.utilities.LocaleUtilities;
import com.ez3softworks.fightnerd.utilities.SpannableStringBuilder;
import com.ez3softworks.fightnerd.utilities.TypefaceUtilities;

import java.text.SimpleDateFormat;

public class EventListItemLayout extends FightListItemLayout {

	protected static final String TAG = "EventListItemLayout";

	public EventListItemLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public EventListItemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public EventListItemLayout(Context context) {
		super(context);
	}
	
	protected void bindTitle(final Context context, Event event) {
		TextView textView = (TextView)this.findViewById(R.id.title_text_view);
		String text = event.getTitle().toUpperCase(LocaleUtilities.DEFAULT_LOCALE);
		SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text)
															.setSpan(context, text, R.style.TextAppearance_UpcomingEventListItemTitle_CageJunkie)
															.setSpan(context, text, TypefaceUtilities.ROBOTO_REGULAR);
		textView.setText(spannableStringBuilder.fit(context, textView).build());
	}
	
	protected void bindSubtitle(final Context context, Event event) {
		TextView textView = (TextView)this.findViewById(R.id.subtitle_text_view);
		String text = event.getSubtitle();
		SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text)
															.setSpan(context, text, R.style.TextAppearance_UpcomingEventListItemSubtitle_CageJunkie)
															.setSpan(context, text, TypefaceUtilities.ROBOTO_REGULAR);
		textView.setText(spannableStringBuilder.fit(context, textView).build());
	}
	
	protected void bindDate(final Context context, Event event) {
		TextView textView = (TextView)this.findViewById(R.id.date_text_view);
		if (event.isTimeValid()) {
			textView.setLineSpacing(0, 1.0f);
			textView.setMaxLines(3);
			String month = new SimpleDateFormat("MMM").format(event.getDate()).toUpperCase(LocaleUtilities.DEFAULT_LOCALE) + "\n";
			String day = new SimpleDateFormat("d").format(event.getDate())+"\n";
			String time = new SimpleDateFormat("hh:mm a").format(event.getDate());
			String text = month+day+time;
			SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text)
																.setSpan(context, month, R.style.TextAppearance_UpcomingEventListItemMonth_CageJunkie)
																.setSpan(context, day, R.style.TextAppearance_UpcomingEventListItemDay_CageJunkie)
																.setSpan(context, time, R.style.TextAppearance_UpcomingEventListItemTime_CageJunkie)
																.setSpan(context, month, TypefaceUtilities.ROBOTO_LIGHT)
																.setSpan(context, day, TypefaceUtilities.ROBOTO_MEDIUM)	
																.setSpan(context, time, TypefaceUtilities.ROBOTO_MEDIUM);
			textView.setText(spannableStringBuilder.setSpan(context, text, new RelativeSizeSpan(0.65f)).build());
		} else {
			textView.setLineSpacing(0, 0.75f);
			textView.setMaxLines(3);
			String month = new SimpleDateFormat("MMM").format(event.getDate()).toUpperCase(LocaleUtilities.DEFAULT_LOCALE) + "\n";
			String day = new SimpleDateFormat("d").format(event.getDate());
			String text = month+day;
			SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text)
																.setSpan(context, month, R.style.TextAppearance_UpcomingEventListItemMonth_CageJunkie)
																.setSpan(context, day, R.style.TextAppearance_UpcomingEventListItemDay_CageJunkie)
																.setSpan(context, month, TypefaceUtilities.ROBOTO_LIGHT)
																.setSpan(context, day, TypefaceUtilities.ROBOTO_MEDIUM);
			textView.setText(spannableStringBuilder.build());
		}
	}
	
	public void bind(final Context context, Event event) {
		bind(context, Event.getMainEvent(event));
		bindTitle(context, event);
		bindSubtitle(context, event);
		bindDate(context, event);
	}
	
}
