package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;

public class PicassoImageView extends ImageView implements Target {
	protected static final String TAG = "PicassoImageView";

	private Delegate mDelegate;
	
	public PicassoImageView(Context context) {
		super(context);
	}
	
	public PicassoImageView(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
	}

	public PicassoImageView(Context context, AttributeSet attributeSet, int defStyle) {
		super(context, attributeSet, defStyle);
	}

	@Override
	public void onBitmapFailed(Drawable drawable) {
		mDelegate.onBitmapFailed(drawable);
	}

	@Override
	public void onBitmapLoaded(Bitmap bitmap, LoadedFrom loadedFrom) {
		mDelegate.onBitmapLoaded(bitmap, loadedFrom);
	}

	@Override
	public void onPrepareLoad(Drawable drawable) {
		mDelegate.onPrepareLoad(drawable);
	}
	
	public Delegate getDelegate() {
		return mDelegate;
	}

	public void setDelegate(Delegate delegate) {
		mDelegate = delegate;
	}


	public interface Delegate {

		void onBitmapFailed(Drawable drawable);

		void onPrepareLoad(Drawable drawable);

		void onBitmapLoaded(Bitmap bitmap, LoadedFrom loadedFrom);
		
	}
	
	public abstract static class FadeInDelegate implements Delegate {
		private boolean mDidFireLastRequestCreator = false;
		
		@Override
		public void onPrepareLoad(Drawable drawable) {
			getImageView().setImageDrawable(drawable);
		}

		@Override
		public void onBitmapFailed(Drawable drawable) {
			if (!mDidFireLastRequestCreator && getLastRequestCreator()!=null) {
				mDidFireLastRequestCreator = true;
				getLastRequestCreator().into((Target)getImageView());
			} else {
				getImageView().setImageDrawable(drawable);
			}
		}

		@Override
		public void onBitmapLoaded(Bitmap bitmap, LoadedFrom loadedFrom) {
			getImageView().setImageBitmap(bitmap);
			if (!loadedFrom.equals(LoadedFrom.MEMORY) && isAnimationEnabled()) {
				Animation fadeAnimation = AnimationUtils.loadAnimation(getContext(), getAnimationId());
				fadeAnimation.setAnimationListener(new AnimationListener() {
					@Override
					public void onAnimationStart(Animation animation) {
						FadeInDelegate.this.onAnimationStart(animation);
					}
					
					@Override
					public void onAnimationRepeat(Animation animation) {
						
					}
					
					@Override
					public void onAnimationEnd(Animation animation) {
						FadeInDelegate.this.onAnimationEnd(animation);
					}
				});
				getImageView().startAnimation(fadeAnimation);
			} else {
				FadeInDelegate.this.onDidNotLoadImageFromMemory(loadedFrom);
			}
		}

		protected boolean isAnimationEnabled() {
			return true;
		}

		protected void onAnimationStart(Animation animation) {
		}

		protected void onAnimationEnd(Animation animation) {
		}

		protected void onDidNotLoadImageFromMemory(LoadedFrom loadedFrom) {
		}

		protected int getAnimationId() {
			return R.anim.list_item_image_fade_in;
		}

		protected abstract ImageView getImageView();

		protected abstract Context getContext();
		
		protected RequestCreator getLastRequestCreator() {
			return null;
		}
	}
	
	public abstract static class FlashAndFadeInDelegate implements Delegate {

		@Override
		public void onPrepareLoad(Drawable drawable) {
			getImageView().setImageDrawable(drawable);
			getFadeView().setVisibility(View.VISIBLE);
			getFadeView().setBackgroundColor(Color.TRANSPARENT);
		}

		@Override
		public void onBitmapLoaded(Bitmap bitmap, LoadedFrom loadedFrom) {
			getImageView().setImageBitmap(bitmap);
			if (!loadedFrom.equals(LoadedFrom.NETWORK)) {
				getFadeView().setBackgroundColor(Color.WHITE);
				Animation fadeAnimation = AnimationUtils.loadAnimation(getContext(), getAnimationId());
				fadeAnimation.setAnimationListener(new AnimationListener() {
					
					@Override
					public void onAnimationStart(Animation animation) {
						FlashAndFadeInDelegate.this.onAnimationStart(animation);
					}
					
					@Override
					public void onAnimationRepeat(Animation animation) {
						
					}
					
					@Override
					public void onAnimationEnd(Animation animation) {
						FlashAndFadeInDelegate.this.onAnimationEnd(animation);
					}
				});
				getFadeView().startAnimation(fadeAnimation);
			} else {
				getFadeView().setVisibility(View.INVISIBLE);
			}
		}

		@Override
		public void onBitmapFailed(Drawable drawable) {
			getFadeView().setVisibility(View.INVISIBLE);
		}

		protected void onAnimationStart(Animation animation) {
			
		}

		protected void onAnimationEnd(Animation animation) {
			getFadeView().setVisibility(View.INVISIBLE);
		}
		
		protected int getAnimationId() {
			return R.anim.list_item_image_fade_out;
		}

		protected abstract ImageView getImageView();

		protected abstract View getFadeView();

		protected abstract Context getContext();
		
	}

}
