package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.util.AttributeSet;

import com.ez3softworks.fightnerd.models.Event;

public class EventOddsListItemLayout extends EventListItemLayout {

	protected static final String TAG = "EventOddsListItemLayout";

	public EventOddsListItemLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public EventOddsListItemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public EventOddsListItemLayout(Context context) {
		super(context);
	}
	
	public void bind(final Context context, Event event) {
		super.bind(context, event);
		bindOdds(context, Event.getMainEvent(event));
	}	
}
