package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.utilities.SpannableStringBuilder;
import com.ez3softworks.fightnerd.utilities.TypefaceUtilities;

public class NavigationTitleListItemLayout extends RelativeLayout {

	protected static final String TAG = "ArticleListItemLayout";

	public NavigationTitleListItemLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public NavigationTitleListItemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public NavigationTitleListItemLayout(Context context) {
		super(context);
	}

	private void loadTitle(final Context context, String title) {
		TextView textView = (TextView)this.findViewById(R.id.title_text_view);
		SpannableString spannableString = new SpannableStringBuilder(title)
											.setSpan(context, title, R.style.TextAppearance_NavigationTitle_CageJunkie)
											.setSpan(context, title, TypefaceUtilities.ROBOTO_LIGHT)
											.fit(context, textView)
											.build();
		textView.setText(spannableString);
	}

	private void loadImage(final Context context, Drawable drawable) {
		ImageView imageView = (ImageView)this.findViewById(R.id.image_view);
		imageView.setImageDrawable(drawable);
	}

	public void loadNavigationTitle(final Context context, String title, Drawable drawable) {
		loadTitle(context, title);
		loadImage(context, drawable);
	}

}
