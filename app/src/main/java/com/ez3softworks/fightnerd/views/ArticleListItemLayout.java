package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Article;
import com.ez3softworks.fightnerd.stores.PicassoManager;
import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

public class ArticleListItemLayout extends ImagelessArticleListItemLayout {

	protected static final String TAG = "ArticleListItemLayout";

	public ArticleListItemLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ArticleListItemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ArticleListItemLayout(Context context) {
		super(context);
	}
	
	protected void loadImageView(final Context context, final Article article) {
		if (article.getImageUrl() == null || article.getImageUrl().isEmpty()) {
			return;
		}
		final PicassoImageView imageView = (PicassoImageView)this.findViewById(R.id.image_view);
//		final View textViewLayout = this.findViewById(R.id.text_view_layout);
		final View fadeView = (View)this.findViewById(R.id.fade_view);
		imageView.setDelegate(new PicassoImageView.FadeInDelegate() {
			
			@Override
			public void onPrepareLoad(Drawable drawable) {
				super.onPrepareLoad(drawable);
			}
			
			@Override
			protected ImageView getImageView() {
				return imageView;
			}
			
			@Override
			protected Context getContext() {
				return context;
			}
			
			@Override
			public void onBitmapLoaded(Bitmap bitmap, LoadedFrom loadedFrom) {
				super.onBitmapLoaded(bitmap, loadedFrom);
				LogUtilities.d(TAG, "Bitmap loaded: %s (%s)", loadedFrom, article.getTitle());
			}
			
			@Override
			public void onAnimationStart(Animation animation) {
				super.onAnimationStart(animation);
			}
			
			@Override
			public void onBitmapFailed(Drawable drawable) {
				super.onBitmapFailed(drawable);
				LogUtilities.e(TAG, "Bitmap failed: %s", article.getImageUrl());				
			}
		});
		
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		int width = displayMetrics.widthPixels;
		int height = context.getResources().getDimensionPixelSize(R.dimen.list_item_article_height);
		PicassoManager.getPicasso(context)
					  .load(article.getImageUrl())
                .resize(width, height)
					  .centerCrop()
					  .placeholder(R.drawable.placeholder_shape_image)
					  .error(R.drawable.placeholder_shape_image)
					  .noFade()
					  .into((Target) imageView);

    }

    protected int getWidth(Context context) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		int width = displayMetrics.widthPixels;
		return width;
	}
	
	public void bindArticle(final Context context, Article article, int position, boolean doShowDimView) {
		super.bindArticle(context, article, position, doShowDimView);
		loadImageView(context, article);
	}
	
}
