package com.ez3softworks.fightnerd.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.models.Fighter;
import com.ez3softworks.fightnerd.stores.PicassoManager;
import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.ez3softworks.fightnerd.utilities.StringUtilities;
import com.ez3softworks.fightnerd.utilities.TypefaceUtilities;
import com.ez3softworks.fightnerd.views.CustomTypefaceSpan;
import com.ez3softworks.fightnerd.views.PicassoImageView;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

public class FighterListItemLayout extends RelativeLayout {

	protected static final String TAG = "FighterListItemLayout";

	public FighterListItemLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public FighterListItemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FighterListItemLayout(Context context) {
		super(context);
	}
	
	@Override    
	protected void onFinishInflate() {      
	  super.onFinishInflate();
	}    
	
	private void loadImageView(final Context context, final Fighter fighter) {
		final PicassoImageView imageView = (PicassoImageView)this.findViewById(R.id.image_view);

		// Fuck you Google...
		imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.fighter_unknown_image));

//		if (fighter.getImageUrl() != null && !fighter.getImageUrl().isEmpty()) {
//			int width = (int) context.getResources().getDimension(R.dimen.list_item_fighter_image_width);
//			int height = (int) context.getResources().getDimension(R.dimen.list_item_fighter_image_height);
//			imageView.setDelegate(new PicassoImageView.FadeInDelegate(){
//				@Override
//				public void onBitmapLoaded(Bitmap bitmap, LoadedFrom loadedFrom) {
//					super.onBitmapLoaded(bitmap, loadedFrom);
//					LogUtilities.d(TAG, "Bitmap loaded: %s (%s)", loadedFrom, fighter.getFullName());
//				}
//
//				@Override
//				public ImageView getImageView() {
//					return imageView;
//				}
//
//				@Override
//				public Context getContext() {
//					return context;
//				}
//			});
//			PicassoManager.getPicasso(context).load(fighter.getImageUrl())
//					.resize(width, height)
//					.placeholder(R.drawable.placeholder_shape_image)
//					.error(R.drawable.fighter_unknown_image)
//					.noFade()
//					.into((Target) imageView);
//		} else {
//			// Add unknown image...
//			imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.fighter_unknown_image));
//		}
	}

	private void loadBackgroundColor(Context context, int position, boolean isHighlighted) {
		View highlightedView = (View)this.findViewById(R.id.highlighted_view);
		if (isHighlighted) {
			this.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_list_item_light_gray));
		} else {
			if (position % 2 == 0) {
				this.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_list_item_light_gray));
			} else {
				this.setBackgroundColor(context.getResources().getColor(R.color.cagejunkie_list_item_dark_gray));
			}
		}
		highlightedView.setVisibility(isHighlighted ? View.VISIBLE:View.INVISIBLE);
	}

	private void loadWeightClass(Context context, Fighter fighter) {
		TextView textView = (TextView)this.findViewById(R.id.weight_class_text_view);
		
		String text = fighter.getWeightClass();
		SpannableString spannableString = new SpannableString(text);
		spannableString = StringUtilities.setSpan(context, spannableString, text, text, R.style.TextAppearance_FighterListItemWeightClass_CageJunkie);
		spannableString = StringUtilities.setSpan(context, spannableString, text, text, new CustomTypefaceSpan(TypefaceUtilities.getTypeface(context, TypefaceUtilities.ROBOTO_REGULAR)));

		textView.setText(spannableString);
	}

	private void loadName(Context context, Fighter fighter, String searchText) {	
		TextView textView = (TextView)this.findViewById(R.id.name_text_view);
		
		String text = fighter.getFullName();
		SpannableString spannableString = new SpannableString(text);
		spannableString = StringUtilities.setSpan(context, spannableString, text, text, R.style.TextAppearance_FighterListItemName_CageJunkie);
		CustomTypefaceSpan regularTypefaceSpan = new CustomTypefaceSpan(TypefaceUtilities.getTypeface(context, TypefaceUtilities.ROBOTO_REGULAR));
		spannableString = StringUtilities.setSpan(context, spannableString, text, text, regularTypefaceSpan);
		if (searchText != null && !searchText.isEmpty()) {
			CustomTypefaceSpan boldTypefaceSpan = new CustomTypefaceSpan(TypefaceUtilities.getTypeface(context, TypefaceUtilities.ROBOTO_BOLD));
//			spannableString = StringUtilities.setSpan(context, spannableString, text, searchText, true, boldTypefaceSpan);
			spannableString = StringUtilities.setSpan(context, spannableString, text, searchText, true, R.style.TextAppearance_FighterListItemHighlightedName_CageJunkie);
		}
		spannableString = StringUtilities.setSpan(context, spannableString, text, text, new RelativeSizeSpan(getRelativeSize(text)));
		
		textView.setText(spannableString);
	}

	private float getRelativeSize(String text) {
		float relativeSize = Math.max(Math.min(2.1f-0.05f*text.length(), 1.0f), 0.75f);
		return relativeSize;
	}

	public void bind(final Context context, Fighter fighter, int position, boolean isHighlighted, String searchText) {
		loadBackgroundColor(context, position, isHighlighted);
		loadName(context, fighter, searchText);
		loadWeightClass(context, fighter);
		loadImageView(context, fighter);
	}
	
}
