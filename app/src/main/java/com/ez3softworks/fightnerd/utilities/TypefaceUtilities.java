package com.ez3softworks.fightnerd.utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.util.LruCache;

public class TypefaceUtilities {

	public static final String ROBOTO_BOLD = "fonts/Roboto-Bold.ttf";
	public static final String ROBOTO_MEDIUM = "fonts/Roboto-Medium.ttf";
	public static final String ROBOTO_REGULAR = "fonts/Roboto-Regular.ttf";
	public static final String ROBOTO_CONDENSED_BOLD = "fonts/RobotoCondensed-Bold.ttf";
	public static final String ROBOTO_CONDENSED_REGULAR = "fonts/RobotoCondensed-Regular.ttf";
	public static final String ROBOTO_LIGHT = "fonts/Roboto-Light.ttf";
	public static final String ROBOTO_LIGHT_ITALIC = "fonts/Roboto-LightItalic.ttf";
	public static final String ROBOTO_ITALIC = "fonts/Roboto-Italic.ttf";
	public static final String ROBOTO_THIN = "fonts/Roboto-Thin.ttf";
	
	private static LruCache<String, Typeface> sTypefaceCache = new LruCache<String, Typeface>(12);

	public static Typeface getTypeface(Context context, String typefaceName) {
		Typeface typeface = sTypefaceCache.get(typefaceName);
		if (typeface == null) {
			typeface = Typeface.createFromAsset(context.getAssets(), typefaceName);
			sTypefaceCache.put(typefaceName, typeface);
		}
		return typeface;
	}
}
