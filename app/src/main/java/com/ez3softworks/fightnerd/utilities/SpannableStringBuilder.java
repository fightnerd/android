package com.ez3softworks.fightnerd.utilities;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.RelativeSizeSpan;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.TextView;

import com.ez3softworks.fightnerd.utilities.StringUtilities.Range;
import com.ez3softworks.fightnerd.views.CustomTypefaceSpan;

public class SpannableStringBuilder {
	public static final String TAG = "SpannableStringBuilder";

	private final String mSourceText;
	private final SpannableString mSpannableString;

	public SpannableStringBuilder(String source) {
		mSourceText = source;
		mSpannableString = new SpannableString(mSourceText);
	}
	
	public SpannableStringBuilder setSpan(Context context, String text, String typefaceName) {
		return setSpan(context, text, false, new CustomTypefaceSpan(TypefaceUtilities.getTypeface(context, typefaceName)));
	}
	
	public SpannableStringBuilder setSpan(Context context, String text, Object span) {
		return setSpan(context, text, false, span);
	}

	public SpannableStringBuilder setSpan(Context context, String text, int resourceId) {
		return setSpan(context, text, false, resourceId);
	}

	public SpannableStringBuilder setSpan(Context context, String text, boolean isCaseInsensitive, int textAppearanceResourceId) {
		Range range = StringUtilities.getRange(mSourceText, text, isCaseInsensitive);
		if (range != null) {
			mSpannableString.setSpan(new TextAppearanceSpan(context, textAppearanceResourceId), range.getStart(), range.getEnd(), 0);
		}
		return this;
	}
	
	public SpannableStringBuilder setSpan(Context context, String text, boolean isCaseInsensitive, Object span) {
		Range range = StringUtilities.getRange(mSourceText, text, isCaseInsensitive);
		if (range != null) {
			mSpannableString.setSpan(span, range.getStart(), range.getEnd(), 0);
		}
		return this;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public SpannableStringBuilder fit(final Context context, final TextView textView) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			textView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
				
				@Override
				public void onLayoutChange(View v, int left, int top, int right,
						int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
	                fit(context, right-left);
	                textView.setText(mSpannableString);
	                textView.removeOnLayoutChangeListener(this);
				}
			});
		} else {
			textView.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
				
				@Override
				public boolean onPreDraw() {
	                fit(context, textView.getMeasuredWidth());
	                textView.setText(mSpannableString);
	                textView.getViewTreeObserver().removeOnPreDrawListener(this);
					return false;
				}
			});
		}
		return this;
	}

	public SpannableString build() {
		return mSpannableString;
	}

	private void fit(final Context context, final float textViewWidth) {
		setSpan(context, mSourceText, new RelativeSizeSpan(1.0f){
			private void resize(TextPaint textPaint) {
				float textWidth = textPaint.measureText(mSpannableString, 0, mSpannableString.length());
				if (textWidth > textViewWidth) {
					float scale = (textViewWidth*0.95f)/textWidth;
					textPaint.setTextSize(textPaint.getTextSize()*scale);
				}
			}
			
			@Override
			public void updateDrawState(TextPaint textPaint) {
				super.updateDrawState(textPaint);
				resize(textPaint);
			}

			@Override
			public void updateMeasureState(TextPaint textPaint) {
				super.updateMeasureState(textPaint);
				resize(textPaint);
			}
		});
	}


}
