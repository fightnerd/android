package com.ez3softworks.fightnerd.utilities;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONObjectUtilities {
	
	public static void put(JSONObject jsonObject, String name, String value) {
		try {
			jsonObject.put(name, value);
		} catch (JSONException e) {
		}
	}

	public static void put(JSONObject jsonObject, String name, boolean value) {
		try {
			jsonObject.put(name, value);
		} catch (JSONException e) {
		}
	}
	
	public static void put(JSONObject jsonObject, String name, int value) {
		try {
			jsonObject.put(name, value);
		} catch (JSONException e) {
		}
	}

	
	public static void put(JSONObject jsonObject, String name, double value) {
		try {
			jsonObject.put(name, value);
		} catch (JSONException e) {
		}
	}
	
	public static void put(JSONObject jsonObject, String name, Date value) {
		try {
			jsonObject.put(name, DateUtilities.toISO8601(value));
		} catch (JSONException e) {
		}
	}

	public static String getString(JSONObject jsonObject, String name) {
		String data = null;
		try {
			data = jsonObject.getString(name);
		} catch (JSONException e) {
		}
		if (data!=null && data.equals("null")) {
			data = null;
		}
		return data;
	}
	
	public static int getInt(JSONObject jsonObject, String name, int defaultValue) {
		int data = defaultValue;
		try {
			data = jsonObject.getInt(name);
		} catch (JSONException e) {
		}
		return data;
	}
	
	public static boolean getBoolean(JSONObject jsonObject, String name, boolean defaultValue) throws JSONException {
		boolean data = defaultValue;
		data = jsonObject.getBoolean(name);
		return data;
	}

	public static double getDouble(JSONObject jsonObject, String name, double defaultValue) {
		double data = defaultValue;
		try {
			data = jsonObject.getDouble(name);
		} catch (JSONException e) {
		}
		return data;
	}

	public static Date getDate(JSONObject jsonObject, String name) {
		Date data = null;
		data = DateUtilities.fromString(JSONObjectUtilities.getString(jsonObject, name));
		return data;
	}
}
