package com.ez3softworks.fightnerd.utilities;

import java.util.Set;
import java.util.TreeSet;

import android.database.Cursor;
import android.widget.AlphabetIndexer;

public class SectionIndexerUtilities {
	
	public static AlphabetIndexer getAlphabetIndexer(Cursor cursor, int sortedColumnIndex) {
		return new AlphabetIndexer(cursor, sortedColumnIndex, getAlphabet(cursor, sortedColumnIndex));
	}
	
	private static String getAlphabet(Cursor cursor, int columnIndex) {
		Set<String> letters = new TreeSet<String>();
		int position = cursor.getPosition();
		cursor.moveToFirst();
		while (!cursor.isBeforeFirst() && !cursor.isAfterLast()) {
			String letter = cursor.getString(columnIndex);
			if (letter!=null) {
				letters.add(letter);
			}
			cursor.moveToNext();
		}
		cursor.moveToPosition(position);
		
		String alphabet = "";
		for (String letter:letters) {
			alphabet = alphabet + letter;
		}
		return alphabet;
	}


}
