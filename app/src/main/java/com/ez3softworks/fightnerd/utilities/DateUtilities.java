package com.ez3softworks.fightnerd.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.Duration;
import org.joda.time.Hours;
import org.joda.time.JodaTimePermission;
import org.joda.time.Minutes;
import org.joda.time.Period;
import org.joda.time.Seconds;
import org.joda.time.tz.UTCProvider;

import android.util.Log;

public class DateUtilities {
	public static final String TAG = "DateUtilities";
	public static final SimpleDateFormat IS08601_US_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
	public static final SimpleDateFormat IS08601_DAY_US_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

	static {
		IS08601_US_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
	}

	public static Date getUtcNow() {
        DateTime dateTime = DateTime.now(DateTimeZone.forOffsetHours(0));
        Date date = dateTime.toDate();
        return date;
    }
	
	public static Date getRoundedDate(Date date, int nearestMinute, boolean doRoundUp) {
		long nearestMinuteInMilliseconds = nearestMinute * 60 * 1000;
		long time = date.getTime();
		long remainder = date.getTime()%nearestMinuteInMilliseconds;
		if (doRoundUp) {
			return new Date(time+(remainder!=0 ? nearestMinuteInMilliseconds-remainder:0));
		} else {
			return new Date(time-(remainder!=0 ? remainder:0));
		}
	}
	
	public static int getAge(Date dateOfBirth) {
		Period period = new Period(new DateTime(dateOfBirth), new DateTime());
		return period.getYears();
	}
	
	public static String toISO8601(Date date) {
		return toISO8601(date, IS08601_US_FORMAT);
	}

	public static String toISO8601(Date date, SimpleDateFormat dateFormat) {
		if (dateFormat.equals(IS08601_DAY_US_FORMAT)) {
			return IS08601_DAY_US_FORMAT.format(date);
		} else {
			return IS08601_US_FORMAT.format(date);
		}
	}

	public static Date fromString(String dateString) {
		if ((dateString == null) || dateString.equals("null")) {
			return null;
		}
		
		Date ret = null;
		if (dateString.matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {
			try {
				ret = IS08601_DAY_US_FORMAT.parse(dateString);
			} catch (ParseException e) {
				Log.e(TAG, "Failed to convert string to date", e);
			}
		} else if (dateString.matches("\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{1,2}:\\d{1,2}")) {
			if (!dateString.endsWith("Z")) {
				dateString = dateString+"Z";
			}
			try {
				ret = IS08601_US_FORMAT.parse(dateString);
			} catch (ParseException e) {
				Log.e(TAG, "Failed to convert string to date", e);
			}
		}
		return ret;
	}
	
	public static Date getDate(Date date, int days) {
		DateTime dateTime = new DateTime(date);
		dateTime = dateTime.plusDays(days);
		return dateTime.toDate();
	}
	
	public static String getVerboseTimeDifferenceSinceNow(Date date) {
		Days days = Days.daysBetween(new DateTime(date), new DateTime());
		Hours hours = Hours.hoursBetween(new DateTime(date), new DateTime());
		Minutes minutes = Minutes.minutesBetween(new DateTime(date), new DateTime());
		Seconds seconds = Seconds.secondsBetween(new DateTime(date), new DateTime());
		String ret = "";
		if (days.getDays() > 0) {
			ret = days.getDays()+" "+StringUtilities.getPlural("day", days.getDays())+" ago";
		} else if (hours.getHours() > 0) {
			ret = hours.getHours()+" "+StringUtilities.getPlural("hour", hours.getHours())+" ago";
		} else if (minutes.getMinutes() > 0) {
			ret = minutes.getMinutes()+" "+StringUtilities.getPlural("minute", minutes.getMinutes())+" ago";
		} else {
			ret = seconds.getSeconds()+" "+StringUtilities.getPlural("second", seconds.getSeconds())+" ago";
		}
		return ret;
	}	
}
