package com.ez3softworks.fightnerd.utilities;

import android.util.Log;

import com.ez3softworks.fightnerd.BuildConfig;
import com.squareup.picasso.Picasso.LoadedFrom;

public class LogUtilities {
	
	public static void i(String tag, String format, Object ... args) {
		if (BuildConfig.DEBUG) {
			Log.i(tag, String.format(format, args));
		}
	}
	
	public static void e(String tag, String format, Object ... args) {
		if (BuildConfig.DEBUG) {
			if (args.length > 0) {
				if (args[args.length-1] instanceof Throwable) {
					Object[] formatArgs = new Object[args.length-1];
					for (int i=0; i<formatArgs.length; i++) {
						formatArgs[i] = args[i];
					}
					Log.e(tag, String.format(format, formatArgs), (Throwable)args[args.length-1]);
					return;
				}
			} 
			Log.e(tag, String.format(format, args));
		}
	}

	public static void d(String tag, String format, Object ... args) {
		if (BuildConfig.DEBUG) {
			Log.d(tag, String.format(format, args));
		}
	}

	public static void w(String tag, String format, Object ... args) {
		if (BuildConfig.DEBUG) {
			Log.w(tag, String.format(format, args));
		}		
	}
}
