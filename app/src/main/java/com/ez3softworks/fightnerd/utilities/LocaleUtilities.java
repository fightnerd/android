package com.ez3softworks.fightnerd.utilities;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LocaleUtilities {
	public static final String TAG = "LocaleUtilities";
	public static final Locale DEFAULT_LOCALE = Locale.US;
	
	private static final Map<String, String> sISOCountryCodes = new HashMap<String,String>();
	public static String getCountryCode(String countryName) {
		if (sISOCountryCodes.isEmpty()) {
			String[] isoCountries = Locale.getISOCountries();
			for (String isoCountry:isoCountries) {
				Locale locale = new Locale("en", isoCountry);
				sISOCountryCodes.put(locale.getDisplayCountry().toLowerCase(DEFAULT_LOCALE), isoCountry);
			}
			sISOCountryCodes.put("england", "GB");
			sISOCountryCodes.put("scotland", "GB");
			sISOCountryCodes.put("ireland", "IE");
			sISOCountryCodes.put("usa", "US");
			sISOCountryCodes.put("united states", "US");
		}
		String countryCode = sISOCountryCodes.get(countryName.toLowerCase(DEFAULT_LOCALE));
		LogUtilities.d(TAG, "Country code is %s for %s", countryCode, countryName);
		return countryCode;
	}

}
