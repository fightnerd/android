package com.ez3softworks.fightnerd.utilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;

public class StringUtilities {
	public static final String TAG = "StringUtilities";
	private static final double CENTIMETERS_TO_INCHES = 0.393701d;
	private static final double KILOGRAMS_TO_POUNDS = 2.20462d;
	
	private static final Set<String> GIBBERISHES = new HashSet<String>();
    public static String getGibberish() {
    	String gibberish;
    	do {
        	int length = (int)(1+Math.random()*24);
        	gibberish = "";
        	for (int i = 0; i<length; i++) {
        		char min = 'A';
        		char max = 'Z';
        		char character = (char)(min+Math.random()*(max-min));
        		gibberish = String.format("%s%c",gibberish, character);
        	}
    	} while (GIBBERISHES.contains(gibberish));
    	GIBBERISHES.add(gibberish);
    	return gibberish;
    }

	public static String join(String[] strings, String separator) {
		String ret = "";
		int size = strings.length;
		for (int i = 0; i<size; i++) {
			ret = join(ret, strings[i], separator, size, i==0, i==(size-1));
		}
		return ret;
	}
	
	public static String join(List<String> strings, String separator) {
		String ret = "";
		int i = 0;
		int size = strings.size();
		for (String string:strings) {
			ret = join(ret, string, separator, size, i==0, i==(size-1));
			i++;
		}
		return ret;
	}
	
	private static String join(String firstString, String secondString, String separator, int size, boolean isFirst, boolean isLast) {
		String ret = firstString;
		if (isFirst) {
			ret = secondString;
		} else {
			ret = firstString+secondString;
		}
		if (size>1 && !isLast) {
			ret = ret+separator;
		}
		return ret;
	}
	
	public static Range getRange(String text, String subText, boolean isCaseInsensitive) {
		int startIndex;
		if (isCaseInsensitive) {
			startIndex = text.toLowerCase(Locale.US).indexOf(subText.toLowerCase(Locale.US));
		} else {
			startIndex = text.indexOf(subText);
		}
		Range range = null;
		if (startIndex >= 0) {
			range = new Range(startIndex, startIndex+subText.length(), subText.length());
		}
		return range;
	}
	
	public static SpannableString setSpan(Context context, SpannableString spannableString, String originalString, String subString, Object span) {
		return setSpan(context, spannableString, originalString, subString, false, span);
	}

	public static SpannableString setSpan(Context context, SpannableString spannableString, String originalString, String subString, int resourceId) {
		return setSpan(context, spannableString, originalString, subString, false, resourceId);
	}

	public static SpannableString setSpan(Context context, SpannableString spannableString, String originalString, String subString, boolean isCaseInsensitive, int resourceId) {
		Range range = getRange(originalString, subString, isCaseInsensitive);
		if (range != null) {
			spannableString.setSpan(new TextAppearanceSpan(context, resourceId), range.getStart(), range.getEnd(), 0);
		}
		return spannableString;
	}
	
	public static SpannableString setSpan(Context context, SpannableString spannableString, String originalString, String subString, boolean isCaseInsensitive, Object span) {
		Range range = getRange(originalString, subString, isCaseInsensitive);
		if (range != null) {
			spannableString.setSpan(span, range.getStart(), range.getEnd(), 0);
		}
		return spannableString;
	}
	
	public static SpannableString getSpannableString(Context context, String text, List<TextStyle> textStyles) {
		SpannableString spannableString = new SpannableString(text);
		for (TextStyle style:textStyles) {
			spannableString = StringUtilities.setSpan(context, spannableString, text, style.getText(), style.getTypeface(context));
			spannableString = StringUtilities.setSpan(context, spannableString, text, style.getText(), style.getResourceId());
		}
		return spannableString;
	}
	
	public static String getUpperBound(String text) {
		String upperBound = text;
		char lastCharacter = upperBound.charAt(upperBound.length()-1);
		if (lastCharacter == Character.MAX_VALUE) {
			upperBound = String.format("%sa", upperBound);
		} else {
			upperBound = String.format("%s%c", upperBound.substring(0, upperBound.length()-1), ++lastCharacter);
//			LogUtilities.d(TAG, "upperBound = %s\tlowerBound = %s", upperBound, text);
		}
		return upperBound;
	}

	public static String getNormalized(String text) {
		String normalized = text.toLowerCase(Locale.US);
		normalized = normalized.replaceAll("\\s+", "");
		normalized = normalized.replace(".", "");
		normalized = normalized.replace("'", "");
		normalized = normalized.replace(",", "");
		normalized = normalized.replace(":", "");
		normalized = normalized.replace(";", "");
		return normalized;
	}


	public static String getHeight(double height) {
		String heightText = "-";
		if (height>0) {
            int inches = (int)(height*CENTIMETERS_TO_INCHES);
            int feet = inches/12;
            inches = inches%12;
            heightText = String.format(LocaleUtilities.DEFAULT_LOCALE, "%d'%d\"", feet, inches);
		}
		return heightText;
	}
	
	public static String getWeight(double weight) {
		String weightText = "-";
		if (weight>0) {
            int pounds = (int) Math.ceil(weight*KILOGRAMS_TO_POUNDS);
            weightText = String.format(LocaleUtilities.DEFAULT_LOCALE, "%d lb", pounds);
		}
		return weightText;
	}
	
	public static String getCapitalized(String text) {
		String[] toks = text.split(" ");
		List<String> capitalizedStrings = new ArrayList<String>();
		for (String tok:toks) {
			tok = tok.toLowerCase(LocaleUtilities.DEFAULT_LOCALE);
			if (tok.equalsIgnoreCase("MMA")) {
				tok = tok.toUpperCase(LocaleUtilities.DEFAULT_LOCALE);
			}
			char[] chars = tok.toCharArray();
			chars[0] = Character.toUpperCase(chars[0]);
			capitalizedStrings.add(new String(chars));
		}
		return join(capitalizedStrings, " ");
	}
	
	public static String getPlural(String text, int count) {
		return count > 1 ? text+"s":text;
	}

	public static class TextStyle {
		private final int mResourceId;
		private final String mTypefaceName;
		private final String mText;
		public TextStyle(String text, int resourceId, String typefaceName) {
			super();
			mResourceId = resourceId;
			mTypefaceName = typefaceName;
			mText = text;
		}
		public Typeface getTypeface(Context context) {
			return TypefaceUtilities.getTypeface(context, mTypefaceName);
		}
		public int getResourceId() {
			return mResourceId;
		}
		public String getTypefaceName() {
			return mTypefaceName;
		}
		public String getText() {
			return mText;
		}
	}

	
	public static class Range {
		private final int mStart;
		private final int mEnd;
		private final int mLength;
		
		public Range(int start, int end, int length) {
			mStart = start;
			mEnd = end;
			mLength = length;
		}
	
		public int getStart() {
			return mStart;
		}
	
		public int getEnd() {
			return mEnd;
		}
	
		public int getLength() {
			return mLength;
		}
	}	
}
