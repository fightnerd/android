package com.ez3softworks.fightnerd.stores;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.IabResult;
import com.android.vending.billing.util.Inventory;
import com.android.vending.billing.util.Purchase;
import com.ez3softworks.fightnerd.utilities.LogUtilities;

public class StoreManager {
	private static final String TAG = "StoreManager";
	
	private static final String PREFERENCESS_FILE = "store";
	private static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgGYfS+jumfa/tZZoSvx3SdhdsKijQTto9KuQHRztI+2MdAXEckUyqVLgUpombqkPEnZMZvdV2t+QRX27zaymjigVwv+0g74VMfE5HSd4zAjMEz+YWBhXzw882LhUO3utHpJsIz+XuX/odVTR/IibswgfzFQ47Yb3qB06UG/NY51YuttfA2cpYgdeZM6MXWZEfPpvPNfrFAhtD0NsxVmwuoBj5mPBLpN2X1lmXR3HglQhAOX8vVtTMetseCTWbf1nnFFffIiRYLUVx5rI5NxWEzvahA/4gduVXhtHiIKd+TAIhUYWnt4HJ43mUTpbJxinUC3YT549LZ10OydV4wIkawIDAQAB";
	public static final String SKU_TEST_PURCHASED = "android.test.purchased";
	public static final String SKU_TEST_CANCELED = "android.test.canceled";
	public static final String SKU_TEST_REFUNDED = "android.test.refunded";
	public static final String SKU_TEST_ITEM_UNAVAILABLE = "android.test.item_unavailable";
	public static final String SKU_DISABLE_ADS = "com.ez3softworks.fightnerd.disableads";
	
	private static StoreManager sStoreManager;
	
	private Context mContext;
	private final SharedPreferences mPreferences;
	private IabHelper mIabHelper;
	private boolean mDidSetup;
	
	// The private constructor forces users to use ArticleManager.get(Context)
	private StoreManager(Context context) {
		mContext = context;
		mPreferences = context.getSharedPreferences(PREFERENCESS_FILE, Context.MODE_PRIVATE);
		mDidSetup = false;
	}
	
	public static StoreManager getInstance(Context context) {
		return getInstance(context, true);
	}

	public static synchronized StoreManager getInstance(Context context, boolean useApplicationContext) {
		if (sStoreManager == null) {
			if (useApplicationContext) {
				sStoreManager = new StoreManager(context.getApplicationContext());
			} else {
				sStoreManager = new StoreManager(context);
			}
		}
		return sStoreManager;
	}
	
	public static void reset() {
		sStoreManager = null;
	}
	
	public void dispose() {
		if (mIabHelper != null) {
			mIabHelper.dispose();
			mIabHelper = null;
		}
	}
	
	public void consume(final Purchase purchase, final IabHelper.OnConsumeFinishedListener listener) {
		getIabHelper().startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					LogUtilities.d(TAG, "Problem setting up In-app Billing: " + result);
				} else {
					getIabHelper().consumeAsync(purchase, listener);
				}
			}
		});
	}

	public void launchPurchaseFlow(final Activity activity, final String sku, final int requestCode, final IabHelper.OnIabPurchaseFinishedListener listener) {
		getIabHelper().startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					LogUtilities.d(TAG, "Problem setting up In-app Billing: " + result);
				} else {
					getIabHelper().launchPurchaseFlow(activity, sku, requestCode, listener);
				}
			}
		});
	}

	public void hasPurchase(final String sku, final IabHelper.QueryInventoryFinishedListener listener) {
		getIabHelper().startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					LogUtilities.d(TAG, "Problem setting up In-app Billing: " + result);
				} else {
					List<String> skus = new ArrayList<String>();
					skus.add(SKU_DISABLE_ADS);
					getIabHelper().queryInventoryAsync(true, skus, listener);
				}
			}
		});
	}

//	private void loadInventory() {
//		List<String> skus = new ArrayList<String>();
//		skus.add(SKU_DISABLE_ADS);
//		getIabHelper().queryInventoryAsync(true, skus, new IabHelper.QueryInventoryFinishedListener(){
//			@Override
//			public void onQueryInventoryFinished(IabResult iabResult, Inventory inventory) {
//				if (iabResult.isFailure()) {
//					// handle error
//					return;
//				}
//				LogUtilities.d(TAG, "%s: %s", SKU_DISABLE_ADS, inventory.getSkuDetails(SKU_DISABLE_ADS).getPrice());
//			}
//		});
//	}
	
	private IabHelper getIabHelper() {
		if (mIabHelper == null) {
			mIabHelper = new IabHelper(mContext, PUBLIC_KEY);
		}
		return mIabHelper;
	}

	public boolean handleActivityResult(int requestCode, int resultCode, Intent intent) {
		return getIabHelper().handleActivityResult(requestCode, resultCode, intent);
	}	
}
