package com.ez3softworks.fightnerd.stores;

import com.ez3softworks.fightnerd.utilities.LogUtilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartupReceiver extends BroadcastReceiver {
	private static final String TAG = "StartupReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		LogUtilities.d(TAG, "Received the boot!!!");
	}

}
