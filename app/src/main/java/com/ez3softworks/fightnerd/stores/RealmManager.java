package com.ez3softworks.fightnerd.stores;

import android.content.Context;
import android.test.RenamingDelegatingContext;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmManager {
    private final Context context;
    private static RealmManager realmManager;

    private RealmManager(Context context) {
        this.context = context;
    }

    public static synchronized RealmManager getInstance(Context context) {
        if (realmManager == null) {
            realmManager = new RealmManager(context);
        }
        return realmManager;
    }

    public Realm getRealm() {
        Realm realm;
//        if (context instanceof RenamingDelegatingContext) {
//            RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).name("FightNerd.realm").inMemory().build();
//            realm = Realm.getInstance(realmConfiguration);
//
//        } else {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).name("FightNerd.realm").build();
        realm = Realm.getInstance(realmConfiguration);
        return realm;
    }
}
