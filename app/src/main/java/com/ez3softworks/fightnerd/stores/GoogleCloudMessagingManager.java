package com.ez3softworks.fightnerd.stores;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;

import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class GoogleCloudMessagingManager {
	private static final String TAG = "GoogleCloudMessagingManager";
	private static final String GOOGLE_CLOUD_MESSAGING_SENDER_ID = "27161842841";
	private static final String GOGGLE_CLOUD_MESSAGING_API_KEY = "AIzaSyAANiWfpvq-5PG-fnctbkIfr46ZItzIugA";

	private static final String PREFERENCESS_FILE = "googleCloudMessagingManager";
	private static final String PREFERENCES_REGISTRATION_ID = "com.ez3softworks.fightnerd.GoogleCloudMessagingManager.registrationID";
	private static final String PREFERENCES_APP_VERSION = "com.ez3softworks.fightnerd.GoogleCloudMessagingManager.appVersion";
	private static final String PREFERENCES_REGISTRATION_ID_CACHE_DATE = "com.ez3softworks.fightnerd.FighterManager.upcomingEventCacheDate";
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private static final long CACHE_REGISTRATION_ID_AGE_MILLISECONDS = 86400*1000*0;
	private SharedPreferences mPreferences;
	private final Context mContext;
	private final Activity mActivity;
	private final GoogleCloudMessaging mGoogleCloudMessaging;
	
	public GoogleCloudMessagingManager(Activity activity) {
		mActivity = activity;
		mPreferences = activity.getApplicationContext().getSharedPreferences(PREFERENCESS_FILE, Context.MODE_PRIVATE);
		mContext = mActivity.getApplicationContext();
		mGoogleCloudMessaging = GoogleCloudMessaging.getInstance(mActivity);
	}

	public void connect() {
		if (checkGooglePlayServices()) {
			// Check the cache age
			Date nowDate = new Date();
			Date lastCacheDate = getRegistrationIdCacheDate();
			long cacheAge = nowDate.getTime()-lastCacheDate.getTime();
			boolean isCacheStale = cacheAge >=CACHE_REGISTRATION_ID_AGE_MILLISECONDS;
			LogUtilities.i(TAG, "Cache age for registration ID is "+cacheAge/1000.0f+" seconds");
	        boolean isAppVersionDifferent = getAppVersion() != getCurrentAppVersion(mContext);
			
	        if(isAppVersionDifferent || isCacheStale) {
	            registerInBackground();
			}
		}
		else {
			LogUtilities.i(TAG, "No valid Google Play Services APK found.");
		}
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkGooglePlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mActivity);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, mActivity, PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        } else {
	            LogUtilities.i(TAG, "This device is not supported.");
	            mActivity.finish();
	        }
	        return false;
	    }
	    return true;
	}
	
	private String getRegistrationId() {
	    return mPreferences.getString(PREFERENCES_REGISTRATION_ID, null);
	}
	
	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getCurrentAppVersion(Context context) {
	    try {
	        PackageInfo packageInfo = context.getPackageManager()
	                .getPackageInfo(context.getPackageName(), 0);
	        return packageInfo.versionCode;
	    } catch (NameNotFoundException e) {
	        // should never happen
	        throw new RuntimeException("Could not get package name: " + e);
	    }
	}
	
	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {

	        @Override
	        protected String doInBackground(Void... params) {
	            String message = "";
	            try {
	                String registrationId = mGoogleCloudMessaging.register(GOOGLE_CLOUD_MESSAGING_SENDER_ID);
	                message = "Device registered, registration ID=" + registrationId;

	                // You should send the registration ID to your server over HTTP,
	                // so it can use GCM/HTTP or CCS to send messages to your app.
	                // The request to your server should be authenticated if your app
	                // is using accounts.
	                sendRegistrationIdToServer(registrationId);

	                // For this demo: we don't need to send it because the device
	                // will send upstream messages to a server that echo back the
	                // message using the 'from' address in the message.

	                // Persist the regID - no need to register again.
	                setRegistrationId(registrationId);
	                setAppVersion(getCurrentAppVersion(mContext));
	            } catch (IOException ex) {
	                message = "Error :" + ex.getMessage();
	                // If there is an error, don't just keep trying to register.
	                // Require the user to click a button again, or perform
	                // exponential back-off.
	            }
	            return message;
	        }

	        @Override
	        protected void onPostExecute(String message) {
	            LogUtilities.i(TAG, message);
	        }
			
		}.execute(null, null, null);
	}
	
	private int getAppVersion() {
	    return mPreferences.getInt(PREFERENCES_APP_VERSION, Integer.MIN_VALUE);
	}
	
	private void setAppVersion(int appVersion) {
		mPreferences.edit().putInt(PREFERENCES_APP_VERSION, appVersion).apply();
	}

	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 *
	 * @param context application's context.
	 * @param regId registration ID
	 */
	private void setRegistrationId(String registrationId) {
		mPreferences.edit().putString(PREFERENCES_REGISTRATION_ID, registrationId).apply();
		mPreferences.edit().putLong(PREFERENCES_REGISTRATION_ID_CACHE_DATE, (new Date()).getTime()).apply();
	}
	
	private Date getRegistrationIdCacheDate() {
		return new Date(mPreferences.getLong(PREFERENCES_REGISTRATION_ID_CACHE_DATE, 0));
	}

	private void sendRegistrationIdToServer(String registrationId) {
		GoogleCloudMessagingProfileDownloader googleCloudMessagingProfileDownloader = new GoogleCloudMessagingProfileDownloader();
		googleCloudMessagingProfileDownloader.post(registrationId);	
	}
	
	private static class GoogleCloudMessagingProfileDownloader {
		private static final String TAG = "GoogleCloudMessagingProfileDownloader";

		private final Map<String, String> mQueryParameters;
		private final String mEndpoint;
		
		public GoogleCloudMessagingProfileDownloader() {
			mQueryParameters = new HashMap<String,String>();
			mEndpoint = "googlecloudmessagingprofile";
		}
		
		public void post(String registrationId) {
//			JSONObject jsonObject = new JSONObject();
//			try {
//				jsonObject.put("registrationId", registrationId);
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				LogUtilities.e(TAG, "Error creating JSON for POST", e);
//			}
//
//			try {
//				post(new StringEntity(jsonObject.toString()));
//			} catch (UnsupportedEncodingException e) {
//				LogUtilities.e(TAG, "Error POSTing registrationId", e);
//			}
		}
		
		protected String getEndpoint() {
			return mEndpoint;
		}

		protected Map<String, String> getQueryParameters() {
			return mQueryParameters;
		}
	}
}
