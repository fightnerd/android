package com.ez3softworks.fightnerd.stores;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.ez3softworks.fightnerd.R;
//import com.ez3softworks.fightnerd.controllers.NavigationDrawerActivity.FragmentType;
import com.ez3softworks.fightnerd.utilities.LogUtilities;

public class NotificationService extends IntentService {
		private static final String TAG = "NotificationService";
		public static final String EXTRA_TITLE = "title";
		public static final String EXTRA_BODY = "body";
		public static final String EXTRA_TICKER = "ticker";

		public NotificationService() {
			super(TAG);
		}

		@Override
		protected void onHandleIntent(Intent intent) {
			LogUtilities.i(TAG, "Received an intent: %s", intent);
			String action = intent.getAction();
			if (action != null) {
				if (action.equals("com.google.android.c2dm.intent.RECEIVE")) {
//					NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//									.setSmallIcon(R.drawable.ic_launcher)
//									.setContentTitle(intent.getStringExtra(EXTRA_TITLE))
//									.setContentText(intent.getStringExtra(EXTRA_BODY))
//									.setTicker(intent.getStringExtra(EXTRA_TICKER))
//									.setAutoCancel(true);
//
//					// The stack builder object will contain an artificial back stack for the
//					// started Activity.
//					// This ensures that navigating backward from the Activity leads out of
//					// your application to the Home screen.
//					TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this);
//					// Adds the back stack for the Intent (but not the Intent itself)
//					taskStackBuilder.addParentStack(NavigationDrawerActivity.class);
//					// Adds the Intent that starts the Activity to the top of the stack
//					taskStackBuilder.addNextIntent(NavigationDrawerActivity.getIntent(this, FragmentType.EventList));
//					PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//					notificationBuilder.setContentIntent(pendingIntent);
//
//					android.app.NotificationManager notificationManager = (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//					notificationManager.notify((int)System.currentTimeMillis(), notificationBuilder.build());
				}
			}
		}
		
		public enum NotificationType {
			UPCOMING_EVENT;			
		}

}