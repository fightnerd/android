package com.ez3softworks.fightnerd.stores;

import java.util.ArrayList;
import java.util.List;

import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.ez3softworks.fightnerd.utilities.StringUtilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class BaseDatabaseHelper extends SQLiteOpenHelper {
	private static final String TAG = "BaseDatabaseHelper";
	protected static final String COLUMN_ID = "_id";
	
	public BaseDatabaseHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}
	
	protected final int delete(From from, Where where) {
		int numDeleted = getWritableDatabase().delete(from.getSql(), where.getSql(), where.getArgs());
		return numDeleted;
	}
	
	protected final long insert(From from, Where where, ContentValues contentValues) {
		SQLiteDatabase writableDatabase = getWritableDatabase();
		writableDatabase.beginTransaction();
		long id = -1;
		try {
			try {
				id = writableDatabase.insertOrThrow(from.getSql(), null, contentValues);
			} catch (SQLException s) {
				
			}
			if (id < 0) {
				// Update the existing entry
				writableDatabase.update(from.getSql(), contentValues, where.getSql(), where.getArgs());
				
				// Get the row id...
				Cursor cursor = getReadableDatabase().query(
						from.getSql(), 
						null, 
						where.getSql(), 
						where.getArgs(), 
						null, 
						null, 
						null);
				cursor.moveToFirst();
				id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
			}
			writableDatabase.setTransactionSuccessful();
		} finally {
			writableDatabase.endTransaction();
		}
		return id;
	}
	
	protected final Cursor query(From from, List<Order> ordering, Where where, String uniqueColumnName) {
		Cursor cursor = null;
		
		// Set up FROM
		String fromSql = from.getSql();
		LogUtilities.d(TAG, "FROM: %s", fromSql);
	
		
		// Set up WHERE
		String whereSql = "";
		String[] whereArgs = null;
		if (where != null) {
			whereSql = where.getSql();
			whereArgs = where.getArgs();
		}
		LogUtilities.d(TAG, "Where: %s", whereSql);
		
		// Set up ORDER BY
		String orderSql = "";
		if (ordering!=null && !ordering.isEmpty()) {
			List<String> orderBys = new ArrayList<String>();
			for (Order order:ordering) {
				orderBys.add(order.getSql());
			}
			orderSql = StringUtilities.join(orderBys, ", ");
		}
		LogUtilities.d(TAG, "Order: %s", orderSql);
		
		cursor = getReadableDatabase().query(
				uniqueColumnName!=null,
				fromSql, 
				null, 
				whereSql, 
				whereArgs, 
				uniqueColumnName, 
				null, 
				orderSql,
				null);
		return cursor;
	}

	public static class On {
		private String mFirstTableName;
		private String mSecondTableName;
		private String mFirstColumnName;
		private String mSecondColumnName;
		
		public On(String firstTableName, String firstColumnName, String secondTableName, String secondColumnName) {
			super();
			mFirstTableName = firstTableName;
			mSecondTableName = secondTableName;
			mFirstColumnName = firstColumnName;
			mSecondColumnName = secondColumnName;
		}
	
		public String getSql() {
			return String.format("%s.%s = %s.%s", 
								 mFirstTableName,
								 mFirstColumnName,
								 mSecondTableName,
								 mSecondColumnName);
		}
		
		public String getFirstColumnName() {
			return mFirstColumnName;
		}
	
		public void setFirstColumnName(String firstColumnName) {
			mFirstColumnName = firstColumnName;
		}
		
		public String getSecondColumnName() {
			return mSecondColumnName;
		}
		
		public void setSecondColumnName(String secondColumnName) {
			mSecondColumnName = secondColumnName;
		}
	}


	public static class From {
		private String mFirstTableName;
		private String mSecondTableName;
		private On mOn;
		
		public From(String firstTableName) {
			this();
			mFirstTableName = firstTableName;
		}
		
		public From(String firstTableName, String secondTableName, On on) {
			this();
			mFirstTableName = firstTableName;
			mSecondTableName = secondTableName;
			mOn = on;
		}
		
		public From() {
			super();
			mFirstTableName = null;
			mSecondTableName = null;
			mOn = null;
		}
		
		public String getSql() {
			String sql = null;
			if (mSecondTableName != null && mOn != null) {
				sql = String.format("%s JOIN %s ON (%s)", mFirstTableName, mSecondTableName, mOn.getSql());
			} else {
				sql = mFirstTableName;
			}
			return sql;
		}
	
		public String getFirstTableName() {
			return mFirstTableName;
		}
		
		public void setFirstTableName(String firstTableName) {
			mFirstTableName = firstTableName;
		}
		
		public String getSecondTableName() {
			return mSecondTableName;
		}
		
		public void setSecondTableName(String secondTableName) {
			mSecondTableName = secondTableName;
		}
		
	}


	public static class Order {
		private String mColumnName;
		private boolean mIsAscending;
		public Order(String columnName, boolean isAscending) {
			super();
			mColumnName = columnName;
			mIsAscending = isAscending;
		}
		
		public String getSql() {
			return String.format("%s %s", mColumnName, mIsAscending ? "asc":"desc");
		}
	}


	public static class Where {
		private String mSql;
		private String[] mArgs;
	
		public Where(String sql, String[] args) {
			super();
			mSql = sql;
			mArgs = args;
		}
	
		public String getSql() {
			return mSql;
		}
	
		public void setSql(String sql) {
			mSql = sql;
		}
	
		public String[] getArgs() {
			return mArgs;
		}
	
		public void setArgs(String[] args) {
			mArgs = args;
		}
	}
}
