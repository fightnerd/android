package com.ez3softworks.fightnerd.stores;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.IabResult;
import com.android.vending.billing.util.Inventory;
import com.ez3softworks.fightnerd.R;
import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class AdManager {
	private static final String TAG = "AdManager";
	private static final String PREFERENCESS_FILE = "ads";
	private static final String PREFERENCES_ARE_ADS_ENABLED = "com.ez3softworks.fightnerd.AdManager.areAdsEnabled";
	private static final String PREFERENCES_NUM_INTERSTITIAL_REQUESTS = "com.ez3softworks.fightnerd.AdManager.numInterstitialRequests";
	private static final boolean PREFERENCES_DEFAULT_ARE_ADS_ENABLED = true;

		
	private static AdManager adManager;
	
	private Context context;
	private InterstitialAd interstitialAd;
	private SharedPreferences preferences;

	
	// The private constructor forces users to use AdManager.get(Context)
	private AdManager(Context context) {
		this.context = context;
		preferences = context.getSharedPreferences(PREFERENCESS_FILE, Context.MODE_PRIVATE);

        interstitialAd = new InterstitialAd(context);
        interstitialAd.setAdUnitId(context.getResources().getString(R.string.admob_interstitial_api_key));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                loadInterstitial();
            }
        });
        loadInterstitial();

        StoreManager.getInstance(this.context).hasPurchase(StoreManager.SKU_DISABLE_ADS, new IabHelper.QueryInventoryFinishedListener() {
			@Override
			public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
				boolean hasPurchase = false;
				if (inventory != null) {
					hasPurchase = inventory.hasPurchase(StoreManager.SKU_DISABLE_ADS);
					LogUtilities.d(TAG, "SKU: %s\tdidPurchase: %s", StoreManager.SKU_DISABLE_ADS, hasPurchase);
				}
				setAreAdsEnabled(!hasPurchase);
			}
		});
	}

	private void loadAds() {        
        loadInterstitial();
	}

	public static synchronized AdManager getInstance(Context context) {
		if (adManager == null) {
            adManager = new AdManager(context);
		}
		return adManager;
	}
	
	public static void reset() {
		adManager = null;
	}
	
	private void loadInterstitial() {
		AdRequest adRequest = getAdRequest();
		interstitialAd.loadAd(adRequest);
	}

	private AdRequest getAdRequest() {
		AdRequest adRequest = new AdRequest.Builder()//.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//				  									 .addTestDevice("9A094DB4B63F5BF57419A0C7A00B626C")
				  									 .build();
		return adRequest;
	}
	
	public boolean showInterstitial() {
		boolean didShowInterstitial = false;
		int numInterstitialRequests = getNumInterstitialRequests();
		if ((interstitialAd != null) && interstitialAd.isLoaded() && (numInterstitialRequests >= 5) && (numInterstitialRequests % 2 == 0)) {
			interstitialAd.show();
			didShowInterstitial = true;
		}
		numInterstitialRequests += 1;
		setNumInterstitialRequests(numInterstitialRequests);
		return didShowInterstitial;
	}
	
	public void setAreAdsEnabled(boolean areAdsEnabled) {
		preferences.edit().putBoolean(PREFERENCES_ARE_ADS_ENABLED, areAdsEnabled).commit();
		loadAds();
	}
	
	public boolean areAdsEnabled() {
		return preferences.getBoolean(PREFERENCES_ARE_ADS_ENABLED, PREFERENCES_DEFAULT_ARE_ADS_ENABLED);
	}

	public int getNumInterstitialRequests() {
		return preferences.getInt(PREFERENCES_NUM_INTERSTITIAL_REQUESTS, 0);
	}

	public void setNumInterstitialRequests(int numInterstitialRequests) {
		preferences.edit().putInt(PREFERENCES_NUM_INTERSTITIAL_REQUESTS, numInterstitialRequests).apply();
	}
}
