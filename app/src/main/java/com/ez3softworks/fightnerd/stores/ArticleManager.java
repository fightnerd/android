package com.ez3softworks.fightnerd.stores;

import android.content.Context;
import android.content.SharedPreferences;
import android.test.RenamingDelegatingContext;
import android.util.Log;

import com.ez3softworks.fightnerd.models.Article;
import com.ez3softworks.fightnerd.models.Fighter;
import com.ez3softworks.fightnerd.models.Publication;
import com.ez3softworks.fightnerd.utilities.DateUtilities;
import com.squareup.picasso.Downloader;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleManager {
	private static final String TAG = "ArticleManager";
	
	private static ArticleManager articleManager;
	
	private final Context context;

	// The private constructor forces users to use ArticleManager.get(Context)
	private ArticleManager(Context context) {
		this.context = context;
	}
	
	public static synchronized ArticleManager getInstance(Context context) {
        if (articleManager == null) {
            articleManager = new ArticleManager(context.getApplicationContext());
        }
        return articleManager;
	}

	public Response<ApiManager.ListResponse<Article>> fetchArticles(Date beforeDate, Publication publication, String query) {
        int limit = 50;
        ApiManager.QueryDate queryDate = null;
        if (beforeDate != null) {
            beforeDate = DateUtilities.getRoundedDate(beforeDate, 30, false);
            queryDate = new ApiManager.QueryDate(beforeDate);
        }

        String publicationName = null;
        if (publication != null && publication != Publication.TopNews) {
            publicationName = publication.getQuery();
        }

        if (query != null) {
            limit = 100;
        }

        Call<ApiManager.ListResponse<Article>> call = ApiManager.getInstance(context).fightNerdService.listArticles(
                limit,
                queryDate,
                publicationName,
                query
        );

        Response<ApiManager.ListResponse<Article>> response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            Log.e(TAG, "Server error", e);
        } catch (RuntimeException e) {
            Log.e(TAG, "Runtime error", e);
        }

        // Dump data into Realm
        if (response != null && response.isSuccess()) {
            Log.i(TAG, "Fetched " + response.raw().request().url().toString());
            for (int i = 0; i < response.body().getResults().size(); i++) {
                Article article = response.body().getResults().get(i);
                article.setRank(i);
                updateArticle(article);
            }
        }

        return response;
    }

    private void updateArticle(final Article article) {
        Realm realm = RealmManager.getInstance(context).getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Article oldArticle = realm.where(Article.class).equalTo("guid", article.getGuid()).findFirst();
                if (oldArticle != null) {
                    article.setIsRead(oldArticle.isRead());
                    article.setIsFavorite(oldArticle.isFavorite());
                    article.setIsSavedForLater(oldArticle.isSavedForLater());
                }
                realm.copyToRealmOrUpdate(article);
            }
        });
    }

	public void fetchArticles(Date beforeDate, String publication, final com.ez3softworks.fightnerd.stores.Callback callback) {
		ApiManager.QueryDate queryDate = null;
		if (beforeDate != null) {
			queryDate = new ApiManager.QueryDate(beforeDate);
		}

		Call<ApiManager.ListResponse<Article>> call = ApiManager.getInstance(context).fightNerdService.listArticles(
				50,
                queryDate,
				publication,
                null
		);
		call.enqueue(new Callback<ApiManager.ListResponse<Article>>() {
			@Override
			public void onResponse(Call<ApiManager.ListResponse<Article>> call, final Response<ApiManager.ListResponse<Article>> response) {
				if (response.isSuccess()) {
					Realm realm = RealmManager.getInstance(context).getRealm();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(response.body().getResults());
                        }
                    }, new Realm.Transaction.Callback() {
                        @Override
                        public void onSuccess() {
                            callback.onSuccess();
                        }

                        @Override
                        public void onError(Exception e) {
                            callback.onFailure(e);
                        }
                    });
				} else {
					callback.onFailure(null);
				}
			}

			@Override
			public void onFailure(Call<ApiManager.ListResponse<Article>> call, Throwable t) {
				callback.onFailure(t);
				Log.d("Error", t.getMessage());
			}
		});
	}

}
