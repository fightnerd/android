package com.ez3softworks.fightnerd.stores;

import android.app.ActivityManager;
import android.content.Context;

import com.ez3softworks.fightnerd.utilities.LogUtilities;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

public class PicassoManager {
	private static final String TAG = "PicassoManager";
	
	private static final float CACHE_RATIO = 0.125f;
	
	private static PicassoManager picassoManager;
	
	private Context context;
	private Picasso picasso;
	
	// The private constructor forces users to use AdManager.get(Context)
	private PicassoManager(Context context) {
		this.context = context;
		
		int memoryClassInMegabytes = ((ActivityManager) this.context.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
		int cacheSizeInBytes = (int)(memoryClassInMegabytes*CACHE_RATIO*1048576); 
		LogUtilities.i(TAG, "Picasso cache has a size of %f megabytes", cacheSizeInBytes/1048576.0f);
		picasso = new Picasso.Builder(this.context).memoryCache(new LruCache(cacheSizeInBytes)).downloader(new OkHttpDownloader(this.context)).build();
	}

	public static PicassoManager getInstance(Context context) {
		return getInstance(context, true);
	}

	public static synchronized PicassoManager getInstance(Context context, boolean useApplicationContext) {
		if (picassoManager == null) {
			if (useApplicationContext) {
				picassoManager = new PicassoManager(context.getApplicationContext());
			} else {
				picassoManager = new PicassoManager(context);
			}
		}
		return picassoManager;
	}
	
	public static void reset() {
		picassoManager = null;
	}
	
	public static Picasso getPicasso(Context context) {
		return getInstance(context).getPicasso();
	}
	
	public Picasso getPicasso() {
		return picasso;
	}
}
