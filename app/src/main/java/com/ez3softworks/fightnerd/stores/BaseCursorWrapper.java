package com.ez3softworks.fightnerd.stores;

import java.util.Date;

import android.database.Cursor;
import android.database.CursorWrapper;

public abstract class BaseCursorWrapper extends CursorWrapper {

	public BaseCursorWrapper(Cursor cursor) {
		super(cursor);
	}
	
	public Date getDate(int columnIndex) {
		long date = getLong(columnIndex);
		if (date >= 0) {
			return new Date(date);
		} else {
			return null;
		}
	}
}
