package com.ez3softworks.fightnerd.stores;

import com.ez3softworks.fightnerd.utilities.LogUtilities;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

public abstract class SQLiteCursorLoader extends AsyncTaskLoader<Cursor> {
	private static final String TAG = "SQLiteCursorLoader";
	private Cursor mCursor;

	public SQLiteCursorLoader(Context context) {
		super(context);
	}
	
	protected abstract Cursor loadCursor();

	@Override
	public Cursor loadInBackground() {
		Cursor cursor = loadCursor();
		if (cursor != null) {
			cursor.getCount();
		}
		return cursor;
	}
	
	@Override
	public void deliverResult(Cursor newCursor) {
		Cursor oldCursor = mCursor;
		mCursor = newCursor;
		
		if (isStarted()) {
			super.deliverResult(newCursor);
		}
		
		if (oldCursor != null && oldCursor != newCursor && !oldCursor.isClosed()) {
			oldCursor.close();
			LogUtilities.i(TAG, "deliverResult(): Closed cursor");
		}
	}
	
	@Override
	protected void onStartLoading() {
		if (mCursor != null) {
			deliverResult(mCursor);
		}
		if (takeContentChanged() || mCursor == null) {
			forceLoad();
		}
	}
	
	@Override
	protected void onStopLoading() {
		// Attempt to cancel the current load task if possible.
		cancelLoad();
	}
	
	@Override
	public void onCanceled(Cursor cursor) {
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
			LogUtilities.i(TAG, "onCanceled(): Closed cursor");
		}
	}
	
	@Override
	protected void onReset() {
		super.onReset();
		
		// Ensure the loader is stopped
		onStopLoading();
		
		if (mCursor != null && !mCursor.isClosed()) {
//			mCursor.close();
//			LogUtilities.i(TAG, "onReset(): Closed cursor");
		}
		mCursor = null;
	}	
}
