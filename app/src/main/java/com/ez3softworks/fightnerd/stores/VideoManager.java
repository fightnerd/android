package com.ez3softworks.fightnerd.stores;

import android.content.Context;
import android.util.Log;

import com.ez3softworks.fightnerd.models.Article;
import com.ez3softworks.fightnerd.models.Channel;
import com.ez3softworks.fightnerd.models.Publication;
import com.ez3softworks.fightnerd.models.Video;
import com.ez3softworks.fightnerd.utilities.DateUtilities;

import java.io.IOException;
import java.util.Date;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoManager {
	private static final String TAG = "VideoManager";

	private static VideoManager videoManager;

	private final Context context;

	// The private constructor forces users to use ArticleManager.get(Context)
	private VideoManager(Context context) {
		this.context = context;
	}
	
	public static synchronized VideoManager getInstance(Context context) {
        if (videoManager == null) {
            videoManager = new VideoManager(context.getApplicationContext());
        }
        return videoManager;
	}

	public Response<ApiManager.ListResponse<Video>> fetchVideos(Date beforeDate, Channel channel, String query) {
        int limit = 50;
        ApiManager.QueryDate queryDate = null;
        if (beforeDate != null) {
            beforeDate = DateUtilities.getRoundedDate(beforeDate, 30, false);
            queryDate = new ApiManager.QueryDate(beforeDate);
        }

        String publicationName = null;
        if (channel != null && channel != Channel.TopVideos) {
            publicationName = channel.getQuery();
        }

        if (query != null) {
            limit = 100;
        }

        Call<ApiManager.ListResponse<Video>> call = ApiManager.getInstance(context).fightNerdService.listVideos(
                limit,
                queryDate,
                publicationName,
                query
        );

        Response<ApiManager.ListResponse<Video>> response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            Log.e(TAG, "Server error", e);
        } catch (RuntimeException e) {
            Log.e(TAG, "Runtime error", e);
        }

        // Dump data into Realm
        if (response != null && response.isSuccess()) {
            Log.i(TAG, "Fetched " + response.raw().request().url().toString());
            for (int i = 0; i < response.body().getResults().size(); i++) {
                Video video = response.body().getResults().get(i);
                video.setRank(i);
                updateVideo(video);
            }
        }

        return response;
    }


    private void updateVideo(final Video video) {
        Realm realm = RealmManager.getInstance(context).getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Video oldVideo = realm.where(Video.class).equalTo("guid", video.getGuid()).findFirst();
                if (oldVideo != null) {
                    video.setIsWatched(oldVideo.isWatched());
                    video.setIsFavorite(oldVideo.isFavorite());
                }
                realm.copyToRealmOrUpdate(video);
            }
        });
    }
}
