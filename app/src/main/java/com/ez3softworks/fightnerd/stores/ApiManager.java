package com.ez3softworks.fightnerd.stores;

import android.content.Context;

import com.ez3softworks.fightnerd.models.Article;
import com.ez3softworks.fightnerd.models.Event;
import com.ez3softworks.fightnerd.models.Fighter;
import com.ez3softworks.fightnerd.models.Ranking;
import com.ez3softworks.fightnerd.models.Video;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.realm.RealmObject;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ApiManager {
    private static ApiManager apiManager;
    private final Context context;
    public FightNerdService fightNerdService;
    private static long HTTP_CACHE_SIZE = 10 * 1024 * 1024;

    private ApiManager(Context context) {
        this.context = context;
        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getDeclaringClass().equals(RealmObject.class);
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        }).create();

        // Build client
        Cache cache = new Cache(new File(this.context.getCacheDir(), "http"), HTTP_CACHE_SIZE);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://d2omniay5i8qm4.cloudfront.net/api/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        fightNerdService = retrofit.create(FightNerdService.class);
    }

    public static synchronized ApiManager getInstance(Context context) {
        if (apiManager == null) {
            apiManager = new ApiManager(context);
        }
        return apiManager;
    }

    public interface FightNerdService {
        @GET("publications/article")
        Call<ListResponse<Article>> listArticles(
                @Query("limit")Integer limit,
                @Query("date__lte")QueryDate beforeDate,
                @Query("publication")String publication,
                @Query("q")String query);

        @GET("publications/video")
        Call<ListResponse<Video>> listVideos(
                @Query("limit")Integer limit,
                @Query("date__lte")QueryDate beforeDate,
                @Query("channel")String channel,
                @Query("q")String query);

        @GET("fighters/fighter")
        Call<ListResponse<Fighter>> listFighters(
                @Query("limit")Integer limit,
                @Query("q")String query);

        @GET("fighters/ranking")
        Call<ListResponse<Ranking>> listRankings(
                @Query("weightClass")String weightClass);

        @GET("fighters/event")
        Call<ListResponse<Event>> listEvents(
                @Query("ordering")String ordering,
                @Query("limit")Integer limit,
                @Query("after_date")QueryDate afterDate);

    }

    public static class QueryDate {
        private static final ThreadLocal<DateFormat> dateFormat = new ThreadLocal<DateFormat>(){
            @Override
            public DateFormat initialValue() {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+00:00", Locale.US);
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                return dateFormat;
            }
        };
        private final Date date;
        public QueryDate(Date date) {
            this.date = date;
        }

        @Override
        public String toString() {
            return dateFormat.get().format(date);
        }
    }

    public static class ListResponse<T> {
        private int count;
        private String next;
        private String previous;
        private List<T> results;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getPrevious() {
            return previous;
        }

        public void setPrevious(String previous) {
            this.previous = previous;
        }

        public List<T> getResults() {
            return results;
        }

        public void setResults(List<T> results) {
            this.results = results;
        }
    }
}
