package com.ez3softworks.fightnerd.stores;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.ez3softworks.fightnerd.models.Article;
import com.ez3softworks.fightnerd.models.Corner;
import com.ez3softworks.fightnerd.models.Event;
import com.ez3softworks.fightnerd.models.Fight;
import com.ez3softworks.fightnerd.models.Fighter;
import com.ez3softworks.fightnerd.models.Ranking;
import com.ez3softworks.fightnerd.utilities.DateUtilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Response;

public class FighterManager {
	private static final String TAG = "FighterManager";

    private static final String PREFERENCESS_FILE = "fighters";
    private static final String PREFERENCES_DID_LOAD_FIGHTERS = "com.ez3softworks.fightnerd.stores.FighterManager.didLoadFighters";

    private static FighterManager fighterManager;
	private final Context context;
    private final SharedPreferences sharedPreferences;

	// The private constructor forces users to use FighterManager.get(Context)
	private FighterManager(Context context) {
		this.context = context;
        this.sharedPreferences = context.getSharedPreferences(PREFERENCESS_FILE, Context.MODE_PRIVATE);
	}
	
	public static synchronized FighterManager getInstance(Context context) {
        if (fighterManager == null) {
            fighterManager = new FighterManager(context.getApplicationContext());
        }
        return fighterManager;
	}

    public void loadFighters() {
        if (didLoadFighters()) {
            return;
        }

        Realm realm = RealmManager.getInstance(context).getRealm();
        try {
            final InputStream inputStream = context.getAssets().open("json/fighters.json");
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    try {
                        realm.createAllFromJson(Fighter.class, inputStream);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        // Set weight class index
        RealmResults<Fighter> realmResults = realm.where(Fighter.class).findAll();
        for (int i = 0; i < realmResults.size(); i++) {
            final Fighter fighter = realmResults.get(i);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    fighter.setWeightClassIndex(Fighter.WeightClass.getEnum(fighter.getWeightClass()).ordinal());
                    Log.d(TAG, fighter.getFullName() + "\t" + fighter.getWeightClass() + "\t" + fighter.getWeightClassIndex());
                }

            });
        }

        // Compact
        Realm.compactRealm(RealmManager.getInstance(context).getRealm().getConfiguration());

        setDidLoadFighters(true);

    }

    public Response<ApiManager.ListResponse<Event>> fetchEvents(Date afterDate) {
        ApiManager.QueryDate queryDate = null;
        if (afterDate != null) {
            afterDate = DateUtilities.getRoundedDate(afterDate, 60, false);
            queryDate = new ApiManager.QueryDate(afterDate);
        }

        Call<ApiManager.ListResponse<Event>> call = ApiManager.getInstance(context).fightNerdService.listEvents(
                "date",
                10,
                queryDate
        );

        Response<ApiManager.ListResponse<Event>> response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            Log.e(TAG, "Server error", e);
        } catch (RuntimeException e) {
            Log.e(TAG, "Runtime error", e);
        }

        // Dump data into Realm
        if (response != null && response.isSuccess()) {
            Log.i(TAG, "Fetched " + response.raw().request().url().toString());
            if (!response.body().getResults().isEmpty()) {
                Realm realm = RealmManager.getInstance(context).getRealm();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.where(Corner.class).findAll().clear();
                        realm.where(Fight.class).findAll().clear();
                        realm.where(Event.class).findAll().clear();
                    }
                });
            }
            for (int i = 0; i < response.body().getResults().size(); i++) {
                Event event = response.body().getResults().get(i);
                updateEvent(event);
            }
        }

        return response;
    }

    public Response<ApiManager.ListResponse<Ranking>> fetchRankings(Ranking.WeightClass weightClass) {
        Call<ApiManager.ListResponse<Ranking>> call = ApiManager.getInstance(context).fightNerdService.listRankings(
                weightClass.getDisplayName()
        );

        Response<ApiManager.ListResponse<Ranking>> response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            Log.e(TAG, "Server error", e);
        } catch (RuntimeException e) {
            Log.e(TAG, "Runtime error", e);
        }

        // Dump data into Realm
        final Response<ApiManager.ListResponse<Ranking>> finalResponse = response;
        if (response != null && response.isSuccess()) {
            Log.i(TAG, "Fetched " + response.raw().request().url().toString());
            for (int i = 0; i < response.body().getResults().size(); i++) {
                Ranking ranking = response.body().getResults().get(i);
                updateRanking(ranking);
            }
        }
        return response;
    }

	public Response<ApiManager.ListResponse<Fighter>> fetchFighters(String query) {
        // Don't go to the interwebz if not a search query
        if (query == null) {
            return null;
        }

        int limit = 100;
        Call<ApiManager.ListResponse<Fighter>> call = ApiManager.getInstance(context).fightNerdService.listFighters(
                limit,
                query
        );

        Response<ApiManager.ListResponse<Fighter>> response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            Log.e(TAG, "Server error", e);
        } catch (RuntimeException e) {
            Log.e(TAG, "Runtime error", e);
        }

        // Dump data into Realm
        if (response != null && response.isSuccess()) {
            Log.i(TAG, "Fetched " + response.raw().request().url().toString());
            for (int i = 0; i < response.body().getResults().size(); i++) {
                Fighter fighter = response.body().getResults().get(i);
                fighter.setRank(i);
                updateFighter(fighter);
            }
        }

        return response;
    }

    private boolean didLoadFighters() {
        return sharedPreferences.getBoolean(PREFERENCES_DID_LOAD_FIGHTERS, false);
    }

    private void setDidLoadFighters(boolean didLoadFighters) {
        sharedPreferences.edit().putBoolean(PREFERENCES_DID_LOAD_FIGHTERS, didLoadFighters).apply();
    }

    private void updateRanking(final Ranking ranking) {
        Realm realm = RealmManager.getInstance(context).getRealm();
        ranking.setFighter(updateFighter(ranking.getFighter()));
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(ranking);
            }
        });
    }

    private Fighter updateFighter(final Fighter fighter) {
        Realm realm = RealmManager.getInstance(context).getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Fighter oldFighter = realm.where(Fighter.class).equalTo("guid", fighter.getGuid()).findFirst();
                if (oldFighter != null) {
                    fighter.setIsFavorite(oldFighter.isFavorite());
                    fighter.setIsSavedForLater(oldFighter.isSavedForLater());
                }
                fighter.setWeightClassIndex(Fighter.WeightClass.getEnum(fighter.getWeightClass()).ordinal());
                realm.copyToRealmOrUpdate(fighter);
            }
        });
        return fighter;
    }

    private Event updateEvent(final Event event) {
        Realm realm = RealmManager.getInstance(context).getRealm();
        for (Fight fight : event.getFights()) {
            for (Corner corner : fight.getCorners()) {
                corner.setFighter(updateFighter(corner.getFighter()));
            }
        }
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(event);
            }
        });
        return event;
    }

//    private Fight updateFight(final Fight fight) {
//        final List<Corner> corners = new ArrayList<>();
//        for (Corner corner : fight.getCorners()) {
//            corners.add(updateCorner(corner));
//        }
//
//        Realm realm = RealmManager.getInstance(context).getRealm();
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                fight.getCorners().clear();
//                realm.copyToRealm(fight);
//            }
//        });
//        realm.executeTransaction(new Realm.Transaction(){
//            @Override
//            public void execute(Realm realm) {
//                for (Corner corner : corners) {
//                    fight.getCorners().add(corner);
//                }
//            }
//        });
//        return fight;
//    }
//
//    private Corner updateCorner(final Corner corner) {
//        final Fighter fighter = updateFighter(corner.getFighter());
//
//        Realm realm = RealmManager.getInstance(context).getRealm();
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                corner.setFighter(null);
//                realm.copyToRealm(corner);
//            }
//        });
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                corner.setFighter(fighter);
//            }
//        });
//        return corner;
//    }
}
